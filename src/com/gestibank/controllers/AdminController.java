package com.gestibank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.gestibank.models.Administrateur;
import com.gestibank.models.LoginUser;
import com.gestibank.models.SessionUser;
import com.gestibank.services.interfaces.AdminService;

@Controller
@SessionAttributes("myAdmin")
public class AdminController {

	@Autowired
	private AdminService adminService;

	//Ajout d'un client dans le model attribute
	@ModelAttribute("myAdmin")
	public SessionUser setAdminSession() {
		return new SessionUser();
	}

	//Acces � l'espace admin
	@RequestMapping(value = "/espace-admin/connexion", method = RequestMethod.POST)
	public String verifConnection(@ModelAttribute("loginUser") LoginUser loginUser, @ModelAttribute("myAdmin") SessionUser myAdmin) {
		System.out.println("Entr�e dans le controller admin");
		System.out.println("loginUser.getLogin() = " + loginUser.getLogin());
		System.out.println("loginUser.getPassword() = " + loginUser.getPassword());
		Boolean isAdmin = adminService.verifCredentials(loginUser.getLogin(), loginUser.getPassword());

		if (isAdmin) {
			System.out.println("Entr�e dans le controller Admin verif true");
			//ajouter un readAdminByLogin() pour remplir le nom et prenom de notre userSession
			Administrateur adminConnecte =  adminService.getAdministrateur(loginUser.getLogin());
			myAdmin.setLogin(adminConnecte.getLogin());
			System.out.println("AdminController myAdmin.getLogin() = " + myAdmin.getLogin());
			myAdmin.setNom(adminConnecte.getNom());
			System.out.println("AdminController myAdmin.getNom() = " + myAdmin.getNom());
			myAdmin.setPrenom(adminConnecte.getPrenom());
			System.out.println("AdminController myAdmin.getPrenom() = " + myAdmin.getPrenom());
			myAdmin.setRole("admin");
			return "redirect:/espace-admin";
		} else {
			System.out.println("Entr�e dans le controller Admin verif false");
			return "redirect:/";
		}
	}
	
	//Affichage de la page espace-admin
	@RequestMapping(value = "/espace-admin", method = RequestMethod.GET)
	public String displayEspaceAdmin() {
		return "admin";
	}	
}
