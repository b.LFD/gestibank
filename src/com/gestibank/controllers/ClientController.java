package com.gestibank.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.gestibank.models.Client;
import com.gestibank.services.interfaces.ClientService;

@Controller
@SessionAttributes({"myClient","role"})
public class ClientController {

	@Autowired
	private ClientService clientService;

	//Ajout d'un client dans le model attribute
	@ModelAttribute("myClient")
	public Client setClientSession() {
		return new Client();
	}
	//Ajout du role dans le model attribute
	@ModelAttribute("role")
	public String addAttributesRole(Model model) {
	    return new String("client");
	}

	//acces � l'espace client
	@RequestMapping(value = "/espace-client/connexion", method = RequestMethod.POST)
	//public String verifConnection(@RequestBody Client client, @ModelAttribute("myClient") Client myClient)
	public String verifConnection(@ModelAttribute("dataClient") Client client, @ModelAttribute("myClient") Client myClient) {
		Boolean isClient = clientService.verifCredentials(client.getLogin(), client.getPassword());

		if(isClient) {
			myClient.setLogin(client.getLogin());
			myClient.setNom(client.getNom());
			myClient.setPrenom(client.getPrenom());
//	        System.out.println("controller client data dans model attributeSession myClient " + myClient.getLogin() + myClient.getNom() + myClient.getPrenom());
			return "redirect:/espace-client";				
		}else{
			return "redirect:/accueil";			
		}
	}

	//Affichage de la page espace-client
	@RequestMapping(value = "/espace-client", method = RequestMethod.GET)
	public String displayEspaceClient() {
		return "espaceClient";
	}
	
	//Affichage page formulaire client
	@RequestMapping(value = "/espace-client/formulaire-client", method = RequestMethod.GET)
	public String displayFormClient(Model model, @ModelAttribute("myClient") Client myClient) {
		Client clientToUpdate = clientService.getClient(myClient.getLogin());
		model.addAttribute("clientToUpdate", clientToUpdate);
		return "formulaireClient";
	}
	
	//Mise a jour par le client des informations clients  (table client)
	@RequestMapping(value = "/espace-client/client", method = RequestMethod.POST)
	public String updateClient(@ModelAttribute("clientUpdated") Client clientUpdated, @ModelAttribute("myClient") Client myClient,  Model model) {
		clientService.updateCompteClientByLogin(myClient.getLogin(), clientUpdated);
		return "redirect:/espace-client";
	}

}
