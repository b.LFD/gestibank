package com.gestibank.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestibank.models.Affectation;
import com.gestibank.services.interfaces.GuestService;


@Controller
public class GuestController {
	
	@Autowired
	private GuestService guestService;
	
	//Affichage de la page d'accueil
	@RequestMapping(value= "/", method= RequestMethod.GET)
	public String displayAccueil() {
		return "accueil";
	}
	
	//Affichage de la page formulaireClient.jsp
	@RequestMapping(value= "/inscription", method= RequestMethod.GET)
	public String displayInscription() {
		return "formulaire-client";
	}
	
	//Envoie demande de cr�ation compte client (cr�ation d'une affectation)
	@RequestMapping(value= "/affectation", method= RequestMethod.POST)
	public String saveAffectation(@ModelAttribute("affectation") Affectation affectation) {
		System.out.println("Entree dans GuestController route /affectation");
		System.out.println("GuestController /affectation affectation.toString() = " + affectation.toString());
		guestService.createAffectation(affectation);
		return "redirect:/";
	}
}
