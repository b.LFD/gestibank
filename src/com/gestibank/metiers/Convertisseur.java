package com.gestibank.metiers;

import java.util.HashMap;
import java.util.Map;

public class Convertisseur {
    
     private static Map conversionTable = new HashMap();
	
	static                                                                                                     
   	{                                                                                                                                               
   		        conversionTable.put("Livre Sterling", new Double(0.86));                                           
   		        conversionTable.put("Dollars Canadien", new Double(1.52)); 
   		        conversionTable.put("Dollar barbadien", new Double(2.44));
   		        conversionTable.put("Riyal Saoudien", new Double(3.75050));                                      
   		        conversionTable.put("YEN", new Double(128.96));                                              
   		        conversionTable.put("Bitcoins", new Double(3.79900)); 
                conversionTable.put("Franc Comorien", new Double(493.26));
                conversionTable.put("Euro", new Double(1));
   		        conversionTable.put("Dollar US", new Double(1.21));                                                    
   	}                                                                                                          
   	public static double convertir(String source, String cible, double montant)                                                                                          
   	{                                                                                                          
   		//The constants should probably be defined somewhere else                                              
   		double tauxSource = ((Double)conversionTable.get(source)).doubleValue();                    
   		double tauxCible = ((Double)conversionTable.get(cible)).doubleValue();                    
   		double tauxConversion = tauxCible/tauxSource;	                                                       
   		return (montant * tauxConversion) ;                                                               
   	}           
   	
   	public static Map getConversionTable()
   	{
   		return conversionTable;	
   	}           
}
