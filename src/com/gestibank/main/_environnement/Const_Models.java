package com.gestibank.main._environnement;

import java.util.Date;
import java.util.List;

import com.gestibank.enumerations.SituationMatrimoniale;
import com.gestibank.enumerations.StatutDemandeChequier;
import com.gestibank.enumerations.TypeOperation;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.CompteCourant;
import com.gestibank.models.CompteRemunere;
import com.gestibank.models.DemandeChequier;
import com.gestibank.models.Transaction;
import com.gestibank.models.Utilisateur;

public class Const_Models {
	private static void constUser(Utilisateur user, String nom, String prenom, String mail, String tel) {
		
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setMail(mail);
		user.setTel(tel);
	}
	
	public static Administrateur constAdmin(String login, String nom, String prenom, String mail, String tel) {
		
		Administrateur admin = new Administrateur(login);
		constUser(admin, nom, prenom, mail, tel);
		
		System.out.println("L'admin cree a pour login: "+admin.getLogin()+" et password: "+admin.getPassword());
		return admin;
	}
	
	public static Agent constAgent(String matricule, String nom, String prenom, String mail, String tel, Administrateur admin) {
		
		Agent agent = new Agent(matricule);
		constUser(agent, nom, prenom, mail, tel);
		agent.setAdminAffecte(admin);
			List<Agent> listAgents = admin.getListeAgents();
			listAgents.add(agent);
			admin.setListeAgents(listAgents);
		
		System.out.println("L'agent cree a pour matricule: "+agent.getMatricule()+" et password: "+agent.getPassword());
		return agent;
	}
	
	public static Client constClient(Affectation affectation, Agent agent) {
		
		Client client = new Client("client_"+affectation.getId());
		return corpsClient(client, affectation, agent);
	}
	
	public static Client constClientID(String login, Affectation affectation, Agent agent) {
		
		Client client = new Client(login);
		return corpsClient(client, affectation, agent);
	}
	
	private static Client corpsClient(Client client, Affectation affectation, Agent agent) {
		
		constUser(client, affectation.getNom(), affectation.getPrenom(), affectation.getMail(), affectation.getTel());
		client.setSituation(affectation.getSituationMatrimoniale());
		client.setDateNaissance(affectation.getDateNaissance());
		client.setAdresse(affectation.getAdresse());
		client.setAgentAffecte(agent);
			List<Client> listClients = agent.getListeClients();
			listClients.add(client);
			agent.setListeClients(listClients);
		
		System.out.println("Le client cree a pour login: "+client.getLogin()+" et password: "+client.getPassword());
		return client;
	}
	
	public static void constCB(CompteBancaire compte, String rib, Client client) {
		
		compte.setRib(rib);
		compte.setSolde(0.0);
		compte.setPropritaireClient(client);
			List<CompteBancaire> listComptes = client.getComptesBancaires();
			listComptes.add(compte);
			client.setComptesBancaires(listComptes);
			
		//System.out.println("\n..............Compte Bancaire Cree..............\n"+compte.toString()+"\n................................................\n");
	}
	
	public static CompteCourant constCC(String rib, Client client, double decouvert, double revenu) {
		
		CompteCourant compte = new CompteCourant();
		compte.setMontantInteret(0.0);
		compte.setNbJoursPostDecouvert(0);
		compte.setEntreeMoyenneMensuelle(revenu);
		if(decouvert < compte.getPlafondDecouvert())
			compte.setDecouvert(decouvert);
		else
			compte.setDecouvert(compte.getPlafondDecouvert());
		compte.setDemandeChequier(null);
		constCB(compte, rib, client);
		
		//System.out.println("\n..............Compte Courant Cree..............\n"+compte.toString()+"\n................................................\n");
		
		return compte;
	}
	
	public static CompteRemunere constCR(String rib, Client client) {
		
		CompteRemunere compte = new CompteRemunere();
		compte.setMontantRemuneration(0.0);
		compte.setNbJoursPostRemuneration(0);
		constCB(compte, rib, client);
		
		//System.out.println("\n..............Compte Remunere Cree..............\n"+compte.toString()+"\n................................................\n");
		
		return compte;
	}
	
	public static Transaction constTransaction(CompteBancaire compte, double montant, TypeOperation operation) {
		
		Transaction transaction = new Transaction();
		transaction.setTypeOperation(operation);
		transaction.setMontant(montant);
		transaction.setCompte(compte);
			List<Transaction> listTransactions = compte.getListeTransactions();
			listTransactions.add(transaction);
			compte.setListeTransactions(listTransactions);
			
		return transaction;
	}
	
	public static DemandeChequier constChequier(CompteCourant compte) {
		
		DemandeChequier chequier = new DemandeChequier();
		chequier.setStatut(StatutDemandeChequier.en_attente);
		chequier.setCompte(compte);
			compte.setDemandeChequier(chequier);
		Agent agent = compte.getPropritaireClient().getAgentAffecte();
		chequier.setAgentAffecte(agent);
			List<DemandeChequier> listDemandeChequiers = agent.getListeDemandeChequier();
			listDemandeChequiers.add(chequier);
			agent.setListeDemandeChequier(listDemandeChequiers);

			return chequier;
	}
	
	public static Affectation constAffectation(String nom, String prenom, String mail, String adresse, String tel, Date dateNaissance, SituationMatrimoniale situation, Administrateur admin) {
		
		Affectation affectation = new Affectation();
		affectation.setNom(nom);
		affectation.setPrenom(prenom);
		affectation.setMail(mail);
		affectation.setAdresse(adresse);
		affectation.setTel(tel);
		affectation.setDateNaissance(dateNaissance);
		affectation.setSituationMatrimoniale(situation);
		affectation.setAdminAffecte(admin);
		
		return affectation;
	}
}
