package com.gestibank.main._environnement;

import java.util.Date;

import com.gestibank.enumerations.SituationMatrimoniale;
import com.gestibank.enumerations.TypeOperation;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.models.CompteCourant;
import com.gestibank.models.CompteRemunere;
import com.gestibank.models.DemandeChequier;
import com.gestibank.models.Transaction;

public class ENVIRONNEMENT_1 {
	public ENVIRONNEMENT_1() {
		super();
	}
	
	
	/* USER */
	
		// Admins
		public Administrateur admin_0 = Const_Models.constAdmin("admin-0", "Denden", "Islem", "islem.denden@wahoo.com", "07.50.47.12.00");
		
		// Agents
		public Agent agent_1 = Const_Models.constAgent("agent-1", "gestibank", "View", "view@hotmail.com", "07.00.58.42.17", admin_0);
		public Agent agent_2 = Const_Models.constAgent("agent-2", "gestibank", "Dao", "dao@hotmail.com", "+336.87.58.24.11", admin_0);
		public Agent agent_3 = Const_Models.constAgent("agent-3", "gestibank", "Service", "service@hotmail.com", "06.99.56.82.08", admin_0);
		public Agent agent_4 = Const_Models.constAgent("agent-4", "gestibank", "Controller", "controller@hotmail.com", "07.25.04.35.18", admin_0);
		public Agent agent_5 = Const_Models.constAgent("agent-5", "gestibank", "Test", "test@hotmail.com", "06.47.25.96.15", admin_0);				//agent sans clients
		
		public Agent agent_6 = Const_Models.constAgent("agent-6", "gestibank", "Extern", "extern@hotmail.com", "07.49.26.38.14", new Administrateur());		//agent sans admin

/* nullPointer */
		//public Agent agent_7 = Const_Models.constAgent("agent-7", "gestibank", "Crash", "crash@hotmail.com", "07.49.26.38.14", null);				//agent null

		// Affectation
		public Affectation affectation_1 = Const_Models.constAffectation("", "Ahmed", "ahmed@free.fr", "68 boulevard des grenouilles, 54470 UTOPIA", "07.44.13.08.25", new Date(), SituationMatrimoniale.marie, admin_0);
		public Affectation affectation_2 = Const_Models.constAffectation("Lafond", "Benjamin", "benjamin.lafond@free.fr", "3 rue des marmottes,51230 TAIN", "+336.66.87.42.36", new Date(), SituationMatrimoniale.veuf, admin_0);
		public Affectation affectation_3 = Const_Models.constAffectation("Dupart", "Gabriel", "gabriel.dupart@free.fr", "7 avenue des vagues, 32800 CANAP", "07.39.41.35.00", new Date(), SituationMatrimoniale.concubinage, admin_0);
		public Affectation affectation_4 = Const_Models.constAffectation("Mouret", "Guillaume", "guillaume.mouret@free.fr", "27 champs des tulipes, 68300 AIHN", "06.84.32.55.69", new Date(), SituationMatrimoniale.divorce, admin_0);
		public Affectation affectation_5 = Const_Models.constAffectation("", "Iulian", "iulian@free.fr", "800 bat.2, 52 chemin des aigrette, 18700 ETROIN", "07.82.67.32.21", new Date(), SituationMatrimoniale.celibataire, admin_0);
		public Affectation affectation_6 = Const_Models.constAffectation("", "Lynda", "lynda@free.fr", "1 palai des deleguees, 666 PARADISE", "06.58.70.87.23", new Date(), SituationMatrimoniale.pacse, admin_0);
		public Affectation affectation_7 = Const_Models.constAffectation("Rambla", "Magalie", "magalie.rambla@free.fr", "42 rue des multi-pc, 26873 SEINYK", "337.86.20.48.37", new Date(), SituationMatrimoniale.concubinage, admin_0);
		
		public Affectation affectation_8 = Const_Models.constAffectation("", "Extern", "extern@free.fr", "50 Porte du Neant", "+336.56.20.00.57", new Date(), SituationMatrimoniale.concubinage, admin_0);
		public Affectation affectation_9 = Const_Models.constAffectation("", "Crash", "crash@free.fr", "14 parc Zombiland", "06.81.39.57.03", new Date(), SituationMatrimoniale.concubinage, admin_0);
		
		public Affectation affectation_10 = Const_Models.constAffectation("", "Espion", "espion@free.fr", "...", "07.00.00.00.00", new Date(), SituationMatrimoniale.celibataire, new Administrateur());									//affectation a un admin externe
		public Affectation affectation_11 = Const_Models.constAffectation("Compte", "Drack", "compte.drack@free.fr", "51 tombe du soleil, 83000 PROVENCE", "06.66.51.24.00", new Date(), SituationMatrimoniale.veuf, null);					//affectation a un admin null
		
		// Clients
		public Client client_1 = Const_Models.constClient(affectation_1, agent_2);
		public Client client_2 = Const_Models.constClient(affectation_2, agent_3);
		public Client client_3 = Const_Models.constClient(affectation_3, agent_3);
		public Client client_4 = Const_Models.constClient(affectation_4, agent_4);
		public Client client_5 = Const_Models.constClient(affectation_5, agent_1);
		public Client client_6 = Const_Models.constClient(affectation_6, agent_2);
		public Client client_7 = Const_Models.constClient(affectation_7, agent_1);
		public Client client_8 = Const_Models.constClient(affectation_8, agent_1);
		public Client client_9 = Const_Models.constClient(affectation_9, agent_1);
		public Client client_10 = Const_Models.constClient(affectation_10, agent_1);
		public Client client_11 = Const_Models.constClient(affectation_11, agent_1);
		
		public Client client_12 = Const_Models.constClient(affectation_9, new Agent());
		public Client client_13 = Const_Models.constClient(new Affectation(), agent_2);
		public Client client_14 = Const_Models.constClient(new Affectation(), new Agent());

/* nullPointer */
		//public Client client_15 = Const_Models.constClient(affectation_10, null);
		//public Client client_16 = Const_Models.constClient(new Affectation(), null);
		//public Client client_17 = Const_Models.constClient(null, agent_1);
		//public Client client_18 = Const_Models.constClient(null, null);
		
	/* COMPTE BANCAIRE */

		// -------------------------------------------------------------------------------------	//Client_1 sans compte
		
		// Comptes Courants
		public CompteCourant ccp_20 = Const_Models.constCC("ccp_20", client_2, 999.99, 25000.00);	//Normal

/* nullPointeur */
		//public CompteCourant ccp_NL = Const_Models.constCC("ccp_NL", null, 100000.00, 6000.00);		//client null
		
		public CompteCourant ccp_10 = Const_Models.constCC("ccp_31", client_3, -1.00, 6000.00);		//decouvert negatif
		public CompteCourant ccp_01 = Const_Models.constCC("ccp_52", client_5, 0.00, -6000.00);		//revenu negatif
		public CompteCourant ccp_IC = Const_Models.constCC("ccp_IC", new Client(), 0.00, 6000.00);	//client inconnu
		public CompteCourant ccp_00 = Const_Models.constCC("ccp_00", client_4, 0.00, 0);			//valeur 0
		public CompteCourant ccp_UN = Const_Models.constCC("ccp_UN", client_6, 100,1200);			//client avec que un ccp
		public CompteCourant ccp_EX = Const_Models.constCC("ccp_EX", client_8, 40, 100);			//ccp du client Extern
		
/* Cascade-1 */ 
		//public CompteCourant ccp_CR = Const_Models.constCC("ccp_CR", client_9, 40, 100);			//ccp du client Crash
		
		// Comptes Remuneres
		public CompteRemunere cr_21 = Const_Models.constCR("cr_21", client_2);						//Normal
		public CompteRemunere cr_IC = Const_Models.constCR("cr_IC", new Client());					//client inconnu

/* nullPointeur */
		//public CompteRemunere cr_NL = Const_Models.constCR("cr_NL", null);							//client null
		
		public CompteRemunere cr_UN = Const_Models.constCR("cr_UN", client_7);						//client avec que un cr
		public CompteRemunere cr_EX = Const_Models.constCR("cr_EX", client_8);						//cr du client Crash

/* Cascade-1 */
		//public CompteRemunere cr_CR = Const_Models.constCR("cr_CR", client_9);						//cr du client Crash
		
		// Demandes Chequiers
		public DemandeChequier chequier_1 = Const_Models.constChequier(ccp_20);
/* Cascade-1 */
		//public DemandeChequier chequier_2 = Const_Models.constChequier(ccp_NL);
		
		public DemandeChequier chequier_3 = Const_Models.constChequier(ccp_10);
		public DemandeChequier chequier_4 = Const_Models.constChequier(ccp_01);
		
/* nullPointer: Agent de IC = null => can't update DemandeChequier in this Agent */
		//public DemandeChequier chequier_5 = Const_Models.constChequier(ccp_IC);
		
		public DemandeChequier chequier_6 = Const_Models.constChequier(ccp_00);
		public DemandeChequier chequier_7 = Const_Models.constChequier(ccp_UN);
		
/* nullPointer */
		//public DemandeChequier chequier_8 = Const_Models.constChequier(null);						//ccp null

/* nullPointer: Agent new Compte = null => can't update DemandeChequier in this Agent */
		//public DemandeChequier chequier_9 = Const_Models.constChequier(new CompteCourant());		//ccp inconnu
		
		// Transaction
		public Transaction transaction_C20101 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.agio);
		public Transaction transaction_C20102 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.agio);
		public Transaction transaction_C20103 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.remuneration);
		public Transaction transaction_C20104 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.remuneration);
		public Transaction transaction_C20105 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.credit_simple);
		public Transaction transaction_C20106 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.credit_simple);
		public Transaction transaction_C20107 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.debit_simple);
		public Transaction transaction_C20108 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.debit_simple);
		public Transaction transaction_C20109 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C20110 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C20111 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.reception_virement);
		public Transaction transaction_C20112 = Const_Models.constTransaction(ccp_20, 53, TypeOperation.reception_virement);

/* Cascade-1 */
		//public Transaction transaction_CNL101 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.agio);
		//public Transaction transaction_CNL102 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.agio);
		//public Transaction transaction_CNL103 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.remuneration);
		//public Transaction transaction_CNL104 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.remuneration);
		//public Transaction transaction_CNL105 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.credit_simple);
		//public Transaction transaction_CNL106 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.credit_simple);
		//public Transaction transaction_CNL107 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.debit_simple);
		//public Transaction transaction_CNL108 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.debit_simple);
		//public Transaction transaction_CNL109 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_CNL110 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_CNL111 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.reception_virement);
		//public Transaction transaction_CNL112 = Const_Models.constTransaction(ccp_NL, 53, TypeOperation.reception_virement);

		public Transaction transaction_C10101 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.agio);
		public Transaction transaction_C10102 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.agio);
		public Transaction transaction_C10103 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.remuneration);
		public Transaction transaction_C10104 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.remuneration);
		public Transaction transaction_C10105 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.credit_simple);
		public Transaction transaction_C10106 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.credit_simple);
		public Transaction transaction_C10107 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.debit_simple);
		public Transaction transaction_C10108 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.debit_simple);
		public Transaction transaction_C10109 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C10110 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C10111 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.reception_virement);
		public Transaction transaction_C10112 = Const_Models.constTransaction(ccp_10, 53, TypeOperation.reception_virement);

		public Transaction transaction_C01101 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.agio);
		public Transaction transaction_C01102 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.agio);
		public Transaction transaction_C01103 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.remuneration);
		public Transaction transaction_C01104 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.remuneration);
		public Transaction transaction_C01105 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.credit_simple);
		public Transaction transaction_C01106 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.credit_simple);
		public Transaction transaction_C01107 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.debit_simple);
		public Transaction transaction_C01108 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.debit_simple);
		public Transaction transaction_C01109 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C01110 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C01111 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.reception_virement);
		public Transaction transaction_C01112 = Const_Models.constTransaction(ccp_01, 53, TypeOperation.reception_virement);

		public Transaction transaction_CIC101 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.agio);
		public Transaction transaction_CIC102 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.agio);
		public Transaction transaction_CIC103 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.remuneration);
		public Transaction transaction_CIC104 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.remuneration);
		public Transaction transaction_CIC105 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.credit_simple);
		public Transaction transaction_CIC106 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.credit_simple);
		public Transaction transaction_CIC107 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.debit_simple);
		public Transaction transaction_CIC108 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.debit_simple);
		public Transaction transaction_CIC109 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.envoi_virement);
		public Transaction transaction_CIC110 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.envoi_virement);
		public Transaction transaction_CIC111 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.reception_virement);
		public Transaction transaction_CIC112 = Const_Models.constTransaction(ccp_IC, 53, TypeOperation.reception_virement);

		public Transaction transaction_C00101 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.agio);
		public Transaction transaction_C00102 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.agio);
		public Transaction transaction_C00103 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.remuneration);
		public Transaction transaction_C00104 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.remuneration);
		public Transaction transaction_C00105 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.credit_simple);
		public Transaction transaction_C00106 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.credit_simple);
		public Transaction transaction_C00107 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.debit_simple);
		public Transaction transaction_C00108 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.debit_simple);
		public Transaction transaction_C00109 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C00110 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.envoi_virement);
		public Transaction transaction_C00111 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.reception_virement);
		public Transaction transaction_C00112 = Const_Models.constTransaction(ccp_00, 53, TypeOperation.reception_virement);

		public Transaction transaction_CUN101 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.agio);
		public Transaction transaction_CUN102 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.agio);
		public Transaction transaction_CUN103 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.remuneration);
		public Transaction transaction_CUN104 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.remuneration);
		public Transaction transaction_CUN105 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.credit_simple);
		public Transaction transaction_CUN106 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.credit_simple);
		public Transaction transaction_CUN107 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.debit_simple);
		public Transaction transaction_CUN108 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.debit_simple);
		public Transaction transaction_CUN109 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.envoi_virement);
		public Transaction transaction_CUN110 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.envoi_virement);
		public Transaction transaction_CUN111 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.reception_virement);
		public Transaction transaction_CUN112 = Const_Models.constTransaction(ccp_UN, 53, TypeOperation.reception_virement);

		public Transaction transaction_R21101 = Const_Models.constTransaction(cr_21, 53, TypeOperation.agio);
		public Transaction transaction_R21102 = Const_Models.constTransaction(cr_21, 53, TypeOperation.agio);
		public Transaction transaction_R21103 = Const_Models.constTransaction(cr_21, 53, TypeOperation.remuneration);
		public Transaction transaction_R21104 = Const_Models.constTransaction(cr_21, 53, TypeOperation.remuneration);
		public Transaction transaction_R21105 = Const_Models.constTransaction(cr_21, 53, TypeOperation.credit_simple);
		public Transaction transaction_R21106 = Const_Models.constTransaction(cr_21, 53, TypeOperation.credit_simple);
		public Transaction transaction_R21107 = Const_Models.constTransaction(cr_21, 53, TypeOperation.debit_simple);
		public Transaction transaction_R21108 = Const_Models.constTransaction(cr_21, 53, TypeOperation.debit_simple);
		public Transaction transaction_R21109 = Const_Models.constTransaction(cr_21, 53, TypeOperation.envoi_virement);
		public Transaction transaction_R21110 = Const_Models.constTransaction(cr_21, 53, TypeOperation.envoi_virement);
		public Transaction transaction_R21111 = Const_Models.constTransaction(cr_21, 53, TypeOperation.reception_virement);
		public Transaction transaction_R21112 = Const_Models.constTransaction(cr_21, 53, TypeOperation.reception_virement);

		public Transaction transaction_RIC101 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.agio);
		public Transaction transaction_RIC102 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.agio);
		public Transaction transaction_RIC103 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.remuneration);
		public Transaction transaction_RIC104 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.remuneration);
		public Transaction transaction_RIC105 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.credit_simple);
		public Transaction transaction_RIC106 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.credit_simple);
		public Transaction transaction_RIC107 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.debit_simple);
		public Transaction transaction_RIC108 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.debit_simple);
		public Transaction transaction_RIC109 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.envoi_virement);
		public Transaction transaction_RIC110 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.envoi_virement);
		public Transaction transaction_RIC111 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.reception_virement);
		public Transaction transaction_RIC112 = Const_Models.constTransaction(cr_IC, 53, TypeOperation.reception_virement);

/* Cascade-1 */
		//public Transaction transaction_RNL101 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.agio);
		//public Transaction transaction_RNL102 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.agio);
		//public Transaction transaction_RNL103 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.remuneration);
		//public Transaction transaction_RNL104 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.remuneration);
		//public Transaction transaction_RNL105 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.credit_simple);
		//public Transaction transaction_RNL106 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.credit_simple);
		//public Transaction transaction_RNL107 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.debit_simple);
		//public Transaction transaction_RNL108 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.debit_simple);
		//public Transaction transaction_RNL109 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_RNL110 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_RNL111 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.reception_virement);
		//public Transaction transaction_RNL112 = Const_Models.constTransaction(cr_NL, 53, TypeOperation.reception_virement);

		public Transaction transaction_RUN101 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.agio);
		public Transaction transaction_RUN102 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.agio);
		public Transaction transaction_RUN103 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.remuneration);
		public Transaction transaction_RUN104 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.remuneration);
		public Transaction transaction_RUN105 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.credit_simple);
		public Transaction transaction_RUN106 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.credit_simple);
		public Transaction transaction_RUN107 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.debit_simple);
		public Transaction transaction_RUN108 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.debit_simple);
		public Transaction transaction_RUN109 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.envoi_virement);
		public Transaction transaction_RUN110 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.envoi_virement);
		public Transaction transaction_RUN111 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.reception_virement);
		public Transaction transaction_RUN112 = Const_Models.constTransaction(cr_UN, 53, TypeOperation.reception_virement);

/* nullPointeur */
		//public Transaction transaction_N101 = Const_Models.constTransaction(null, 53, TypeOperation.agio);
		//public Transaction transaction_N102 = Const_Models.constTransaction(null, 53, TypeOperation.agio);
		//public Transaction transaction_N103 = Const_Models.constTransaction(null, 53, TypeOperation.remuneration);
		//public Transaction transaction_N104 = Const_Models.constTransaction(null, 53, TypeOperation.remuneration);
		//public Transaction transaction_N105 = Const_Models.constTransaction(null, 53, TypeOperation.credit_simple);
		//public Transaction transaction_N106 = Const_Models.constTransaction(null, 53, TypeOperation.credit_simple);
		//public Transaction transaction_N107 = Const_Models.constTransaction(null, 53, TypeOperation.debit_simple);
		//public Transaction transaction_N108 = Const_Models.constTransaction(null, 53, TypeOperation.debit_simple);
		//public Transaction transaction_N109 = Const_Models.constTransaction(null, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_N110 = Const_Models.constTransaction(null, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_N111 = Const_Models.constTransaction(null, 53, TypeOperation.reception_virement);
		//public Transaction transaction_N112 = Const_Models.constTransaction(null, 53, TypeOperation.reception_virement);

		public Transaction transaction_IC101 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.agio);
		public Transaction transaction_IC102 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.agio);
		public Transaction transaction_IC103 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.remuneration);
		public Transaction transaction_IC104 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.remuneration);
		public Transaction transaction_IC105 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.credit_simple);
		public Transaction transaction_IC106 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.credit_simple);
		public Transaction transaction_IC107 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.debit_simple);
		public Transaction transaction_IC108 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.debit_simple);
		public Transaction transaction_IC109 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.envoi_virement);
		public Transaction transaction_IC110 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.envoi_virement);
		public Transaction transaction_IC111 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.reception_virement);
		public Transaction transaction_IC112 = Const_Models.constTransaction(new CompteCourant(), 53, TypeOperation.reception_virement);

		public Transaction transaction_IR101 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.agio);
		public Transaction transaction_IR102 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.agio);
		public Transaction transaction_IR103 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.remuneration);
		public Transaction transaction_IR104 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.remuneration);
		public Transaction transaction_IR105 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.credit_simple);
		public Transaction transaction_IR106 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.credit_simple);
		public Transaction transaction_IR107 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.debit_simple);
		public Transaction transaction_IR108 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.debit_simple);
		public Transaction transaction_IR109 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.envoi_virement);
		public Transaction transaction_IR110 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.envoi_virement);
		public Transaction transaction_IR111 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.reception_virement);
		public Transaction transaction_IR112 = Const_Models.constTransaction(new CompteRemunere(), 53, TypeOperation.reception_virement);
		
		public Transaction transaction_CEX101 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.agio);
		public Transaction transaction_CEX102 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.agio);
		public Transaction transaction_CEX103 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.remuneration);
		public Transaction transaction_CEX104 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.remuneration);
		public Transaction transaction_CEX105 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.credit_simple);
		public Transaction transaction_CEX106 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.credit_simple);
		public Transaction transaction_CEX107 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.debit_simple);
		public Transaction transaction_CEX108 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.debit_simple);
		public Transaction transaction_CEX109 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.envoi_virement);
		public Transaction transaction_CEX110 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.envoi_virement);
		public Transaction transaction_CEX111 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.reception_virement);
		public Transaction transaction_CEX112 = Const_Models.constTransaction(ccp_EX, 53, TypeOperation.reception_virement);

/* Cascade-2 */
		//public Transaction transaction_CCR101 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.agio);
		//public Transaction transaction_CCR102 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.agio);
		//public Transaction transaction_CCR103 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.remuneration);
		//public Transaction transaction_CCR104 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.remuneration);
		//public Transaction transaction_CCR105 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.credit_simple);
		//public Transaction transaction_CCR106 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.credit_simple);
		//public Transaction transaction_CCR107 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.debit_simple);
		//public Transaction transaction_CCR108 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.debit_simple);
		//public Transaction transaction_CCR109 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_CCR110 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_CCR111 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.reception_virement);
		//public Transaction transaction_CCR112 = Const_Models.constTransaction(ccp_CR, 53, TypeOperation.reception_virement);

		public Transaction transaction_REX101 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.agio);
		public Transaction transaction_REX102 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.agio);
		public Transaction transaction_REX103 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.remuneration);
		public Transaction transaction_REX104 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.remuneration);
		public Transaction transaction_REX105 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.credit_simple);
		public Transaction transaction_REX106 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.credit_simple);
		public Transaction transaction_REX107 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.debit_simple);
		public Transaction transaction_REX108 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.debit_simple);
		public Transaction transaction_REX109 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.envoi_virement);
		public Transaction transaction_REX110 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.envoi_virement);
		public Transaction transaction_REX111 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.reception_virement);
		public Transaction transaction_REX112 = Const_Models.constTransaction(cr_EX, 53, TypeOperation.reception_virement);

/* Cascade-2 */
		//public Transaction transaction_RCR101 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.agio);
		//public Transaction transaction_RCR102 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.agio);
		//public Transaction transaction_RCR103 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.remuneration);
		//public Transaction transaction_RCR104 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.remuneration);
		//public Transaction transaction_RCR105 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.credit_simple);
		//public Transaction transaction_RCR106 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.credit_simple);
		//public Transaction transaction_RCR107 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.debit_simple);
		//public Transaction transaction_RCR108 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.debit_simple);
		//public Transaction transaction_RCR109 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_RCR110 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.envoi_virement);
		//public Transaction transaction_RCR111 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.reception_virement);
		//public Transaction transaction_RCR112 = Const_Models.constTransaction(cr_CR, 53, TypeOperation.reception_virement);

		
		public Transaction transaction_C20001 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.agio);
		public Transaction transaction_C20002 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.agio);
		public Transaction transaction_C20003 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.remuneration);
		public Transaction transaction_C20004 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.remuneration);
		public Transaction transaction_C20005 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.credit_simple);
		public Transaction transaction_C20006 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.credit_simple);
		public Transaction transaction_C20007 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.debit_simple);
		public Transaction transaction_C20008 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.debit_simple);
		public Transaction transaction_C20009 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C20010 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C20011 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.reception_virement);
		public Transaction transaction_C20012 = Const_Models.constTransaction(ccp_20, 0, TypeOperation.reception_virement);

/* Cascade-1 */
		//public Transaction transaction_CNL001 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.agio);
		//public Transaction transaction_CNL002 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.agio);
		//public Transaction transaction_CNL003 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.remuneration);
		//public Transaction transaction_CNL004 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.remuneration);
		//public Transaction transaction_CNL005 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.credit_simple);
		//public Transaction transaction_CNL006 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.credit_simple);
		//public Transaction transaction_CNL007 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.debit_simple);
		//public Transaction transaction_CNL008 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.debit_simple);
		//public Transaction transaction_CNL009 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_CNL010 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_CNL011 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.reception_virement);
		//public Transaction transaction_CNL012 = Const_Models.constTransaction(ccp_NL, 0, TypeOperation.reception_virement);

		public Transaction transaction_C10001 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.agio);
		public Transaction transaction_C10002 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.agio);
		public Transaction transaction_C10003 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.remuneration);
		public Transaction transaction_C10004 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.remuneration);
		public Transaction transaction_C10005 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.credit_simple);
		public Transaction transaction_C10006 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.credit_simple);
		public Transaction transaction_C10007 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.debit_simple);
		public Transaction transaction_C10008 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.debit_simple);
		public Transaction transaction_C10009 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C10010 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C10011 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.reception_virement);
		public Transaction transaction_C10012 = Const_Models.constTransaction(ccp_10, 0, TypeOperation.reception_virement);

		public Transaction transaction_C0001 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.agio);
		public Transaction transaction_C01002 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.agio);
		public Transaction transaction_C01003 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.remuneration);
		public Transaction transaction_C01004 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.remuneration);
		public Transaction transaction_C01005 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.credit_simple);
		public Transaction transaction_C01006 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.credit_simple);
		public Transaction transaction_C01007 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.debit_simple);
		public Transaction transaction_C01008 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.debit_simple);
		public Transaction transaction_C01009 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C01010 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C01011 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.reception_virement);
		public Transaction transaction_C01012 = Const_Models.constTransaction(ccp_01, 0, TypeOperation.reception_virement);
		
		public Transaction transaction_CIC001 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.agio);
		public Transaction transaction_CIC002 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.agio);
		public Transaction transaction_CIC003 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.remuneration);
		public Transaction transaction_CIC004 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.remuneration);
		public Transaction transaction_CIC005 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.credit_simple);
		public Transaction transaction_CIC006 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.credit_simple);
		public Transaction transaction_CIC007 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.debit_simple);
		public Transaction transaction_CIC008 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.debit_simple);
		public Transaction transaction_CIC009 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.envoi_virement);
		public Transaction transaction_CIC010 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.envoi_virement);
		public Transaction transaction_CIC011 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.reception_virement);
		public Transaction transaction_CIC012 = Const_Models.constTransaction(ccp_IC, 0, TypeOperation.reception_virement);

		public Transaction transaction_C00001 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.agio);
		public Transaction transaction_C00002 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.agio);
		public Transaction transaction_C00003 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.remuneration);
		public Transaction transaction_C00004 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.remuneration);
		public Transaction transaction_C00005 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.credit_simple);
		public Transaction transaction_C00006 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.credit_simple);
		public Transaction transaction_C00007 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.debit_simple);
		public Transaction transaction_C00008 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.debit_simple);
		public Transaction transaction_C00009 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C00010 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.envoi_virement);
		public Transaction transaction_C00011 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.reception_virement);
		public Transaction transaction_C00012 = Const_Models.constTransaction(ccp_00, 0, TypeOperation.reception_virement);

		public Transaction transaction_CUN001 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.agio);
		public Transaction transaction_CUN002 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.agio);
		public Transaction transaction_CUN003 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.remuneration);
		public Transaction transaction_CUN004 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.remuneration);
		public Transaction transaction_CUN005 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.credit_simple);
		public Transaction transaction_CUN006 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.credit_simple);
		public Transaction transaction_CUN007 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.debit_simple);
		public Transaction transaction_CUN008 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.debit_simple);
		public Transaction transaction_CUN009 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.envoi_virement);
		public Transaction transaction_CUN010 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.envoi_virement);
		public Transaction transaction_CUN011 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.reception_virement);
		public Transaction transaction_CUN012 = Const_Models.constTransaction(ccp_UN, 0, TypeOperation.reception_virement);

		public Transaction transaction_R21001 = Const_Models.constTransaction(cr_21, 0, TypeOperation.agio);
		public Transaction transaction_R21002 = Const_Models.constTransaction(cr_21, 0, TypeOperation.agio);
		public Transaction transaction_R21003 = Const_Models.constTransaction(cr_21, 0, TypeOperation.remuneration);
		public Transaction transaction_R21004 = Const_Models.constTransaction(cr_21, 0, TypeOperation.remuneration);
		public Transaction transaction_R21005 = Const_Models.constTransaction(cr_21, 0, TypeOperation.credit_simple);
		public Transaction transaction_R21006 = Const_Models.constTransaction(cr_21, 0, TypeOperation.credit_simple);
		public Transaction transaction_R21007 = Const_Models.constTransaction(cr_21, 0, TypeOperation.debit_simple);
		public Transaction transaction_R21008 = Const_Models.constTransaction(cr_21, 0, TypeOperation.debit_simple);
		public Transaction transaction_R21009 = Const_Models.constTransaction(cr_21, 0, TypeOperation.envoi_virement);
		public Transaction transaction_R21010 = Const_Models.constTransaction(cr_21, 0, TypeOperation.envoi_virement);
		public Transaction transaction_R21011 = Const_Models.constTransaction(cr_21, 0, TypeOperation.reception_virement);
		public Transaction transaction_R21012 = Const_Models.constTransaction(cr_21, 0, TypeOperation.reception_virement);

		public Transaction transaction_RIC001 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.agio);
		public Transaction transaction_RIC002 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.agio);
		public Transaction transaction_RIC003 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.remuneration);
		public Transaction transaction_RIC004 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.remuneration);
		public Transaction transaction_RIC005 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.credit_simple);
		public Transaction transaction_RIC006 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.credit_simple);
		public Transaction transaction_RIC007 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.debit_simple);
		public Transaction transaction_RIC008 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.debit_simple);
		public Transaction transaction_RIC009 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.envoi_virement);
		public Transaction transaction_RIC010 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.envoi_virement);
		public Transaction transaction_RIC011 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.reception_virement);
		public Transaction transaction_RIC012 = Const_Models.constTransaction(cr_IC, 0, TypeOperation.reception_virement);

/* Cascade-1 */
		//public Transaction transaction_RNL001 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.agio);
		//public Transaction transaction_RNL002 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.agio);
		//public Transaction transaction_RNL003 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.remuneration);
		//public Transaction transaction_RNL004 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.remuneration);
		//public Transaction transaction_RNL005 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.credit_simple);
		//public Transaction transaction_RNL006 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.credit_simple);
		//public Transaction transaction_RNL007 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.debit_simple);
		//public Transaction transaction_RNL008 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.debit_simple);
		//public Transaction transaction_RNL009 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_RNL010 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_RNL011 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.reception_virement);
		//public Transaction transaction_RNL012 = Const_Models.constTransaction(cr_NL, 0, TypeOperation.reception_virement);

		public Transaction transaction_RUN001 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.agio);
		public Transaction transaction_RUN002 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.agio);
		public Transaction transaction_RUN003 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.remuneration);
		public Transaction transaction_RUN004 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.remuneration);
		public Transaction transaction_RUN005 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.credit_simple);
		public Transaction transaction_RUN006 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.credit_simple);
		public Transaction transaction_RUN007 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.debit_simple);
		public Transaction transaction_RUN008 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.debit_simple);
		public Transaction transaction_RUN009 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.envoi_virement);
		public Transaction transaction_RUN010 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.envoi_virement);
		public Transaction transaction_RUN011 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.reception_virement);
		public Transaction transaction_RUN012 = Const_Models.constTransaction(cr_UN, 0, TypeOperation.reception_virement);

/* nullPointer */
		//public Transaction transaction_N001 = Const_Models.constTransaction(null, 0, TypeOperation.agio);
		//public Transaction transaction_N002 = Const_Models.constTransaction(null, 0, TypeOperation.agio);
		//public Transaction transaction_N003 = Const_Models.constTransaction(null, 0, TypeOperation.remuneration);
		//public Transaction transaction_N004 = Const_Models.constTransaction(null, 0, TypeOperation.remuneration);
		//public Transaction transaction_N005 = Const_Models.constTransaction(null, 0, TypeOperation.credit_simple);
		//public Transaction transaction_N006 = Const_Models.constTransaction(null, 0, TypeOperation.credit_simple);
		//public Transaction transaction_N007 = Const_Models.constTransaction(null, 0, TypeOperation.debit_simple);
		//public Transaction transaction_N008 = Const_Models.constTransaction(null, 0, TypeOperation.debit_simple);
		//public Transaction transaction_N009 = Const_Models.constTransaction(null, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_N010 = Const_Models.constTransaction(null, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_N011 = Const_Models.constTransaction(null, 0, TypeOperation.reception_virement);
		//public Transaction transaction_N012 = Const_Models.constTransaction(null, 0, TypeOperation.reception_virement);

		public Transaction transaction_IC001 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.agio);
		public Transaction transaction_IC002 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.agio);
		public Transaction transaction_IC003 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.remuneration);
		public Transaction transaction_IC004 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.remuneration);
		public Transaction transaction_IC005 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.credit_simple);
		public Transaction transaction_IC006 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.credit_simple);
		public Transaction transaction_IC007 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.debit_simple);
		public Transaction transaction_IC008 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.debit_simple);
		public Transaction transaction_IC009 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.envoi_virement);
		public Transaction transaction_IC010 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.envoi_virement);
		public Transaction transaction_IC011 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.reception_virement);
		public Transaction transaction_IC012 = Const_Models.constTransaction(new CompteCourant(), 0, TypeOperation.reception_virement);

		public Transaction transaction_IR001 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.agio);
		public Transaction transaction_IR002 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.agio);
		public Transaction transaction_IR003 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.remuneration);
		public Transaction transaction_IR004 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.remuneration);
		public Transaction transaction_IR005 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.credit_simple);
		public Transaction transaction_IR006 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.credit_simple);
		public Transaction transaction_IR007 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.debit_simple);
		public Transaction transaction_IR008 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.debit_simple);
		public Transaction transaction_IR009 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.envoi_virement);
		public Transaction transaction_IR010 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.envoi_virement);
		public Transaction transaction_IR011 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.reception_virement);
		public Transaction transaction_IR012 = Const_Models.constTransaction(new CompteRemunere(), 0, TypeOperation.reception_virement);

		public Transaction transaction_CEX001 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.agio);
		public Transaction transaction_CEX002 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.agio);
		public Transaction transaction_CEX003 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.remuneration);
		public Transaction transaction_CEX004 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.remuneration);
		public Transaction transaction_CEX005 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.credit_simple);
		public Transaction transaction_CEX006 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.credit_simple);
		public Transaction transaction_CEX007 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.debit_simple);
		public Transaction transaction_CEX008 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.debit_simple);
		public Transaction transaction_CEX009 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.envoi_virement);
		public Transaction transaction_CEX010 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.envoi_virement);
		public Transaction transaction_CEX011 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.reception_virement);
		public Transaction transaction_CEX012 = Const_Models.constTransaction(ccp_EX, 0, TypeOperation.reception_virement);

/* Cascade-2 */
		//public Transaction transaction_CCR001 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.agio);
		//public Transaction transaction_CCR002 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.agio);
		//public Transaction transaction_CCR003 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.remuneration);
		//public Transaction transaction_CCR004 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.remuneration);
		//public Transaction transaction_CCR005 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.credit_simple);
		//public Transaction transaction_CCR006 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.credit_simple);
		//public Transaction transaction_CCR007 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.debit_simple);
		//public Transaction transaction_CCR008 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.debit_simple);
		//public Transaction transaction_CCR009 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_CCR010 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_CCR011 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.reception_virement);
		//public Transaction transaction_CCR012 = Const_Models.constTransaction(ccp_CR, 0, TypeOperation.reception_virement);

		public Transaction transaction_REX001 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.agio);
		public Transaction transaction_REX002 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.agio);
		public Transaction transaction_REX003 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.remuneration);
		public Transaction transaction_REX004 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.remuneration);
		public Transaction transaction_REX005 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.credit_simple);
		public Transaction transaction_REX006 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.credit_simple);
		public Transaction transaction_REX007 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.debit_simple);
		public Transaction transaction_REX008 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.debit_simple);
		public Transaction transaction_REX009 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.envoi_virement);
		public Transaction transaction_REX010 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.envoi_virement);
		public Transaction transaction_REX011 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.reception_virement);
		public Transaction transaction_REX012 = Const_Models.constTransaction(cr_EX, 0, TypeOperation.reception_virement);

/* Cascade-2 */
		//public Transaction transaction_RCR001 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.agio);
		//public Transaction transaction_RCR002 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.agio);
		//public Transaction transaction_RCR003 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.remuneration);
		//public Transaction transaction_RCR004 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.remuneration);
		//public Transaction transaction_RCR005 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.credit_simple);
		//public Transaction transaction_RCR006 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.credit_simple);
		//public Transaction transaction_RCR007 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.debit_simple);
		//public Transaction transaction_RCR008 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.debit_simple);
		//public Transaction transaction_RCR009 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_RCR010 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.envoi_virement);
		//public Transaction transaction_RCR011 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.reception_virement);
		//public Transaction transaction_RCR012 = Const_Models.constTransaction(cr_CR, 0, TypeOperation.reception_virement);

		
		public Transaction transaction_C20_01 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.agio);
		public Transaction transaction_C20_02 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.agio);
		public Transaction transaction_C20_03 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.remuneration);
		public Transaction transaction_C20_04 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.remuneration);
		public Transaction transaction_C20_05 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.credit_simple);
		public Transaction transaction_C20_06 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.credit_simple);
		public Transaction transaction_C20_07 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.debit_simple);
		public Transaction transaction_C20_08 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.debit_simple);
		public Transaction transaction_C20_09 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C20_10 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C20_11 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.reception_virement);
		public Transaction transaction_C20_12 = Const_Models.constTransaction(ccp_20, -1, TypeOperation.reception_virement);

/* Cascade-1 */
		//public Transaction transaction_CNL_01 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.agio);
		//public Transaction transaction_CNL_02 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.agio);
		//public Transaction transaction_CNL_03 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.remuneration);
		//public Transaction transaction_CNL_04 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.remuneration);
		//public Transaction transaction_CNL_05 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.credit_simple);
		//public Transaction transaction_CNL_06 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.credit_simple);
		//public Transaction transaction_CNL_07 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.debit_simple);
		//public Transaction transaction_CNL_08 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.debit_simple);
		//public Transaction transaction_CNL_09 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_CNL_10 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_CNL_11 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.reception_virement);
		//public Transaction transaction_CNL_12 = Const_Models.constTransaction(ccp_NL, -1, TypeOperation.reception_virement);

		public Transaction transaction_C10_01 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.agio);
		public Transaction transaction_C10_02 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.agio);
		public Transaction transaction_C10_03 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.remuneration);
		public Transaction transaction_C10_04 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.remuneration);
		public Transaction transaction_C10_05 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.credit_simple);
		public Transaction transaction_C10_06 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.credit_simple);
		public Transaction transaction_C10_07 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.debit_simple);
		public Transaction transaction_C10_08 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.debit_simple);
		public Transaction transaction_C10_09 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C10_10 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C10_11 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.reception_virement);
		public Transaction transaction_C10_12 = Const_Models.constTransaction(ccp_10, -1, TypeOperation.reception_virement);
		
		public Transaction transaction_C01_01 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.agio);
		public Transaction transaction_C01_02 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.agio);
		public Transaction transaction_C01_03 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.remuneration);
		public Transaction transaction_C01_04 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.remuneration);
		public Transaction transaction_C01_05 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.credit_simple);
		public Transaction transaction_C01_06 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.credit_simple);
		public Transaction transaction_C01_07 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.debit_simple);
		public Transaction transaction_C01_08 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.debit_simple);
		public Transaction transaction_C01_09 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C01_10 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C01_11 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.reception_virement);
		public Transaction transaction_C01_12 = Const_Models.constTransaction(ccp_01, -1, TypeOperation.reception_virement);
		
		public Transaction transaction_CIC_01 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.agio);
		public Transaction transaction_CIC_02 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.agio);
		public Transaction transaction_CIC_03 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.remuneration);
		public Transaction transaction_CIC_04 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.remuneration);
		public Transaction transaction_CIC_05 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.credit_simple);
		public Transaction transaction_CIC_06 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.credit_simple);
		public Transaction transaction_CIC_07 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.debit_simple);
		public Transaction transaction_CIC_08 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.debit_simple);
		public Transaction transaction_CIC_09 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.envoi_virement);
		public Transaction transaction_CIC_10 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.envoi_virement);
		public Transaction transaction_CIC_11 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.reception_virement);
		public Transaction transaction_CIC_12 = Const_Models.constTransaction(ccp_IC, -1, TypeOperation.reception_virement);

		public Transaction transaction_C00_01 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.agio);
		public Transaction transaction_C00_02 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.agio);
		public Transaction transaction_C00_03 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.remuneration);
		public Transaction transaction_C00_04 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.remuneration);
		public Transaction transaction_C00_05 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.credit_simple);
		public Transaction transaction_C00_06 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.credit_simple);
		public Transaction transaction_C00_07 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.debit_simple);
		public Transaction transaction_C00_08 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.debit_simple);
		public Transaction transaction_C00_09 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C00_10 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.envoi_virement);
		public Transaction transaction_C00_11 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.reception_virement);
		public Transaction transaction_C00_12 = Const_Models.constTransaction(ccp_00, -1, TypeOperation.reception_virement);

		public Transaction transaction_CUN_01 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.agio);
		public Transaction transaction_CUN_02 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.agio);
		public Transaction transaction_CUN_03 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.remuneration);
		public Transaction transaction_CUN_04 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.remuneration);
		public Transaction transaction_CUN_05 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.credit_simple);
		public Transaction transaction_CUN_06 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.credit_simple);
		public Transaction transaction_CUN_07 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.debit_simple);
		public Transaction transaction_CUN_08 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.debit_simple);
		public Transaction transaction_CUN_09 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.envoi_virement);
		public Transaction transaction_CUN_10 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.envoi_virement);
		public Transaction transaction_CUN_11 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.reception_virement);
		public Transaction transaction_CUN_12 = Const_Models.constTransaction(ccp_UN, -1, TypeOperation.reception_virement);

		public Transaction transaction_R21_01 = Const_Models.constTransaction(cr_21, -1, TypeOperation.agio);
		public Transaction transaction_R21_02 = Const_Models.constTransaction(cr_21, -1, TypeOperation.agio);
		public Transaction transaction_R21_03 = Const_Models.constTransaction(cr_21, -1, TypeOperation.remuneration);
		public Transaction transaction_R21_04 = Const_Models.constTransaction(cr_21, -1, TypeOperation.remuneration);
		public Transaction transaction_R21_05 = Const_Models.constTransaction(cr_21, -1, TypeOperation.credit_simple);
		public Transaction transaction_R21_06 = Const_Models.constTransaction(cr_21, -1, TypeOperation.credit_simple);
		public Transaction transaction_R21_07 = Const_Models.constTransaction(cr_21, -1, TypeOperation.debit_simple);
		public Transaction transaction_R21_08 = Const_Models.constTransaction(cr_21, -1, TypeOperation.debit_simple);
		public Transaction transaction_R21_09 = Const_Models.constTransaction(cr_21, -1, TypeOperation.envoi_virement);
		public Transaction transaction_R21_10 = Const_Models.constTransaction(cr_21, -1, TypeOperation.envoi_virement);
		public Transaction transaction_R21_11 = Const_Models.constTransaction(cr_21, -1, TypeOperation.reception_virement);
		public Transaction transaction_R21_12 = Const_Models.constTransaction(cr_21, -1, TypeOperation.reception_virement);

		public Transaction transaction_RIC_01 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.agio);
		public Transaction transaction_RIC_02 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.agio);
		public Transaction transaction_RIC_03 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.remuneration);
		public Transaction transaction_RIC_04 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.remuneration);
		public Transaction transaction_RIC_05 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.credit_simple);
		public Transaction transaction_RIC_06 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.credit_simple);
		public Transaction transaction_RIC_07 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.debit_simple);
		public Transaction transaction_RIC_08 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.debit_simple);
		public Transaction transaction_RIC_09 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.envoi_virement);
		public Transaction transaction_RIC_10 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.envoi_virement);
		public Transaction transaction_RIC_11 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.reception_virement);
		public Transaction transaction_RIC_12 = Const_Models.constTransaction(cr_IC, -1, TypeOperation.reception_virement);
		
/* Cascade-1 */
		//public Transaction transaction_RNL_01 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.agio);
		//public Transaction transaction_RNL_02 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.agio);
		//public Transaction transaction_RNL_03 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.remuneration);
		//public Transaction transaction_RNL_04 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.remuneration);
		//public Transaction transaction_RNL_05 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.credit_simple);
		//public Transaction transaction_RNL_06 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.credit_simple);
		//public Transaction transaction_RNL_07 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.debit_simple);
		//public Transaction transaction_RNL_08 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.debit_simple);
		//public Transaction transaction_RNL_09 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_RNL_10 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_RNL_11 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.reception_virement);
		//public Transaction transaction_RNL_12 = Const_Models.constTransaction(cr_NL, -1, TypeOperation.reception_virement);

		public Transaction transaction_RUN_01 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.agio);
		public Transaction transaction_RUN_02 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.agio);
		public Transaction transaction_RUN_03 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.remuneration);
		public Transaction transaction_RUN_04 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.remuneration);
		public Transaction transaction_RUN_05 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.credit_simple);
		public Transaction transaction_RUN_06 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.credit_simple);
		public Transaction transaction_RUN_07 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.debit_simple);
		public Transaction transaction_RUN_08 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.debit_simple);
		public Transaction transaction_RUN_09 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.envoi_virement);
		public Transaction transaction_RUN_10 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.envoi_virement);
		public Transaction transaction_RUN_11 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.reception_virement);
		public Transaction transaction_RUN_12 = Const_Models.constTransaction(cr_UN, -1, TypeOperation.reception_virement);
		
/* nullPointer */
		//public Transaction transaction_N_01 = Const_Models.constTransaction(null, -1, TypeOperation.agio);
		//public Transaction transaction_N_02 = Const_Models.constTransaction(null, -1, TypeOperation.agio);
		//public Transaction transaction_N_03 = Const_Models.constTransaction(null, -1, TypeOperation.remuneration);
		//public Transaction transaction_N_04 = Const_Models.constTransaction(null, -1, TypeOperation.remuneration);
		//public Transaction transaction_N_05 = Const_Models.constTransaction(null, -1, TypeOperation.credit_simple);
		//public Transaction transaction_N_06 = Const_Models.constTransaction(null, -1, TypeOperation.credit_simple);
		//public Transaction transaction_N_07 = Const_Models.constTransaction(null, -1, TypeOperation.debit_simple);
		//public Transaction transaction_N_08 = Const_Models.constTransaction(null, -1, TypeOperation.debit_simple);
		//public Transaction transaction_N_09 = Const_Models.constTransaction(null, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_N_10 = Const_Models.constTransaction(null, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_N_11 = Const_Models.constTransaction(null, -1, TypeOperation.reception_virement);
		//public Transaction transaction_N_12 = Const_Models.constTransaction(null, -1, TypeOperation.reception_virement);

		public Transaction transaction_IC_01 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.agio);
		public Transaction transaction_IC_02 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.agio);
		public Transaction transaction_IC_03 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.remuneration);
		public Transaction transaction_IC_04 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.remuneration);
		public Transaction transaction_IC_05 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.credit_simple);
		public Transaction transaction_IC_06 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.credit_simple);
		public Transaction transaction_IC_07 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.debit_simple);
		public Transaction transaction_IC_08 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.debit_simple);
		public Transaction transaction_IC_09 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.envoi_virement);
		public Transaction transaction_IC_10 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.envoi_virement);
		public Transaction transaction_IC_11 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.reception_virement);
		public Transaction transaction_IC_12 = Const_Models.constTransaction(new CompteCourant(), -1, TypeOperation.reception_virement);

		public Transaction transaction_IR_01 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.agio);
		public Transaction transaction_IR_02 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.agio);
		public Transaction transaction_IR_03 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.remuneration);
		public Transaction transaction_IR_04 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.remuneration);
		public Transaction transaction_IR_05 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.credit_simple);
		public Transaction transaction_IR_06 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.credit_simple);
		public Transaction transaction_IR_07 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.debit_simple);
		public Transaction transaction_IR_08 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.debit_simple);
		public Transaction transaction_IR_09 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.envoi_virement);
		public Transaction transaction_IR_10 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.envoi_virement);
		public Transaction transaction_IR_11 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.reception_virement);
		public Transaction transaction_IR_12 = Const_Models.constTransaction(new CompteRemunere(), -1, TypeOperation.reception_virement);

		public Transaction transaction_CEX_01 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.agio);
		public Transaction transaction_CEX_02 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.agio);
		public Transaction transaction_CEX_03 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.remuneration);
		public Transaction transaction_CEX_04 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.remuneration);
		public Transaction transaction_CEX_05 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.credit_simple);
		public Transaction transaction_CEX_06 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.credit_simple);
		public Transaction transaction_CEX_07 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.debit_simple);
		public Transaction transaction_CEX_08 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.debit_simple);
		public Transaction transaction_CEX_09 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.envoi_virement);
		public Transaction transaction_CEX_10 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.envoi_virement);
		public Transaction transaction_CEX_11 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.reception_virement);
		public Transaction transaction_CEX_12 = Const_Models.constTransaction(ccp_EX, -1, TypeOperation.reception_virement);

/* Cascade-2 */
		//public Transaction transaction_CCR_01 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.agio);
		//public Transaction transaction_CCR_02 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.agio);
		//public Transaction transaction_CCR_03 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.remuneration);
		//public Transaction transaction_CCR_04 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.remuneration);
		//public Transaction transaction_CCR_05 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.credit_simple);
		//public Transaction transaction_CCR_06 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.credit_simple);
		//public Transaction transaction_CCR_07 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.debit_simple);
		//public Transaction transaction_CCR_08 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.debit_simple);
		//public Transaction transaction_CCR_09 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_CCR_10 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_CCR_11 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.reception_virement);
		//public Transaction transaction_CCR_12 = Const_Models.constTransaction(ccp_CR, -1, TypeOperation.reception_virement);

		public Transaction transaction_REX_01 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.agio);
		public Transaction transaction_REX_02 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.agio);
		public Transaction transaction_REX_03 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.remuneration);
		public Transaction transaction_REX_04 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.remuneration);
		public Transaction transaction_REX_05 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.credit_simple);
		public Transaction transaction_REX_06 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.credit_simple);
		public Transaction transaction_REX_07 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.debit_simple);
		public Transaction transaction_REX_08 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.debit_simple);
		public Transaction transaction_REX_09 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.envoi_virement);
		public Transaction transaction_REX_10 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.envoi_virement);
		public Transaction transaction_REX_11 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.reception_virement);
		public Transaction transaction_REX_12 = Const_Models.constTransaction(cr_EX, -1, TypeOperation.reception_virement);

/* Cascade-2 */
		//public Transaction transaction_RCR_01 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.agio);
		//public Transaction transaction_RCR_02 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.agio);
		//public Transaction transaction_RCR_03 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.remuneration);
		//public Transaction transaction_RCR_04 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.remuneration);
		//public Transaction transaction_RCR_05 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.credit_simple);
		//public Transaction transaction_RCR_06 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.credit_simple);
		//public Transaction transaction_RCR_07 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.debit_simple);
		//public Transaction transaction_RCR_08 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.debit_simple);
		//public Transaction transaction_RCR_09 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_RCR_10 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.envoi_virement);
		//public Transaction transaction_RCR_11 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.reception_virement);
		//public Transaction transaction_RCR_12 = Const_Models.constTransaction(cr_CR, -1, TypeOperation.reception_virement);

}