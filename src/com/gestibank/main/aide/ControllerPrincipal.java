package com.gestibank.main.aide;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.AdministrateurDAO;
import com.gestibank.dao.interfaces.CompteCourantDAO;
import com.gestibank.dao.interfaces.TransactionDAO;
import com.gestibank.enumerations.TypeOperation;
import com.gestibank.models.Administrateur;

import com.gestibank.models.CompteCourant;
import com.gestibank.models.Transaction;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/CP")

public class ControllerPrincipal {
	@Autowired
	TransactionDAO transactionDAO;
	@Autowired
	CompteCourantDAO compteCourantDAO;
	@Autowired
	AdministrateurDAO administrateurDAO;
	
	
	@GetMapping("/Test1")
	public String Test1()
	{

		CompteCourant compteCourant = new CompteCourant();
		
		//compteCourant.setRib("0000000001");
		compteCourant.setDecouvert(100);
		compteCourant.setEntreeMoyenneMensuelle(10000);
		compteCourant.setMontantInteret(0.05);
		compteCourant.setNbJoursPostDecouvert(10);
		compteCourantDAO.createCompteCourant(compteCourant);
		
		Transaction transaction = new Transaction();
		transaction.setCompte(compteCourant);
		transaction.setMontant(2050);
		transaction.setTypeOperation(TypeOperation.credit_simple);
		Boolean reponse = transactionDAO.createTransaction(transaction);
		System.out.println("Transaction creation = " + reponse);


		// List<Transaction> liste = new ArrayList();

		// Boolean reponse = tr_Imp.deleteTransaction(44);


		//AdministrateurDAO_Impl administrateurDAO_Impl = new AdministrateurDAO_Impl();

		Administrateur administrateur = new Administrateur();

		administrateur.setNom("Zinedine");
		administrateur.setPrenom("Zidane");
		administrateur.setLogin("Veronique");
		administrateur.setMail("zizou@gmail.com");
		administrateur.setTel("0542155650");

		Boolean resultat = administrateurDAO.createAdministrateur(administrateur);
		System.out.println("Administrateur creation = " + resultat);
		
		return "ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
