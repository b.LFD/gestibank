package com.gestibank.main.dao;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.AffectationDAO;
import com.gestibank.enumerations.SituationMatrimoniale;
import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.TypeTriAffectionAdmin;
import com.gestibank.main._environnement.Const_Models;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.models.Affectation;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_AffectationDAO_1")
public class TEST_AffectationDAO_1 {
	@Autowired
	AffectationDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;

	@GetMapping("/createAffectation")
	public String createAffectation()
	{
		trueExecute = objetTest.createAffectation(env.affectation_2);
		System.out.println("Le retour de trueCreate est: "+trueExecute);

		return trueExecute?"ok":"pas ok";
	}

	@GetMapping("/readAffectationById")
	public String readAffectationById()
	{
		System.out.println("Entr�e dans readAffectationById()" );
		Affectation affectation = env.affectation_5;
		objetTest.createAffectation(affectation);  //le create est une methode que l'on imagine deja ok
		int id = affectation.getId();
		System.out.println("mon affectation id = " + id);
		Affectation rendu = objetTest.readAffectationById(id);
		//System.out.println("/n-------------- Ton Affectation --------------/n"+affectation.toString());
		System.out.println("/n-------------- Ton Affectation --------------/n"+affectation.getId());
		System.out.println();
		//System.out.println("/n-------------- Son Affectation --------------/n"+rendu.toString());
		System.out.println("/n-------------- Son Affectation --------------/n"+rendu.getId());

		return (affectation.getId() == rendu.getId())?"ok":"pas ok";
	}

	@GetMapping("/updateAffectation")
	public String updateAffectation()
	{
		//en supposant que l'env.affectation_1 soit dans la table (id: ?)
		Affectation affectation = objetTest.readAffectationById(2);  //le read est une methode que l'on imagine deja ok
		//System.out.println("/n-------------- Avant --------------/n"+affectation.toString());
		System.out.println("/n-------------- Avant --------------/n"+affectation.getMail());
		System.out.println();
		affectation.setMail("new_mail@free.com");
		trueExecute = objetTest.updateAffectation(affectation);
		affectation = objetTest.readAffectationById(affectation.getId());
		//System.out.println("/n-------------- Apres --------------/n"+affectation.toString());
		System.out.println("/n-------------- Apres --------------/n"+affectation.getMail());

		return trueExecute?"ok":"pas ok";
	}

	@GetMapping("/deleteAffectationById")
	public String deleteAffectationById()
	{
		//en supposant que l'env.affectation_1 soit dans la table (id: ?)
		Affectation affectation = objetTest.readAffectationById(13);  //le read est une methode que l'on imagine deja ok
		System.out.println();
		trueExecute = objetTest.deleteAffectationById(affectation.getId());
		try {
			objetTest.readAffectationById(affectation.getId());
		} catch(Exception e) {
			System.out.println("Impossible de retrouver cette affectation");
		}

		return trueExecute?"ok":"pas ok";
	}

	@GetMapping("/readAllAffectations")
	public String readAllAffectations()
	{
		trueExecute = false;
		objetTest.createAffectation(env.affectation_1);
		objetTest.createAffectation(env.affectation_2);
		env.affectation_2.setStatut(StatutAffectation.accepte);
		objetTest.updateAffectation(env.affectation_2);
		objetTest.createAffectation(env.affectation_3);
		objetTest.createAffectation(env.affectation_4);
		env.affectation_4.setStatut(StatutAffectation.refuse);
		objetTest.updateAffectation(env.affectation_4);
		objetTest.createAffectation(env.affectation_5);
		objetTest.createAffectation(env.affectation_6);
		objetTest.createAffectation(env.affectation_7);
		objetTest.createAffectation(env.affectation_8);
		env.affectation_8.setStatut(StatutAffectation.en_attente_agent);
		objetTest.updateAffectation(env.affectation_8);
		objetTest.createAffectation(env.affectation_9);
		//objetTest.createAffectation(env.affectation_10);
		//objetTest.createAffectation(env.affectation_11);
		objetTest.createAffectation(Const_Models.constAffectation("Bob", "Marley", "bonnew@wanadoo.com", "Jamaique", "", new Date(), SituationMatrimoniale.celibataire, env.admin_0));
		try {
			System.out.println("Entr�e dans TestAffectationDao readAllAffectations() try");
			List<Affectation> listAffect = objetTest.readAllAffectations();
			for(int i=0; i!=listAffect.size(); i++) {
				System.out.println("         Affectation_"+listAffect.get(i).getId()+":  -  "+listAffect.get(i).getNom()+", "+listAffect.get(i).getPrenom()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les affectations");
		}

		return trueExecute?"ok":"pas ok";
	}

	@GetMapping("/readAllAffectationsByStatut")
	public String readAllAffectationsByStatut()
	{
		trueExecute = false;
		/*
		 * objetTest.createAffectation(env.affectation_1);
		objetTest.createAffectation(env.affectation_2);
		env.affectation_2.setStatut(StatutAffectation.accepte);
		objetTest.updateAffectation(env.affectation_2);
		objetTest.createAffectation(env.affectation_3);
		objetTest.createAffectation(env.affectation_4);
		env.affectation_4.setStatut(StatutAffectation.refuse);
		objetTest.updateAffectation(env.affectation_4);
		objetTest.createAffectation(env.affectation_5);
		objetTest.createAffectation(env.affectation_6);
		objetTest.createAffectation(env.affectation_7);
		objetTest.createAffectation(env.affectation_8);
		env.affectation_8.setStatut(StatutAffectation.en_attente_agent);
		objetTest.updateAffectation(env.affectation_8);
		objetTest.createAffectation(env.affectation_9);
		objetTest.createAffectation(env.affectation_10);
		objetTest.createAffectation(env.affectation_11);
		objetTest.createAffectation(Const_Models.constAffectation("Bob", "Marley", "bonnew@wanadoo.com", "Jamaique", "", new Date(), SituationMatrimoniale.celibataire, env.admin_0));*/
		try {
        	System.out.println("Entr�e dans TestAffectationDao readAllAffectationsByStatut() try");

			List<Affectation> listAffect = objetTest.readAllAffectationsByStatut(StatutAffectation.en_attente_admin);
			for(int i=0; i!=listAffect.size(); i++) {
				System.out.println("TestAffectationDao readAllAffectationsByStatut() try boucle liste");
				System.out.println("         Affectation_"+listAffect.get(i).getId()+":  -  "+listAffect.get(i).getNom()+", "+listAffect.get(i).getPrenom()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les affectations");
		}

		return trueExecute?"ok":"pas ok";
	}

	@GetMapping("/readAllAffectationsOrderByTypeTriAffectationAdmin")
	public String readAllAffectationsOrderByTypeTriAffectationAdmin()
	{
		trueExecute = false;
		/*
		 * objetTest.createAffectation(env.affectation_1);
		objetTest.createAffectation(env.affectation_2);
		env.affectation_2.setStatut(StatutAffectation.accepte);
		objetTest.updateAffectation(env.affectation_2);
		objetTest.createAffectation(env.affectation_3);
		objetTest.createAffectation(env.affectation_4);
		env.affectation_4.setStatut(StatutAffectation.refuse);
		objetTest.updateAffectation(env.affectation_4);
		objetTest.createAffectation(env.affectation_5);
		objetTest.createAffectation(env.affectation_6);
		objetTest.createAffectation(env.affectation_7);
		objetTest.createAffectation(env.affectation_8);
		env.affectation_8.setStatut(StatutAffectation.en_attente_agent);
		objetTest.updateAffectation(env.affectation_8);
		objetTest.createAffectation(env.affectation_9);
		objetTest.createAffectation(env.affectation_10);
		objetTest.createAffectation(env.affectation_11);
		objetTest.createAffectation(Const_Models.constAffectation("Bob", "Marley", "bonnew@wanadoo.com", "Jamaique", "", new Date(), SituationMatrimoniale.celibataire, env.admin_0));
		*/
		try {
			List<Affectation> listAffect = objetTest.readAllAffectationsOrderByTypeTriAffectationAdmin(TypeTriAffectionAdmin.date_demande);
			for(int i=0; i!=listAffect.size(); i++) {
				System.out.println("         Affectation_"+listAffect.get(i).getId()+":  -  "+listAffect.get(i).getNom()+", "+listAffect.get(i).getPrenom()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les administrateurs");
		}

		return trueExecute?"ok":"pas ok";
	}

	@GetMapping("/readAllAffectationsByStatutOrderByTypeTriAffectationAdmin")
	public String readAllAffectationsByStatutOrderByTypeTriAffectationAdmin()
	{
		trueExecute = false;
		/*objetTest.createAffectation(env.affectation_1);
		objetTest.createAffectation(env.affectation_2);
		env.affectation_2.setStatut(StatutAffectation.accepte);
		objetTest.updateAffectation(env.affectation_2);
		objetTest.createAffectation(env.affectation_3);
		objetTest.createAffectation(env.affectation_4);
		env.affectation_4.setStatut(StatutAffectation.refuse);
		objetTest.updateAffectation(env.affectation_4);
		objetTest.createAffectation(env.affectation_5);
		objetTest.createAffectation(env.affectation_6);
		objetTest.createAffectation(env.affectation_7);
		objetTest.createAffectation(env.affectation_8);
		env.affectation_8.setStatut(StatutAffectation.en_attente_agent);
		objetTest.updateAffectation(env.affectation_8);
		objetTest.createAffectation(env.affectation_9);
		objetTest.createAffectation(env.affectation_10);
		objetTest.createAffectation(env.affectation_11);
		objetTest.createAffectation(Const_Models.constAffectation("Bob", "Marley", "bonnew@wanadoo.com", "Jamaique", "", new Date(), SituationMatrimoniale.celibataire, env.admin_0));
		*/
		try {
			List<Affectation> listAffect = objetTest.readAllAffectations();
			for(int i=0; i!=listAffect.size(); i++) {
				System.out.println("         Affectation_"+listAffect.get(i).getId()+":  -  "+listAffect.get(i).getNom()+", "+listAffect.get(i).getPrenom()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les administrateurs");
		}

		return trueExecute?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
