package com.gestibank.main.dao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.CompteRemunereDAO;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_CompteRemunereDAO_1")
public class TEST_CompteRemunereDAO_1 {
	@Autowired
	CompteRemunereDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createCompteRemunere")
	public String createCompteRemunere()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readCompteRemunereByRib")
	public String readCompteRemunereByRib()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateCompteRemunere")
	public String updateCompteRemunere()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/deleteCompteRemunereByRib")
	public String deleteCompteRemunereByRib()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readAllCompteRemuneres")
	public String readAllCompteRemuneres()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readAllCompteRemuneresByLogin")
	public String readAllCompteRemuneresByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
