package com.gestibank.main.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.AgentDAO;
import com.gestibank.main._environnement.Const_Models;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.models.Agent;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_AgentDAO_1")
public class TEST_AgentDAO_1 {
	@Autowired
	AgentDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createAgent")
	public String createAgent()
	{
		trueExecute = objetTest.createAgent(env.agent_1);
		System.out.println("Le retour de trueCreate est: "+trueExecute);
		
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAgentByMatricule")
	public String readAgentByMatricule()
	{
		Agent appelant = env.agent_1;
		objetTest.createAgent(appelant);  //le create est une methode que l'on imagine deja ok
		String login = appelant.getMatricule();
		Agent rendu = objetTest.readAgentByMatricule(login);
		System.out.println("/n-------------- Ton Profil --------------/n"+appelant.toString());
		System.out.println();
		System.out.println("/n-------------- Son Profil --------------/n"+rendu.toString());
		
		return (appelant.toString().equals(rendu.toString()))?"ok":"pas ok";
	}
	
	@GetMapping("/updateAgent")
	public String updateAgent()
	{
		//en supposant que l'env.agent_1 soit dans la table (matricule: agent-1)
		Agent agent = objetTest.readAgentByMatricule("agent-1");  //le read est une methode que l'on imagine deja ok
		System.out.println("/n-------------- Avant --------------/n"+agent.toString());
		System.out.println();
		agent.setMail("new_mail@wahoo.com");
		trueExecute = objetTest.updateAgent(agent);
		agent = objetTest.readAgentByMatricule(agent.getMatricule());
		System.out.println("/n-------------- Apres --------------/n"+agent.toString());
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/deleteAgentByMatricule")
	public String deleteAgentByMatricule()
	{
		//en supposant que l'env.agent_1 soit dans la table (matricule: agent-1)
				Agent agent = objetTest.readAgentByMatricule("agent-1");  //le read est une methode que l'on imagine deja ok
				System.out.println();
				trueExecute = objetTest.deleteAgentByMatricule(agent.getMatricule());
				try {
					objetTest.readAgentByMatricule(agent.getMatricule());
				} catch(Exception e) {
					System.out.println("Impossible de retrouver cet agent");
				}
				
				return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAllAgents")
	public String readAllAgents()
	{
		trueExecute = false;
		objetTest.createAgent(env.agent_1);
		objetTest.createAgent(env.agent_2);
		objetTest.createAgent(env.agent_3);
		objetTest.createAgent(env.agent_4);
		objetTest.createAgent(env.agent_5);
		objetTest.createAgent(env.agent_6);
		objetTest.createAgent(Const_Models.constAgent("J.S.", "Smith", "John", "john.smith@hatlook.com", "06.75.69.12.38", env.admin_0));
		try {
			List<Agent> listAgent = objetTest.readAllAgents();
			for(int i=0; i!=listAgent.size(); i++) {
				System.out.println("              Agent_"+(i+1)+": "+listAgent.get(i).getMatricule()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les administrateurs");
		}
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAllAgentsByLoginAdmin")
	public String readAllAgentsByLoginAdmin()
	{
		trueExecute = false;
		objetTest.createAgent(env.agent_1);
		objetTest.createAgent(env.agent_2);
		objetTest.createAgent(env.agent_3);
		objetTest.createAgent(env.agent_4);
		objetTest.createAgent(env.agent_5);
		objetTest.createAgent(env.agent_6);
		objetTest.createAgent(Const_Models.constAgent("J.S.", "Smith", "John", "john.smith@hatlook.com", "06.75.69.12.38", env.admin_0));
		try {
			System.out.println("On recherche les agents sous la tutelle de l'amin <<admin-0>>");
			List<Agent> listAgent = objetTest.readAllAgentsByLoginAdmin(env.admin_0.getLogin());
			for(int i=0; i!=listAgent.size(); i++) {
				System.out.println("              Agent_"+(i+1)+": "+listAgent.get(i).getMatricule()+", affilie a: "+listAgent.get(i).getAdminAffecte().getLogin()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les administrateurs");
		}
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/searchAllAgentsByLoginAdmin")
	public String searchAllAgentsByLoginAdmin()
	{
		trueExecute = false;
		objetTest.createAgent(env.agent_1);
		objetTest.createAgent(env.agent_2);
		objetTest.createAgent(env.agent_3);
		objetTest.createAgent(env.agent_4);
		objetTest.createAgent(env.agent_5);
		objetTest.createAgent(env.agent_6);
		objetTest.createAgent(Const_Models.constAgent("S.J.", "Mith", "John", "john.smith@hatlook.com", "06.75.69.12.38", env.admin_0));
		try {
			System.out.println("On recherche les agents sous la tutelle de l'amin <<admin-0>> avec un nom ou un matricule commencant par S");
			List<Agent> listAgent = objetTest.searchAllAgentsByLoginAdmin(env.admin_0.getLogin(), "S");
			for(int i=0; i!=listAgent.size(); i++) {
				System.out.println("              Agent_"+(i+1)+": "+listAgent.get(i).getMatricule()+" - "+listAgent.get(i).getNom()+", affilie a: "+listAgent.get(i).getAdminAffecte().getLogin()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les administrateurs");
		}
		
		return trueExecute?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}