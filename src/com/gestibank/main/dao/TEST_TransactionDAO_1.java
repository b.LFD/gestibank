package com.gestibank.main.dao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.TransactionDAO;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_TransactionDAO_1")
public class TEST_TransactionDAO_1 {
	@Autowired
	TransactionDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createTransaction")
	public String createTransaction()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readTransactionById")
	public String readTransactionById()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateTransaction")
	public String updateTransaction()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/deleteTransaction")
	public String deleteTransaction()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readAllTransaction")
	public String readAllTransaction()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readAllTransactionByRibCompteCourant")
	public String readAllTransactionByRibCompteCourant()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readAllTransactionByRibCompteRemunere")
	public String readAllTransactionByRibCompteRemunere()
	{	
		return (true)?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
