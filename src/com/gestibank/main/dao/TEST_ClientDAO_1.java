package com.gestibank.main.dao;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.ClientDAO;
import com.gestibank.enumerations.SituationMatrimoniale;
import com.gestibank.main._environnement.Const_Models;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.models.Client;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_ClientDAO_1")
public class TEST_ClientDAO_1 {
	@Autowired
	ClientDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createClient")
	public String createClient()
	{
		trueExecute = objetTest.createClient(env.client_1);
		System.out.println("Le retour de trueCreate est: "+trueExecute);
		
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readClientByLogin")
	public String readClientByLogin()
	{
		Client appelant = env.client_1;
		objetTest.createClient(appelant);  //le create est une methode que l'on imagine deja ok
		String login = appelant.getLogin();
		Client rendu = objetTest.readClientByLogin(login);
		System.out.println("/n-------------- Ton Profil --------------/n"+appelant.toString());
		System.out.println();
		System.out.println("/n-------------- Son Profil --------------/n"+rendu.toString());
		
		return (appelant.toString().equals(rendu.toString()))?"ok":"pas ok";
	}
	
	@GetMapping("/updateClient")
	public String updateClient()
	{
		//en supposant que l'env.client_1 soit dans la table (login: client-1)
		Client client = objetTest.readClientByLogin("client-1");  //le read est une methode que l'on imagine deja ok
		System.out.println("/n-------------- Avant --------------/n"+client.toStringTest());
		System.out.println();
		client.setMail("new_mail@wahoo.com");
		trueExecute = objetTest.updateClient(client);
		client = objetTest.readClientByLogin(client.getLogin());
		System.out.println("/n-------------- Apres --------------/n"+client.toString());
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/deleteClientByLogin")
	public String deleteClientByLogin()
	{
		//en supposant que l'env.client_1 soit dans la table (login: client-1)
		Client client = objetTest.readClientByLogin("client-1");  //le read est une methode que l'on imagine deja ok
		System.out.println();
		trueExecute = objetTest.deleteClientByLogin(client.getLogin());
		try {
			objetTest.readClientByLogin(client.getLogin());
		} catch(Exception e) {
			System.out.println("Impossible de retrouver ce client");
		}
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAllClients")
	public String readAllClients()
	{
		trueExecute = false;
		objetTest.createClient(env.client_1);
		objetTest.createClient(env.client_2);
		objetTest.createClient(env.client_3);
		objetTest.createClient(env.client_4);
		objetTest.createClient(env.client_5);
		objetTest.createClient(env.client_6);
		objetTest.createClient(env.client_7);
		objetTest.createClient(env.client_8);
		Affectation nouvelleAffect = Const_Models.constAffectation("Paddle", "Kid", "univers_bd@hastore.com", "65 page BDjeune", "", new Date(), SituationMatrimoniale.veuf, env.admin_0);
		objetTest.createClient(Const_Models.constClient(nouvelleAffect, env.agent_2));
		try {
			List<Client> listClients = objetTest.readAllClients();
			for(int i=0; i!=listClients.size(); i++) {
				System.out.println("              Client"+i+": "+listClients.get(i).getLogin()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les clients");
		}
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAllClientsByMatricule")
	public String readAllClientsByMatricule()
	{
		trueExecute = false;
		objetTest.createClient(env.client_1);
		objetTest.createClient(env.client_2);
		objetTest.createClient(env.client_3);
		objetTest.createClient(env.client_4);
		objetTest.createClient(env.client_5);
		objetTest.createClient(env.client_6);
		objetTest.createClient(env.client_7);
		objetTest.createClient(env.client_8);
		Affectation nouvelleAffect = Const_Models.constAffectation("Paddle", "Kid", "univers_bd@hastore.com", "65 page BDjeune", "", new Date(), SituationMatrimoniale.veuf, env.admin_0);
		objetTest.createClient(Const_Models.constClient(nouvelleAffect, env.agent_2));
		try {
			List<Client> listClients = objetTest.readAllClientsByMatricule(env.agent_3.getMatricule());
			for(int i=0; i!=listClients.size(); i++) {
				System.out.println("              Client"+i+": "+listClients.get(i).getLogin()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les clients");
		}
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/searchAllClientsByMatricule")
	public String searchAllClientsByMatricule() {
		trueExecute = false;
		objetTest.createClient(env.client_1);
		objetTest.createClient(env.client_2);
		objetTest.createClient(env.client_3);
		objetTest.createClient(env.client_4);
		objetTest.createClient(env.client_5);
		objetTest.createClient(env.client_6);
		objetTest.createClient(env.client_7);
		objetTest.createClient(env.client_8);
		Affectation nouvelleAffect = Const_Models.constAffectation("Paddle", "Kid", "univers_bd@hastore.com", "65 page BDjeune", "", new Date(), SituationMatrimoniale.veuf, env.admin_0);
		objetTest.createClient(Const_Models.constClient(nouvelleAffect, env.agent_2));
		try {
			List<Client> listClients = objetTest.searchAllClientsByMatricule(env.agent_3.getMatricule(),"clien");
			for(int i=0; i!=listClients.size(); i++) {
				System.out.println("              Client"+i+": "+listClients.get(i).getLogin()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les clients");
		}
		
		return trueExecute?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
