package com.gestibank.main.dao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.CompteCourantDAO;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_CompteCourantDAO_1")
public class TEST_CompteCourantDAO_1 {
	@Autowired
	CompteCourantDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createCompteCourant")
	public String createCompteCourant()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readCompteCourantByRib")
	public String readCompteCourantByRib()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateCompteCourant")
	public String updateCompteCourant()
	{
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/deleteCompteCourantByRib")
	public String deleteCompteCourantByRib()
	{
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/readAllCompteCourants")
	public String readAllCompteCourants()
	{
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/readAllCompteCourantsByLogin")
	public String readAllCompteCourantsByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
