package com.gestibank.main.dao;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.AdministrateurDAO;
import com.gestibank.main._environnement.Const_Models;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.models.Administrateur;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_AdministrateurDAO_1")
public class TEST_AdministrateurDAO_1 {
	@Autowired
	AdministrateurDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createAdministrateur")
	public String createAdministrateur()
	{
		trueExecute = objetTest.createAdministrateur(env.admin_0);
		System.out.println("Le retour de trueCreate est: "+trueExecute);
		
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAdministrateurByLogin")
	public String readAdministrateurByLogin()
	{
		Administrateur appelant = env.admin_0;
		objetTest.createAdministrateur(appelant);  //le create est une methode que l'on imagine deja ok
		String login = appelant.getLogin();
		Administrateur rendu = objetTest.readAdministrateurByLogin(login);
		System.out.println("/n-------------- Ton Profil --------------/n"+appelant.toString());
		System.out.println();
		System.out.println("/n-------------- Son Profil --------------/n"+rendu.toString());
		
		return (appelant.toString().equals(rendu.toString()))?"ok":"pas ok";
	}
	
	@GetMapping("/updateAdministrateur")
	public String updateAdministrateur()
	{
		//en supposant que l'env.admin_0 soit dans la table (login: admin-0)
		Administrateur admin = objetTest.readAdministrateurByLogin("admin-0");  //le read est une methode que l'on imagine deja ok
		System.out.println("/n-------------- Avant --------------/n"+admin.toString());
		System.out.println();
		admin.setMail("new_mail@wahoo.com");
		trueExecute = objetTest.updateAdministrateur(admin);
		admin = objetTest.readAdministrateurByLogin(admin.getLogin());
		System.out.println("/n-------------- Apres --------------/n"+admin.toString());
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/deleteAdministrateurByLogin")
	public String deleteAdministrateurByLogin()
	{
		//en supposant que l'env.admin_0 soit dans la table (login: admin-0)
		Administrateur admin = objetTest.readAdministrateurByLogin("admin-0");  //le read est une methode que l'on imagine deja ok
		System.out.println();
		trueExecute = objetTest.deleteAdministrateurByLogin(admin.getLogin());
		try {
			objetTest.readAdministrateurByLogin(admin.getLogin());
		} catch(Exception e) {
			System.out.println("Impossible de retrouver cet admin");
		}
		
		return trueExecute?"ok":"pas ok";
	}
	
	@GetMapping("/readAllAdministrateur")
	public String readAllAdministrateur()
	{
		trueExecute = false;
		objetTest.createAdministrateur(env.admin_0);
		objetTest.createAdministrateur(Const_Models.constAdmin("J.S.", "Smith", "John", "john.smith@atlook.com", "06.73.86.14.95"));
		try {
			List<Administrateur> listAdmin = objetTest.readAllAdministrateur();
			for(int i=0; i!=listAdmin.size(); i++) {
				System.out.println("              Admin_"+i+": "+listAdmin.get(i).getLogin()+"\n");
			}
			trueExecute = true;
		} catch (Exception e) {
			System.out.println("Impossible de lire la liste de tout les administrateurs");
		}
		
		return trueExecute?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
