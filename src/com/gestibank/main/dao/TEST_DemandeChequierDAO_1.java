package com.gestibank.main.dao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.DemandeChequierDAO;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_DemandeChequierDAO_1")
public class TEST_DemandeChequierDAO_1 {
	@Autowired
	DemandeChequierDAO objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	private Boolean trueExecute = false;
	
	@GetMapping("/createDemandeChequier")
	public String createDemandeChequier()
	{	
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readDemandeChequierById")
	public String readDemandeChequierById()
	{	
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/updateDemandeChequier")
	public String updateDemandeChequier()
	{
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/deleteDemandeChequierById")
	public String deleteDemandeChequierById()
	{
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/readAllDemandeChequier")
	public String readAllDemandeChequier()
	{
		return (true)?"ok":"pas ok";
	}
	
	@GetMapping("/readAllDemandeChequierByStatus")
	public String readAllDemandeChequierByStatus()
	{
		return (true)?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
