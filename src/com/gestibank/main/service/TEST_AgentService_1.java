package com.gestibank.main.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.services.interfaces.AgentService;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_CompteRemunereDAO_1")
public class TEST_AgentService_1 {
	@Autowired
	AgentService objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	
	@GetMapping("/verifCredentials")
	public String verifCredentials()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readListClientsByMatricule")
	public String readListClientsByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateClientByMatricule")
	public String updateClientByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/deleteClientByMatricule")
	public String deleteClientByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/printListClients")
	public String printListClients()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/createCompteBancaireByMatricule")
	public String createCompteBancaireByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/deleteCompteBancaireByMatricule")
	public String deleteCompteBancaireByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readListComptesBancaireByMatricule")
	public String readListComptesBancaireByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/createTransactionByMatricule")
	public String createTransactionByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/createVirementByMatricule")
	public String createVirementByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readListPendingAffectationsByMatricule")
	public String readListPendingAffectationsByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateAffectationByMatricule")
	public String updateAffectationByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateDemandeChequierByMatricule")
	public String updateDemandeChequierByMatricule()
	{
		return (true)?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
