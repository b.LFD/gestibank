package com.gestibank.main.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.services.interfaces.ClientService;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_ClientService_1")
public class TEST_ClientService_1 {
	@Autowired
	ClientService objetTest;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	
	@GetMapping("/verifCredentials")
	public String verifCredentials()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/updateCompteClientByLogin")
	public String updateCompteClientByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readListComptesBancairesByLogin")
	public String readListComptesBancairesByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/createTransactionByLogin")
	public String createTransactionByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/createVirementByLogin")
	public String createVirementByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/readListTransactionByLogin")
	public String readListTransactionByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/printListTransaction")
	public String printListTransaction()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/createDemandeChequierByLogin")
	public String createDemandeChequierByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/getSoldeCompteBancaireByLogin")
	public String getSoldeCompteBancaireByLogin()
	{
		return (true)?"ok":"pas ok";
	}

	@GetMapping("/getClient")
	public String getClient()
	{
		return (true)?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
