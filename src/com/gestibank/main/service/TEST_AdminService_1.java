package com.gestibank.main.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gestibank.dao.interfaces.AdministrateurDAO;
import com.gestibank.dao.interfaces.AffectationDAO;
import com.gestibank.dao.interfaces.AgentDAO;
import com.gestibank.dao.interfaces.ClientDAO;
import com.gestibank.enumerations.SituationMatrimoniale;
import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.TypeTriAffectionAdmin;
import com.gestibank.main._environnement.Const_Models;
import com.gestibank.main._environnement.ENVIRONNEMENT_1;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.services.interfaces.AdminService;

//just push it FFS!!!!
//====><====
@Controller
@RequestMapping("/TEST_AdminService_1")
public class TEST_AdminService_1 {
	@Autowired
	AdminService objetTest;
	AdministrateurDAO adminDao;
	AgentDAO agentDao;
	ClientDAO clientDao;
	AffectationDAO affectationDao;

	private final ENVIRONNEMENT_1 env = new ENVIRONNEMENT_1();
	
	@GetMapping("/verifCredentials")
	public String verifCredentials()
	{
		adminDao.createAdministrateur(env.admin_0);   //on estime que createAdmin dans la dao est ok; se n'ai pas se qui est test� ici
		String login_true = "admin-0";
		String password_true = "root";
		String login_false = "admin_0";
		String password_false = "route";
		
		Boolean true_true = objetTest.verifCredentials(login_true, password_true);
		Boolean false_true = objetTest.verifCredentials(login_false, password_true);
		Boolean true_false = objetTest.verifCredentials(login_true, password_false);
		Boolean false_false = objetTest.verifCredentials(login_false, password_false);
		
		Boolean result = true_true && !false_true && !true_false && !false_false; 
		System.out.println("Le test retourne: "+result);
		return (result)?"ok":"pas ok";
	}

	@GetMapping("/createAgentByLogin")
	public String createAgentByLogin()
	{
		System.out.println("/n----------------- MON AGENT ------------------/n"+env.agent_1.toString());
		System.out.println();
		Boolean result = objetTest.createAgentByLogin(env.admin_0.getLogin(), env.agent_1);
		System.out.println("/n----------------- SON AGENT ------------------/n"+agentDao.readAgentByMatricule(env.agent_1.getMatricule()).toString()); //on admet que le readAgent de la dao est ok, c'est pas une fonction que l'on teste ici
		
		return (result)?"ok":"pas ok";
	}

	@GetMapping("/readListAgentByLogin")
	public String readListAgentByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		agentDao.createAgent(Const_Models.constAgent("Stagiaire", "Nouveau", "..", "..@.. .com", "__.__.__.__.__", admin));
		List<Agent> listAgent = objetTest.readListAgentByLogin(env.admin_0.getLogin(), "S");
		System.out.println("Agent dependant de "+admin.getLogin()+":");
		Boolean result = true;
		for(Agent agent : listAgent) {
			result &= agent.getAdminAffecte().getLogin().equals(admin.getLogin());
			System.out.println(agent.getMatricule()+" - "+agent.getNom()+", "+agent.getPrenom()+"   -> "+agent.getAdminAffecte().getLogin());
		}
		
		return (result)?"ok":"pas ok";
	}

	@GetMapping("/updateAgentByLogin")
	public String updateAgentByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		agentDao.createAgent(Const_Models.constAgent("Stagiaire", "Nouveau", "..", "..@.. .com", "__.__.__.__.__", admin));
		
		Agent agent = agentDao.readAgentByMatricule("Stagiaire");
		agent.setMail("nouveau@snapshoot.com");
		
		Boolean result = objetTest.updateAgentByLogin(admin.getLogin(), agent);
		
		return (result)?"ok":"pas ok";
	}

	@GetMapping("/deleteAgentByLogin")
	public String deleteAgentByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		agentDao.createAgent(Const_Models.constAgent("Stagiaire", "Nouveau", "..", "..@.. .com", "__.__.__.__.__", admin));
		
		System.out.println("/n---------- List des Agents avant -------------/n");
		List<Agent> listAgent = agentDao.readAllAgents();
		String result_1 = "";
		for(Agent agent : listAgent) {
				result_1 += agent.getMatricule()+"  -  "+agent.getNom()+", "+agent.getPrenom()+"\n";
		}
		System.out.println(result_1);
		
		objetTest.deleteAgentByLogin(admin.getLogin(), env.agent_1);
		
		System.out.println("/n---------- List des Agents apres -------------/n");
		listAgent = agentDao.readAllAgents();
		String result_2 = "";
		for(Agent agent : listAgent) {
				result_2 += agent.getMatricule()+"  -  "+agent.getNom()+", "+agent.getPrenom()+"\n";
		}
		System.out.println(result_2);
		
		return (!result_1.equals(result_2))?"ok":"pas ok";
	}

	@GetMapping("/readListClientByLogin")
	public String readListClientByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		agentDao.createAgent(Const_Models.constAgent("Stagiaire", "Nouveau", "..", "..@.. .com", "__.__.__.__.__", admin));
		clientDao.createClient(env.client_1);
		clientDao.createClient(env.client_2);
		clientDao.createClient(env.client_3);
		clientDao.createClient(env.client_4);
		clientDao.createClient(env.client_5);
		clientDao.createClient(env.client_6);
		clientDao.createClient(env.client_7);
		clientDao.createClient(env.client_8);
		
		Boolean result = true;
		List<Client> listClients = objetTest.readListClientByLogin(admin.getLogin(), env.agent_2);
		for(Client client : listClients) {
			result &= admin.getLogin().equals(client.getAgentAffecte().getAdminAffecte().getLogin());
			System.out.println(client.getLogin()+"  -  "+client.getNom()+", "+client.getPrenom());
		}
		
		return result?"ok":"pas ok";
	}

	@GetMapping("/readListClientsByLogin_2")
	public String readListClientsByLogin_2()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		agentDao.createAgent(Const_Models.constAgent("Stagiaire", "Nouveau", "..", "..@.. .com", "__.__.__.__.__", admin));
		clientDao.createClient(env.client_1);
		clientDao.createClient(env.client_2);
		clientDao.createClient(env.client_3);
		clientDao.createClient(env.client_4);
		clientDao.createClient(env.client_5);
		clientDao.createClient(env.client_6);
		clientDao.createClient(env.client_7);
		clientDao.createClient(env.client_8);
		Affectation affectation = Const_Models.constAffectation("Nouveau", "client", "..@..", "adresse", "__.__.__.__.__", new Date(), SituationMatrimoniale.celibataire, admin);
		clientDao.createClient(Const_Models.constClient(affectation, env.agent_3));
		
		Boolean result = true;
		String recherche = "L";
		List<Client> listClients = objetTest.readListClientsByLogin(admin.getLogin(), env.agent_3, recherche);
		for(Client client : listClients) {
			result &= (recherche.equals(client.getLogin().substring(0, recherche.length())) || recherche.equals(client.getNom().substring(0, recherche.length())));
			System.out.println(client.getLogin()+"  -  "+client.getNom()+", "+client.getPrenom());
		}
		
		return result?"ok":"pas ok";
	}

	@GetMapping("/updateClientByLogin")
	public String updateClientByLogin()
	{
		Client client = env.client_7;
		Administrateur admin = env.admin_0;
		
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(client.getAgentAffecte());
		agentDao.createAgent(env.agent_2);
		clientDao.createClient(client);
		
		String result_1 = clientDao.readClientByLogin(client.getLogin()).toStringTest();
		System.out.println("\n-------------- CLIENT AVANT ---------------\n"+result_1);
		System.out.println();
		client.setAdresse("nouvelle@dress.com");
		objetTest.updateClientByLogin(admin.getLogin(), client);
		String result_2 = clientDao.readClientByLogin(client.getLogin()).toStringTest();
		System.out.println("\n-------------- CLIENT APRES ---------------\n"+result_2);
		
		return (!result_1.equals(result_2))?"ok":"pas ok";
	}

	@GetMapping("/printListAgents")
	public String printListAgents()
	{
		// TODO
		
		return "ok";
	}

	@GetMapping("/printListClients")
	public String printListClients()
	{
		// TODO
		
		return "ok";
	}

	@GetMapping("/readPendingAffectationsByLogin")
	public String readPendingAffectationsByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
			env.affectation_1.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_1);
			env.affectation_2.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_2);
			env.affectation_3.setStatut(StatutAffectation.refuse);
		affectationDao.createAffectation(env.affectation_3);
			env.affectation_4.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_4);
			env.affectation_5.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_5);
			env.affectation_6.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_6);
			env.affectation_7.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_7);
			env.affectation_8.setStatut(StatutAffectation.refuse);
		affectationDao.createAffectation(env.affectation_8);
			env.affectation_9.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_9);
			env.affectation_10.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_10);
			env.affectation_11.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_11);
		
		Boolean result = true;
		List<Affectation> listAffect = objetTest.readPendingAffectationsByLogin(admin.getLogin());
		for(Affectation affect : listAffect) {
			result &= (affect.getStatut() == StatutAffectation.en_attente_admin);
			System.out.println("Affectation "+affect.getId()+": "+affect.getNom()+", "+affect.getPrenom()+"   ->"+affect.getAdminAffecte().getLogin()+" -- "+affect.getStatut());
		}
		
		return (result)?"ok":"pas ok";
	}

	@GetMapping("/updateAffectationByLogin")
	public String updateAffectationByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		
		affectationDao.createAffectation(env.affectation_1);
		affectationDao.createAffectation(env.affectation_2);
		affectationDao.createAffectation(env.affectation_3);
		affectationDao.createAffectation(env.affectation_4);
		affectationDao.createAffectation(env.affectation_5);
		affectationDao.createAffectation(env.affectation_6);
		affectationDao.createAffectation(env.affectation_7);
		affectationDao.createAffectation(env.affectation_8);
		affectationDao.createAffectation(env.affectation_9);
		affectationDao.createAffectation(env.affectation_10);
		affectationDao.createAffectation(env.affectation_11);
		
		List<Affectation> listAffect = affectationDao.readAllAffectations();
		Affectation affect = null;
		int i = 0;
		while(i != listAffect.size() && affect == null) {
			if(admin.getLogin().equals(listAffect.get(i).getAdminAffecte().getLogin())) {
				affect = listAffect.get(i);
			}
			i++;
		}
		
		Boolean result = false;
		
		if(affect != null) {
			System.out.println("/n----------- AFFECTATION AVANT -------------/n");
			System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"   ->   "+affect.getAgentAffecte().getMatricule());
			
			affect.setAgentAffecte(env.agent_2);
			result = objetTest.updateAffectationByLogin(admin.getLogin(), affect);
	
			affect = affectationDao.readAffectationById(affect.getId());
			System.out.println("/n----------- AFFECTATION APRES -------------/n");
			System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"   ->   "+affect.getAgentAffecte().getMatricule());
		} else {
			System.out.println("Il y a aucune affectation affilie a l'admin en cours");
		}
			
		return (result)?"ok":"pas ok";
	}

	@GetMapping("/readListAffectationsByLogin")
	public String readListAffectationsByLogin()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		Affectation affectation = Const_Models.constAffectation("Nouveau", "client", "..@..", "adresse", "__.__.__.__.__", new Date(), SituationMatrimoniale.celibataire, admin);
		clientDao.createClient(Const_Models.constClient(affectation, env.agent_3));
		affectationDao.createAffectation(env.affectation_1);
		affectationDao.createAffectation(env.affectation_2);
		affectationDao.createAffectation(env.affectation_3);
		affectationDao.createAffectation(env.affectation_4);
		affectationDao.createAffectation(env.affectation_5);
		affectationDao.createAffectation(env.affectation_6);
		affectationDao.createAffectation(env.affectation_7);
		affectationDao.createAffectation(env.affectation_8);
		affectationDao.createAffectation(env.affectation_9);
		affectationDao.createAffectation(env.affectation_10);
		affectationDao.createAffectation(env.affectation_11);
		
		Boolean result = true;
		List<Affectation> listAffect = objetTest.readListAffectationsByLogin(admin.getLogin());
		for(Affectation affect : listAffect) {
			result &= admin.getLogin().equals(affect.getAdminAffecte().getLogin());
			System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom());
		}
		
		return result?"ok":"pas ok";
	}

	@GetMapping("/readListAffectationsByLogin_2")
	public String readListAffectationsByLogin_2()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
			env.affectation_1.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_1);
			env.affectation_2.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_2);
			env.affectation_3.setStatut(StatutAffectation.refuse);
		affectationDao.createAffectation(env.affectation_3);
			env.affectation_4.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_4);
			env.affectation_5.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_5);
			env.affectation_6.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_6);
			env.affectation_7.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_7);
			env.affectation_8.setStatut(StatutAffectation.refuse);
		affectationDao.createAffectation(env.affectation_8);
			env.affectation_9.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_9);
			env.affectation_10.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_10);
			env.affectation_11.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_11);
	
		Boolean result = true;
		StatutAffectation statut = StatutAffectation.en_attente_agent;
		List<Affectation> listAffect = objetTest.readListAffectationsByLogin(admin.getLogin(),statut);
		for(Affectation affect : listAffect) {
			result &= (admin.getLogin().equals(affect.getAdminAffecte().getLogin()) && (affect.getStatut() == statut));
			System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"  --  "+affect.getStatut());
		}
		
		return result?"ok":"pas ok";
	}

	@GetMapping("/readListAffectationsByLogin_3")
	public String readListAffectationsByLogin_3()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
		Affectation affectation = Const_Models.constAffectation("Nouveau", "client", "..@..", "adresse", "__.__.__.__.__", new Date(), SituationMatrimoniale.celibataire, admin);
		clientDao.createClient(Const_Models.constClient(affectation, env.agent_3));
		affectationDao.createAffectation(env.affectation_1);
		affectationDao.createAffectation(env.affectation_2);
		affectationDao.createAffectation(env.affectation_3);
		affectationDao.createAffectation(env.affectation_4);
		affectationDao.createAffectation(env.affectation_5);
		affectationDao.createAffectation(env.affectation_6);
		affectationDao.createAffectation(env.affectation_7);
		affectationDao.createAffectation(env.affectation_8);
		affectationDao.createAffectation(env.affectation_9);
		affectationDao.createAffectation(env.affectation_10);
		affectationDao.createAffectation(env.affectation_11);
		
		Boolean result = true;
		TypeTriAffectionAdmin tri = TypeTriAffectionAdmin.date_demande;
		List<Affectation> listAffect = objetTest.readListAffectationsByLogin(admin.getLogin(),tri);
		switch(tri) {
			case date_affectation:
				System.out.println("Tri par date d'affectation");
				for(Affectation affect : listAffect) {
					result &= admin.getLogin().equals(affect.getAdminAffecte().getLogin());
					System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"   @   "+affect.getDateAffectation().toString());
				}
				break;
				
			case date_demande:
				System.out.println("Tri par date de demande");
				for(Affectation affect : listAffect) {
					result &= admin.getLogin().equals(affect.getAdminAffecte().getLogin());
					System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"   @   "+affect.getDateCreation().toString());
				}
				break;
				
			default:
				System.out.println("Cas d'un tri inconnu");
		}
		
		return result?"ok":"pas ok";
	}

	@GetMapping("/readListAffectationsByLogin_4")
	public String readListAffectationsByLogin_4()
	{
		Administrateur admin = env.admin_0;
		adminDao.createAdministrateur(admin);
		agentDao.createAgent(env.agent_1);
		agentDao.createAgent(env.agent_2);
		agentDao.createAgent(env.agent_3);
		agentDao.createAgent(env.agent_4);
		agentDao.createAgent(env.agent_5);
		agentDao.createAgent(env.agent_6);
			env.affectation_1.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_1);
			env.affectation_2.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_2);
			env.affectation_3.setStatut(StatutAffectation.refuse);
		affectationDao.createAffectation(env.affectation_3);
			env.affectation_4.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_4);
			env.affectation_5.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_5);
			env.affectation_6.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_6);
			env.affectation_7.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_7);
			env.affectation_8.setStatut(StatutAffectation.refuse);
		affectationDao.createAffectation(env.affectation_8);
			env.affectation_9.setStatut(StatutAffectation.en_attente_admin);
		affectationDao.createAffectation(env.affectation_9);
			env.affectation_10.setStatut(StatutAffectation.en_attente_agent);
		affectationDao.createAffectation(env.affectation_10);
			env.affectation_11.setStatut(StatutAffectation.accepte);
		affectationDao.createAffectation(env.affectation_11);
	
		Boolean result = true;
		TypeTriAffectionAdmin tri = TypeTriAffectionAdmin.date_demande;
		StatutAffectation statut = StatutAffectation.en_attente_agent;
		List<Affectation> listAffect = objetTest.readListAffectationsByLogin(admin.getLogin(),statut,tri);
		switch(tri) {
			case date_affectation:
				System.out.println("Tri par date d'affectation");
				for(Affectation affect : listAffect) {
					result &= (admin.getLogin().equals(affect.getAdminAffecte().getLogin()) && (affect.getStatut() == statut));
					System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"  --  "+affect.getStatut()+"   @   "+affect.getDateAffectation().toString());
				}
				break;
				
			case date_demande:
				System.out.println("Tri par date de demande");
				for(Affectation affect : listAffect) {
					result &= (admin.getLogin().equals(affect.getAdminAffecte().getLogin()) && (affect.getStatut() == statut));
					System.out.println("Affectation "+affect.getId()+":  -  "+affect.getNom()+", "+affect.getPrenom()+"  --  "+affect.getStatut()+"   @   "+affect.getDateCreation().toString());
				}
				break;
				
			default:
				System.out.println("Cas d'un tri inconnu");
		}
		
		return result?"ok":"pas ok";
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
