/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gestibank.entites;

import java.util.HashMap;
import java.util.Map;

import com.gestibank.metiers.Convertisseur;

public class Convertbean {

    private String monnaiedep ;
    private String montant;
    private String monnaiearr;
    private double newMont;

   
    private Map<String, String> errors = new HashMap<String, String>();

    public Convertbean() {
        errors.put("monnaiedep", "");
        errors.put("montant", "");
        errors.put("monnaiearr", "");
    }

    public Convertbean(String monnaiedep, String montant, String monnaiearr) {
        this.monnaiedep = monnaiedep;
        this.montant = montant;
        this.monnaiearr = monnaiearr;
    }

    public String getMonnaiedep() {
        return monnaiedep;
    }

    public String getMontant() {
        return montant;
    }

    public String getMonnaiearr() {
        return monnaiearr;
    }

    public void setMonnaiedep(String monnaiedep) {
        this.monnaiedep = monnaiedep;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public void setMonnaiearr(String monnaiearr) {
        this.monnaiearr = monnaiearr;
    }

     public double getNewMont() {
        return newMont;
    }

    public void setNewMont(double newMont) {
        this.newMont = newMont;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public boolean controler() {
        boolean ok = true;

        if ((monnaiedep.equals("")) && (monnaiedep.isEmpty())) {
            ok = false;
            errors.put("monnaiedep", "Monnaie de d�part Obligatoire");

        }
        if (montant.equals("")) {
            ok = false;
            errors.put("montant", "Montant  Obligatoire");

        }
        if (monnaiearr.equals("")) {
            ok = false;
            errors.put("monnaiearr", "Monnaie d'arriv�e Obligatoire");
        }
        
        if (ok)
        {
             newMont = Convertisseur.convertir(monnaiedep, monnaiearr, Double.parseDouble(montant));
        }
        return ok;
    }

}
