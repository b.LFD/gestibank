package com.gestibank.dao.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gestibank.models.Administrateur;

@Repository("administrateurDAO")
public interface AdministrateurDAO {
	
	/** Envoi l'objet administrateur pass� en param�tre dans la BDD
	 *  Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param administrateur
	 * @return boolean
	 */
	Boolean createAdministrateur(Administrateur administrateur);
	
	
	/** Fonction qui renvoit l'administrateur associ� au login pass� en param�tre
	 * 
	 * @param login
	 * @return administrateur
	 */
	Administrateur readAdministrateurByLogin(String login);
	
	/** Met � jour l'administrateur pass� en param�tre dans la BDD
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param administrateur
	 * @return boolean
	 */
	Boolean updateAdministrateur(Administrateur administrateur);
	
	/** Supprime l'administrateur pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param login
	 * @return boolean
	 */
	Boolean deleteAdministrateurByLogin(String login);
	
	/**
	 * Renvoie la liste de tous les administrateurs
	 * @return List<Administrateur>
	 */
    List<Administrateur> readAllAdministrateur();
}
