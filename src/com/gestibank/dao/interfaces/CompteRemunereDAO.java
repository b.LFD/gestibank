package com.gestibank.dao.interfaces;


import java.util.List;

import com.gestibank.models.CompteRemunere;

public interface CompteRemunereDAO {
	
	/**
	 * Cr�er dans la bdd le compte remunere pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param compteRemunere
	 * @return boolean
	 */
	Boolean createCompteRemunere(CompteRemunere compteRemunere);
	
	/**
	 * Renvoie le compte remunere associ� au rib pass� en param�tre
	 * @param rib
	 * @return CompteRemunere
	 */
	CompteRemunere readCompteRemunereByRib(String rib);
	
	/**
	 * Met � jour le compte remunere pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param compteRemunere
	 * @return boolean
	 */
	Boolean updateCompteRemunere(CompteRemunere compteRemunere);
	
	/**
	 * Supprime le compte remunere associ� au rib pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param rib
	 * @return boolean
	 */
	Boolean deleteCompteRemunereByRib(String rib);
	
	/**
	 * Renvoie la liste de tous les comptes remuneres stock�es en bdd
	 * @return List<CompteRemunere>
	 */
	List<CompteRemunere> readAllCompteRemuneres();
	
	/**
	 * Renvoie la liste de tous les comptes remuneres associ�s au client correspondant au login pass� en param�tre
	 * @param login
	 * @return List<CompteRemunere>
	 */
	List<CompteRemunere> readAllCompteRemuneresByLogin(String login);
}