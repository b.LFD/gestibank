package com.gestibank.dao.interfaces;

import java.util.List;

import com.gestibank.models.Transaction;


public interface TransactionDAO {
	
	/**
	 * Cr�er la transaction pass� en param�tre dans la bdd
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param transaction
	 * @return boolean
	 */
	Boolean createTransaction(Transaction transaction);
	
	/**
	 * Renvoie la transaction correspondant � l'id pass� en param�tre
	 * @param id
	 * @return Transaction
	 */
	Transaction readTransactionById(int id);
	
	/**
	 * Met � jour la transaction pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param transaction
	 * @return boolean
	 */
	Boolean updateTransaction(Transaction transaction);
	
	/**
	 * Supprime la transaction dans la bdd correspondant � l'id pass� en param�tre
	 * @param id
	 * @return
	 */
	Boolean deleteTransaction(int id);
	
	/**
	 * Renvoie la liste de toutes les transactions stock�es en bdd
	 * @param rib
	 * @return List<Transaction>
	 */
	List<Transaction> readAllTransaction();
	
	/**
	 * Renvoie la liste de toutes les transactions associ�es au compte courant correspondant au rib pass� en param�tre
	 * @param rib
	 * @return List<Transaction>
	 */
	List<Transaction> readAllTransactionByRibCompteCourant(String rib);
	
	/**
	 * Renvoie liste de toutes les transactions associ�es au compte remunere correspondant au rib pass� en param�tre
	 * @param rib
	 * @return List<Transaction>
	 */
	List<Transaction> readAllTransactionByRibCompteRemunere(String rib);
}