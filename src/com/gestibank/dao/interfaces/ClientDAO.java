package com.gestibank.dao.interfaces;

import java.util.List;

import com.gestibank.models.Client;


public interface ClientDAO {
	
	/**
	 * Cr�er le client dans la bdd correspondant � celui pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param client
	 * @return
	 */
	Boolean createClient(Client client);

	/**
	 * Renvoie le client associ� au login pass� en param�tre
	 * @param login
	 * @return client
	 */
	Client readClientByLogin(String login);
	
	/**
	 * Met � jour le client pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param client
	 * @return boolean
	 */
	Boolean updateClient(Client client);
	
	/**
	 * Supprime le client pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param login
	 * @return
	 */
	Boolean deleteClientByLogin(String login);
	
	/**
	 * Renvoie l'int�gralit�e des Clients pr�sent dans la base de donn�e
	 * @return
	 */
	List<Client> readAllClients();
	
	/**
	 * Renvoie la liste de tous les clients stock�s en bdd
	 * @param matricule
	 * @return List<Client>
	 */
	List<Client> readAllClientsByMatricule(String matricule);
	
	/**
	 * Renvoie tous les clients li�s �/aux agents dont le login ou le matricule contient le premier param�tre
	 * @param matricule
	 * @param loginClient_nom
	 * @return List<Client>
	 */
	List<Client> searchAllClientsByMatricule(String matricule, String loginClient_nom);
}
