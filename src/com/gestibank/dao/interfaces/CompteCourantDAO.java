package com.gestibank.dao.interfaces;

import java.util.List;

import com.gestibank.models.CompteCourant;

public interface CompteCourantDAO {
	
	/**
	 * Cr�er le compte courant pass� en param�tre dans la bb
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param compteCourant
	 * @return boolean
	 */
	Boolean createCompteCourant(CompteCourant compteCourant);
	
	/**
	 * Renvoie le compte courant portant le rib pass� en param�tre
	 * @param rib
	 * @return compteCourant
	 */
	CompteCourant readCompteCourantByRib(String rib);
	
	/**
	 * Met � jour le compte courant pass� en param�tre
	 * @param compteCourant
	 * @return boolean
	 */
	Boolean updateCompteCourant(CompteCourant compteCourant);
	
	/**
	 * Supprime le compte courant aossi� au rib pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param rib
	 * @return boolean
	 */
	Boolean deleteCompteCourantByRib(String rib);
	
	/**
	 * Renvoie la liste de tous les comptes courants stock�es en bdd
	 * @return List<CompteCourant>
	 */
	List<CompteCourant> readAllCompteCourants();
	
	/**
	 * Renvoie la liste de tous les comptes courants associ�s au client correspondant au login pass� en param�tre
	 * @param login
	 * @return List<CompteCourant>
	 */
	List<CompteCourant> readAllCompteCourantsByLogin(String login);
}
