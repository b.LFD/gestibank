package com.gestibank.dao.interfaces;

import java.util.List;

import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.TypeTriAffectionAdmin;
import com.gestibank.models.Affectation;

public interface AffectationDAO {
	
	/**
	 * Cr�er dans la BDD l'entit� affectation pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param affectation
	 * @return boolean
	 */
	Boolean createAffectation(Affectation affectation);
	
	/**
	 * Renvoie l'affectation stock�e dans la BDD dont l'id correspond au nombre pass� en param�tre
	 * @param id
	 * @return affectation
	 */
	Affectation readAffectationById(int id);
	
	/**
	 * Met � jour l'affectation correspondante � celle pass�e en param�tre 
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param affectation
	 * @return boolean
	 */
	Boolean updateAffectation(Affectation affectation);
	
	/**
	 * Supprime dans la bdd l'affectation correspondante � celle pass�e en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param id
	 * @return boolean
	 */
	Boolean deleteAffectationById(int id);
	
	/**
	 * Renvoie l'entieret� de la table affectation sous forme de liste
	 * @return List<Affectation>
	 */
	List<Affectation> readAllAffectations();
	
	/**
	 * Renvoie toutes les affectations selon le type d'affectation pass� en param�tre
	 * @param statutAffection
	 * @return List<Affectation>
	 */
	List<Affectation> readAllAffectationsByStatut(StatutAffectation statutAffection);
	
	/**
	 * Renvoie toutes les affectations tri�es selon le type de tri choisi pass� en param�tre plac�es dans une liste d'affectation 
	 * @param order
	 * @return List<Affectation>
	 */
	List<Affectation> readAllAffectationsOrderByTypeTriAffectationAdmin(TypeTriAffectionAdmin order);
	
	/**
	 *  Renvoie toutes les affectations en fonction du statut de l'affectation elles-m�mes tri�es selon le tri pass� en param�tre
	 * @param statutAffection
	 * @param order
	 * @return List<Affectation>
	 */
	List<Affectation> readAllAffectationsByStatutOrderByTypeTriAffectationAdmin(StatutAffectation statutAffection, TypeTriAffectionAdmin order);
}
