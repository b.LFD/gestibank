package com.gestibank.dao.interfaces;

import java.util.List;

import com.gestibank.models.Agent;

public interface AgentDAO {
	/**
	 * Cr�er l'agent dans la bdd
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param agent
	 * @return boolean
	 */
	Boolean createAgent(Agent agent);
	
	/**
	 * Renvoie l'agent associ� au matricule pass� en param�tre
	 * @param matricule
	 * @return agent
	 */
	Agent readAgentByMatricule(String matricule);
	
	/**
	 * Met � jour l'agent pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param agent
	 * @return boolean
	 */
	Boolean updateAgent(Agent agent);
	
	/**
	 * Supprime l'agent associ� au matricule pass� en param�tre
	 * @param matricule
	 * @return boolean
	 */
	Boolean deleteAgentByMatricule(String matricule);
	
	/**
	 * Renvoie tous les agents stock�s en bdd
	 * @return List<Agent>
	 */
	List<Agent> readAllAgents();
	
	/**
	 * Renvoie tous les agents li�s � l'admin pass� en param�tre 
	 * @param loginAdmin
	 * @return List<Agent<>
	 */
	List<Agent> readAllAgentsByLoginAdmin(String loginAdmin);
	
	/**
	 * Renvoies tous les agents li�s �/aux admin dont le matricule ou le login contient le premier param�tre
	 * @param loginAdmin
	 * @param matricule_nom
	 * @return List<Agent>
	 */
	List<Agent> searchAllAgentsByLoginAdmin(String loginAdmin, String matricule_nom);
}