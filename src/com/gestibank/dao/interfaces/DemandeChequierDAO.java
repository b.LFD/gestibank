package com.gestibank.dao.interfaces;

import java.util.List;

import com.gestibank.enumerations.StatutDemandeChequier;
import com.gestibank.models.DemandeChequier;


public interface DemandeChequierDAO {
	
	/**
	 * Cr�er la demande chequier pass� en param�tre dans la bdd
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param demandeChequier
	 * @return boolean
	 */
	Boolean createDemandeChequier(DemandeChequier demandeChequier);
	
	/**
	 * Renvoie la demande chequier correspondant � l'id pass� en param�tre
	 * @param id
	 * @return DemandeChequier
	 */
	DemandeChequier readDemandeChequierById(Integer id);
	
	/**
	 * Met � jour la demande chequier pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param demandeChequier
	 * @return boolean
	 */
	Boolean updateDemandeChequier(DemandeChequier demandeChequier);
	
	/**
	 * Supprime la demande chequier pass� en param�tre
	 * Renvoie true si l'op�ration s'est effectu�e correctement
	 * @param id
	 * @return boolean
	 */
	Boolean deleteDemandeChequierById(Integer id);
	
	/**
	 * Renvoie la liste de toutes les demande chequier stock�s en bdd
	 * @return List<DemandeChequier>
	 */
	List<DemandeChequier> readAllDemandeChequier();
	
	/**
	 * Renvoie la liste de toutes les demande chequier selon le statut pass� en param�tre
	 * @param statutDemandeChequier
	 * @return List<DemandeChequier>
	 */
	List<DemandeChequier> readAllDemandeChequierByStatus(StatutDemandeChequier statutDemandeChequier);
}