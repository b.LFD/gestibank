package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.CompteCourantDAO;
import com.gestibank.models.CompteCourant;

@Repository("compteCourantDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class CompteCourantDAO_Impl extends AbstractDAO implements CompteCourantDAO {
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public CompteCourantDAO_Impl() {
			// TODO Auto-generated constructor stub
		}

		
	/* METHODES INTERFACE */
		@Transactional(propagation = Propagation.NESTED, readOnly=false)

		public Boolean createCompteCourant(CompteCourant compteCourant) {			
			return persist(compteCourant);
		}
		@Transactional(propagation = Propagation.NESTED, readOnly=false)
		
		public CompteCourant readCompteCourantByRib(String rib) {			
			return (CompteCourant) findById(CompteCourant.class, rib);
		}
		@Transactional(propagation = Propagation.NESTED, readOnly=false)
		
		public Boolean updateCompteCourant(CompteCourant compteCourant) {
			return update(compteCourant);
		}
		@Transactional(propagation = Propagation.NESTED, readOnly=false)
		
		public Boolean deleteCompteCourantByRib(String rib) {
			CompteCourant compteCourant = (CompteCourant) findById(CompteCourant.class, rib);
			return delete(compteCourant);
		}
		@Transactional(propagation = Propagation.NESTED, readOnly=false)
		
		public List<CompteCourant> readAllCompteCourants() {
			List<CompteCourant> listCpteCourant = new ArrayList<CompteCourant>();
			
			try {
				  CriteriaBuilder cb = getSession().getCriteriaBuilder();
				  CriteriaQuery<CompteCourant> cq = cb.createQuery(CompteCourant.class);
				  Root<CompteCourant> root = cq.from(CompteCourant.class);
				  cq.select(root);
				  Query<CompteCourant> query = getSession().createQuery(cq);
				  listCpteCourant = query.getResultList();
				
			} catch (Exception e) {
			  System.out.println(e.getMessage());
			}
			return listCpteCourant; 
		}
		@Transactional(propagation = Propagation.NESTED, readOnly=false)
		
		public List<CompteCourant> readAllCompteCourantsByLogin(String login) {		
			List<CompteCourant> listCpteCourant = new ArrayList<CompteCourant>();
			
			try {
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<CompteCourant> cq = cb.createQuery(CompteCourant.class);
                Root<CompteCourant> root = cq.from(CompteCourant.class);
                cq.select(root)
                .where(cb.equal(root.get("login"), login));
                Query<CompteCourant> query = getSession().createQuery(cq);
                listCpteCourant =  query.getResultList();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
			return listCpteCourant;
		}
}
