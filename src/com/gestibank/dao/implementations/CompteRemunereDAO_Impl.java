package com.gestibank.dao.implementations;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.CompteRemunereDAO;
import com.gestibank.models.CompteRemunere;

@Repository("compteRemunereDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class CompteRemunereDAO_Impl extends AbstractDAO implements CompteRemunereDAO {
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public CompteRemunereDAO_Impl() {
			// TODO Auto-generated constructor stub
		}

		
	/* METHODES INTERFACE */
		public Boolean createCompteRemunere(CompteRemunere compteRemunere) {
			return persist(compteRemunere);
		}
		
		public CompteRemunere readCompteRemunereByRib(String rib) {
			/**/return (CompteRemunere) findById(CompteRemunere.class, rib);
		}
		
		public Boolean updateCompteRemunere(CompteRemunere compteRemunere) {
			return update(compteRemunere);
		}
		
		public Boolean deleteCompteRemunereByRib(String rib) {
			/**/CompteRemunere cpteremu = (CompteRemunere) findById(CompteRemunere.class, rib);
			return delete(cpteremu);
		}
		
		public List<CompteRemunere> readAllCompteRemuneres() {			
			List<CompteRemunere> listcpteRemu = new ArrayList<CompteRemunere>();
			
			try {
				  CriteriaBuilder cb = getSession().getCriteriaBuilder();
				  CriteriaQuery<CompteRemunere> cq = cb.createQuery(CompteRemunere.class);
				  Root<CompteRemunere> root = cq.from(CompteRemunere.class);
				  cq.select(root);
				  Query<CompteRemunere> query = getSession().createQuery(cq);
				  listcpteRemu = query.getResultList();
				
			} catch (Exception e) {
			  System.out.println(e.getMessage());
			}
			return listcpteRemu; 			
		}
		
		public List<CompteRemunere> readAllCompteRemuneresByLogin(String login) {
			List<CompteRemunere> listcpteRemu = new ArrayList<CompteRemunere>();

			try {
				CriteriaBuilder cb = getSession().getCriteriaBuilder();
				CriteriaQuery<CompteRemunere> cq = cb.createQuery(CompteRemunere.class);
				Root<CompteRemunere> root = cq.from(CompteRemunere.class);
				cq.select(root).where(cb.equal(root.get("login"), login));
				Query<CompteRemunere> query = getSession().createQuery(cq);
				listcpteRemu =  query.getResultList();

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return listcpteRemu;
		}
}
