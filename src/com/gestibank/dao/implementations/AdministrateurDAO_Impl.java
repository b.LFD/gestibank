package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.AdministrateurDAO;
import com.gestibank.models.Administrateur;

@Repository("administrateurDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class AdministrateurDAO_Impl extends AbstractDAO implements AdministrateurDAO {
	

	/* CONSTRUCTEURS */
	
		//Constructeurs sans parametres
		public AdministrateurDAO_Impl() {
			// TODO Auto-generated constructor stub
		}

		
	/* METHODES INTERFACE */
		public Boolean createAdministrateur(Administrateur administrateur) {
			return persist(administrateur);	
		}
		
		public Administrateur readAdministrateurByLogin(String login) {
			System.out.println("Adminstrateur DAO login : " + login);
			/**/return (Administrateur) findById(Administrateur.class, login);
		}
		
		public Boolean updateAdministrateur(Administrateur administrateur) {
			return update(administrateur);
		}
		
		public Boolean deleteAdministrateurByLogin(String login) {
			/**/Administrateur admin = (Administrateur) findById(Administrateur.class, login);
			return delete(admin);
		}
		
		public List<Administrateur> readAllAdministrateur(){
			List<Administrateur> listAdmins = new ArrayList<Administrateur>();
			
			try {
				  CriteriaBuilder cb = getSession().getCriteriaBuilder();
				  CriteriaQuery<Administrateur> cq = cb.createQuery(Administrateur.class);
				  Root<Administrateur> root = cq.from(Administrateur.class);
				  cq.select(root);
				  Query<Administrateur> query = getSession().createQuery(cq);
				  listAdmins = query.getResultList();
				
			} catch (Exception e) {
			  System.out.println(e.getMessage());
			}
			return listAdmins;	
		}
}
