package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.TransactionDAO;
import com.gestibank.models.Transaction;

@Repository("transactionDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class TransactionDAO_Impl extends AbstractDAO implements TransactionDAO {

	/* CONSTRUCTEURS */
	// Constructeurs sans parametres
	public TransactionDAO_Impl() {
		// TODO Auto-generated constructor stub
	}

	/* METHODES INTERFACE */ 
	//@Transactional(propagation = Propagation.NESTED, readOnly=false)
	//@Transactional
	public Boolean createTransaction(Transaction transaction) {
		return persist(transaction);
	}

//	@Transactional

	public Transaction readTransactionById(int id) {
		/**/return (Transaction) findById(Transaction.class, id);
	}

//	@Transactional

	public Boolean updateTransaction(Transaction transaction) {
		return update(transaction);
	}

//	@Transactional

	public Boolean deleteTransaction(int id) {
		Transaction transaction = (Transaction) findById(Transaction.class, id);
		return delete(transaction);
	}

//	@Transactional
	public List<Transaction> readAllTransaction() {
		List<Transaction> listTransac = new ArrayList<Transaction>();

		try {
			CriteriaBuilder cb = getSession().getCriteriaBuilder();
			CriteriaQuery<Transaction> cq = cb.createQuery(Transaction.class);
			Root<Transaction> root = cq.from(Transaction.class);
			cq.select(root);
			Query<Transaction> query = getSession().createQuery(cq);
			listTransac = query.getResultList();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return listTransac;
	}

	public List<Transaction> readAllTransactionByRibCompteCourant(String rib) {
		List<Transaction> listTransac = new ArrayList<Transaction>();

		try {
			CriteriaBuilder cb = getSession().getCriteriaBuilder();
			CriteriaQuery<Transaction> cq = cb.createQuery(Transaction.class);
			Root<Transaction> root = cq.from(Transaction.class);
			cq.select(root).where(cb.equal(root.get("ribCompte"), rib));
			Query<Transaction> query = getSession().createQuery(cq);
			listTransac = query.getResultList();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return listTransac;
	}

	public List<Transaction> readAllTransactionByRibCompteRemunere(String rib) {
		List<Transaction> listTransac = new ArrayList<Transaction>();

		try {
			CriteriaBuilder cb = getSession().getCriteriaBuilder();
			CriteriaQuery<Transaction> cq = cb.createQuery(Transaction.class);
			Root<Transaction> root = cq.from(Transaction.class);
			cq.select(root).where(cb.equal(root.get("ribCompte"), rib));
			Query<Transaction> query = getSession().createQuery(cq);
			listTransac = query.getResultList();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return listTransac;
	}
}
