package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.DemandeChequierDAO;
import com.gestibank.enumerations.StatutDemandeChequier;
import com.gestibank.models.*;

@Repository("demandeChequierDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class DemandeChequierDAO_Impl extends AbstractDAO implements DemandeChequierDAO {


	/* CONSTRUCTEURS */
	//Constructeurs sans parametres
	public DemandeChequierDAO_Impl() {
		// TODO Auto-generated constructor stub
	}


	/* METHODES INTERFACE */
	public Boolean createDemandeChequier(DemandeChequier demandeChequier) {
		return persist(demandeChequier);
	}
	public DemandeChequier readDemandeChequierById(Integer id) {
		/**/return (DemandeChequier) findById(DemandeChequier.class, id);
	}
	public Boolean updateDemandeChequier(DemandeChequier demandeChequier) {
		return update(demandeChequier);
	}
	public Boolean deleteDemandeChequierById(Integer id) {
		/**/DemandeChequier dmdchq = (DemandeChequier) findById(DemandeChequier.class, id);
		return delete(dmdchq);
	}

	public List<DemandeChequier> readAllDemandeChequier() {
		List<DemandeChequier> listeChequiers = new ArrayList<DemandeChequier>();

		try {
			CriteriaBuilder cb = getSession().getCriteriaBuilder();
			CriteriaQuery<DemandeChequier> cq = cb.createQuery(DemandeChequier.class);
			Root<DemandeChequier> root = cq.from(DemandeChequier.class);
			cq.select(root);
			Query<DemandeChequier> query = getSession().createQuery(cq);
			listeChequiers = query.getResultList();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return listeChequiers;
	}

	public List<DemandeChequier> readAllDemandeChequierByStatus(StatutDemandeChequier statutDemandeChequier) {
		List<DemandeChequier> listeChequier = new ArrayList<DemandeChequier>();
		Integer statutInt = statutDemandeChequier.id;

		try {
			CriteriaBuilder cb = getSession().getCriteriaBuilder();
			CriteriaQuery<DemandeChequier> cq = cb.createQuery(DemandeChequier.class);
			Root<DemandeChequier> root = cq.from(DemandeChequier.class);
			cq.select(root)
			.where(cb.equal(root.get("statut"), statutInt));
			Query<DemandeChequier> query = getSession().createQuery(cq);
			listeChequier =  query.getResultList();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return listeChequier;
	}
}
