package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.AgentDAO;
import com.gestibank.models.Agent;


@Repository("agentDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class AgentDAO_Impl extends AbstractDAO implements AgentDAO {
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public AgentDAO_Impl() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* METHODES INTERFACE */
		public Boolean createAgent(Agent agent) {
			return persist(agent);
		}
		
		public Agent readAgentByMatricule(String matricule) {			
			 /**/return (Agent) findById(Agent.class, matricule);
		}
		
		public Boolean updateAgent(Agent agent) {
			return update(agent);
		}
		
		public Boolean deleteAgentByMatricule(String matricule) {
			/**/Agent agent = (Agent) findById(Agent.class,matricule);
			return delete(agent);
		}
		
		public List<Agent> readAllAgents() {
			List<Agent> listAgent = new ArrayList<Agent>();
			
            try {
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<Agent> cq = cb.createQuery(Agent.class);
                Root<Agent> root = cq.from(Agent.class);
                cq.select(root);
                Query<Agent> query = getSession().createQuery(cq);
                listAgent =  query.getResultList();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
             return listAgent;
		}
		
		public List<Agent> readAllAgentsByLoginAdmin(String loginAdmin) {			
			List<Agent> listAgent = new ArrayList<Agent>();
			
            try {
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<Agent> cq = cb.createQuery(Agent.class);
                Root<Agent> root = cq.from(Agent.class);
                cq.select(root)
                .where(cb.equal(root.get("adminAffecte"), loginAdmin));
                Query<Agent> query = getSession().createQuery(cq);
                listAgent =  query.getResultList();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return listAgent;
		}
		
		public List<Agent> searchAllAgentsByLoginAdmin(String loginAdmin, String matricule_nom) {
			List<Agent> listAgent = new ArrayList<Agent>();

            try {
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<Agent> cq = cb.createQuery(Agent.class);
                Root<Agent> root = cq.from(Agent.class);
                Predicate agentRestriction= cb.and(
                        cb.equal(root.get("adminAffecte"), loginAdmin),
                        cb.or(
                        	cb.equal(root.get("loginUser"), matricule_nom)),
                        	cb.equal(root.get("nom"), matricule_nom)                        
                        );
                cq.where(agentRestriction);
                Query<Agent> query = getSession().createQuery(cq);
                listAgent =  query.getResultList();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return listAgent;
		}
}