package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.AffectationDAO;
import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.TypeTriAffectionAdmin;
import com.gestibank.models.Affectation;

@Repository("affectationDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class AffectationDAO_Impl extends AbstractDAO implements AffectationDAO {
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public AffectationDAO_Impl() {
			// TODO Auto-generated constructor stub
		}
		
		
	/* METHODES INTERFACE */
		public Boolean createAffectation(Affectation affectation) {
			return persist(affectation);
		}
		
		public Affectation readAffectationById(int id) {
			return (Affectation) findById(Affectation.class, id);
		}
		
		public Boolean updateAffectation(Affectation affectation) {
			return update(affectation);
		}
		
		public Boolean deleteAffectationById(int id) {
			Affectation affectation = (Affectation) findById(Affectation.class, id);
			return delete(affectation);
		}
		
		//List 1
		public List<Affectation> readAllAffectations() {
			List<Affectation> listAffect = new ArrayList<Affectation>();
			
            try {
            	System.out.println("Entr�e dans AffectationDaoImpl readAllAffectations() try");
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<Affectation> cq = cb.createQuery(Affectation.class);
                Root<Affectation> root = cq.from(Affectation.class);
                cq.select(root);
                Query<Affectation> query = getSession().createQuery(cq);
                listAffect =  query.getResultList();

            } catch (Exception e) {
            	System.out.println("Entr�e dans AffectationDaoImpl readAllAffectations() catch");
                System.out.println(e.getMessage());
            }
             return listAffect;	
		}
		
		//List 2
		public List<Affectation> readAllAffectationsByStatut(StatutAffectation statutAffectation) {
			List<Affectation> listAffect = new ArrayList<Affectation>();
			Integer statutInt = statutAffectation.id;
			
			System.out.println("AffectationDaoImpl readAllAffectationsByStatus() statutInt = " + statutInt);
			try {
				  System.out.println("Entr�e dans AffectationDaoImpl readAllAffectationsByStatus() try");
				  CriteriaBuilder cb = getSession().getCriteriaBuilder();
				  CriteriaQuery<Affectation> cq = cb.createQuery(Affectation.class);
				  Root<Affectation> root = cq.from(Affectation.class);
				  Predicate predicateStatut = cb.equal(root.get("affectation"), statutInt);
				  System.out.println("root.get(\"statut\") = " + root.get("statut"));
				  cq.select(root).where(predicateStatut);
				  TypedQuery<Affectation> query = getSession().createQuery(cq);
				  listAffect = query.getResultList();
				  System.out.println("query = " + query.getResultList().size());
				  /*cq.select(root)
				  .where(cb.equal(root.get("statut"), statutInt));
				  System.out.println("affichage de statutInt = " + statutInt);
				  listAffect = query.getResultList();
				  System.out.println("listAffect  = " + listAffect);*/
				
			} catch (Exception e) {
				System.out.println("Entr�e dans AffectationDaoImpl readAllAffectationsByStatus() catch");
				e.printStackTrace();
			}
			return listAffect;
		}
		
		//List 3
		public List<Affectation> readAllAffectationsByStatutOrderByTypeTriAffectationAdmin(
				StatutAffectation statutAffection, TypeTriAffectionAdmin order) {			
			Integer statutInt = statutAffection.id;
			TypeTriAffectionAdmin triDate = order;
			String triDateString = "";
			List<Affectation> listAffectation = new ArrayList<Affectation>();
			
			switch (triDate) {
			case date_affectation:
				triDateString = "dateAffectation";
				break;
			case date_demande:
				triDateString = "dateCreation";				
				break;
			default:
				break;
			}
			
            try {
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<Affectation> cq = cb.createQuery(Affectation.class);
                Root<Affectation> root = cq.from(Affectation.class);
                Predicate affectationRestriction = cb.equal(root.get("statut"), statutInt);                
                cq.where(affectationRestriction).orderBy(cb.desc(root.get(triDateString)));
                Query<Affectation> query = getSession().createQuery(cq);
                listAffectation = query.getResultList();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return listAffectation;	
		}
			
		//list 4
		public List<Affectation> readAllAffectationsOrderByTypeTriAffectationAdmin(TypeTriAffectionAdmin order) {
			List<Affectation> listAffectation = new ArrayList<Affectation>();
			
			TypeTriAffectionAdmin triDate = order;
			String triDateString = "";
			
			switch (triDate) {
			case date_affectation:
				triDateString = "dateAffectation";
				break;
			case date_demande:
				triDateString = "dateCreation";				
				break;
			default:
				break;
			}
			
			try {
				CriteriaBuilder cb = getSession().getCriteriaBuilder();
				CriteriaQuery<Affectation> cq = cb.createQuery(Affectation.class);
                Root<Affectation> root = cq.from(Affectation.class);
                cq.select(root)
                .orderBy(cb.desc(root.get(triDateString)));
                Query<Affectation> query = getSession().createQuery(cq);
                listAffectation =  query.getResultList();
				
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			return listAffectation;
		}
}

