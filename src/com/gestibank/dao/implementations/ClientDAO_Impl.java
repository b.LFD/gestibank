package com.gestibank.dao.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.ClientDAO;
import com.gestibank.models.Client;

@Repository("clientDAO")
@Transactional//Present sur la couche DAO uniquement pour les tests de la DAO
public class ClientDAO_Impl extends AbstractDAO implements ClientDAO {
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public ClientDAO_Impl() {
			// TODO Auto-generated constructor stub
		}

		
	/* METHODES INTERFACE */
		public Boolean createClient(Client client) {	
			return persist(client);
		}
		
		public Client readClientByLogin(String login) {
			/**/return (Client) findById(Client.class, login);
		}
		
		public Boolean updateClient(Client client) {
			return update(client);
		}
		
		public Boolean deleteClientByLogin(String login) {
			/**/Client client = (Client) findById(Client.class, login);
			return delete(client);
		}

		public List<Client> readAllClients() {			
			List<Client> listClient = new ArrayList<Client>();
			
			try {
				  CriteriaBuilder cb = getSession().getCriteriaBuilder();
				  CriteriaQuery<Client> cq = cb.createQuery(Client.class);
				  Root<Client> root = cq.from(Client.class);
				  cq.select(root);
				  Query<Client> query = getSession().createQuery(cq);
				  listClient = query.getResultList();
				
			} catch (Exception e) {
			  System.out.println(e.getMessage());
			}
			return listClient;
		}

		public List<Client> readAllClientsByMatricule(String matricule) {
			List<Client> listClient = new ArrayList<Client>();
			
			try {
                CriteriaBuilder cb = getSession().getCriteriaBuilder();
                CriteriaQuery<Client> cq = cb.createQuery(Client.class);
                Root<Client> root = cq.from(Client.class);
                cq.select(root)
                .where(cb.equal(root.get("agentAffecte"), matricule));
                Query<Client> query = getSession().createQuery(cq);
                listClient =  query.getResultList();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
			return listClient;
		}

		public List<Client> searchAllClientsByMatricule(String matricule, String loginClient_nom) {
			List<Client> listClient = new ArrayList<Client>();
			
			 try {
	                CriteriaBuilder cb = getSession().getCriteriaBuilder();
	                CriteriaQuery<Client> cq = cb.createQuery(Client.class);
	                Root<Client> root = cq.from(Client.class);
	                Predicate clientRestriction= cb.and(
	                        cb.equal(root.get("agentAffecte"), matricule),
	                        cb.or(
	                        	cb.equal(root.get("loginUser"), loginClient_nom),
	                        	cb.equal(root.get("nom"), loginClient_nom))
	                        );
	                cq.where(clientRestriction);
	                Query<Client> query = getSession().createQuery(cq);
	                listClient =  query.getResultList();
	            } catch (Exception e) {
	                System.out.println(e.getMessage());
	            }			
			return listClient;
		}
}
