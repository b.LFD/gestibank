package com.gestibank.dao.implementations;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import  org.springframework.beans.factory.annotation.Autowired;


public class AbstractDAO {
	@Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
    //Recup�ration d'une session Hibernate
    public Session getSession() {
//    	System.out.println(sessionFactory);
        return sessionFactory.getCurrentSession();
    }
    
    //Enregistrer un objet
    public boolean persist(Object entity) {
        Boolean isPersist = false;
        
        try {
            getSession().saveOrUpdate(entity);
            getSession().persist(entity);
            getSession().flush();
            isPersist =true;
            System.out.println("AbstractDAO persist() isPersist = " + isPersist);
        } catch (Exception e) {            
            isPersist =false; 
            System.out.println("AbstractDAO persist() isPersist = " + isPersist);
            e.printStackTrace();
        }
                
        return isPersist;
    }

    //Recup�rer un objet by id
    public Object findById(Class<?> class1, int id) {
    	System.out.println("entr�e dans findById");
        Object myObject = new Object();
        
        try {
        	System.out.println("entr�e dans findById Try id = " + id);
            myObject = getSession().get(class1, id);
            //System.out.println("entr�e dans findById Try Object ID = " + myObject.toString());
            
                    
        } catch (Exception e) {
        	System.out.println("entr�e dans findById catch");
        	e.printStackTrace();
        }        
        
        return myObject;        
    }
    
    public Object findById(Class<?> class1, String id) {
    	System.out.println("entr�e dans findById");
        Object myObject = new Object();
        
        try {
        	System.out.println("entr�e dans findById Try id = " + id);
            myObject = getSession().get(class1, id);
        } catch (Exception e) {
        	System.out.println("entr�e dans findById catch");
            System.out.println(e.getMessage());
        }
        
        return myObject;        
    }

    //Mise � jour d'un objet
    public  Boolean update(Object entity) {
        Boolean isUpdate = false;
        
        try {
            getSession().merge(entity);
            getSession().flush();            
            isUpdate = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return isUpdate;
    }

    //Supprimer un objet
    public Boolean delete(Object entity) {
        Boolean isDelete = false;
        
        try {
            getSession().delete(entity);
            getSession().flush();
            isDelete = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return isDelete;        
    }    

}
