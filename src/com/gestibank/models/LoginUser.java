package com.gestibank.models;

public class LoginUser {

	private String login;
	private String password;
	
	public LoginUser() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
