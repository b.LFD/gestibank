package com.gestibank.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gestibank.enumerations.SituationMatrimoniale;
import com.gestibank.enumerations.StatutAffectation;

@Entity
@Table
public class Affectation {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(nullable = false)
		private int id;
		private StatutAffectation statut;
		
		@ManyToOne
	    @JoinColumn( name="agentAffecte")
		private Agent agentAffecte;
		private Date dateCreation;
		private Date dateAffectation;
		private String nom;
		private String prenom;
		private String mail;
		private String adresse;
		private String tel;
		private String raisonRefus;
		private Date dateNaissance;
		private SituationMatrimoniale SituationMatrimoniale;
		@ManyToOne
	    @JoinColumn( name="adminAffecte")
		/**/		private Administrateur adminAffecte;
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Affectation() {
			this.dateCreation = new Date();
		}
		
		
	/* GETERS-SETERS */
		//adresse
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		
		//agentAffecte
		public Agent getAgentAffecte() {
			return agentAffecte;
		}
		public void setAgentAffecte(Agent agentAffecte) {
			this.agentAffecte = agentAffecte;
		}
		
		//dateAffectation
		public Date getDateAffectation() {
			return dateAffectation;
		}
		public void setDateAffectation(Date dateAffectation) {
			this.dateAffectation = dateAffectation;
		}
		
		//dateCreation
		public Date getDateCreation() {
			return dateCreation;
		}
		
		//id
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		//mail
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		
		//nom
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		
		//prenom
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		
		//statut
		public StatutAffectation getStatut() {
			return statut;
		}
		public void setStatut(StatutAffectation statut) {
			if(statut == StatutAffectation.en_attente_agent) {
				dateAffectation = new Date();
			}
			this.statut = statut;
		}
		
		//tel
		public String getTel() {
			return tel;
		}
		public void setTel(String tel) {
			this.tel = tel;
		}
		
		//raisonRefus
		public String getRaisonRefus() {
			return raisonRefus;
		}
		public void setRaisonRefus(String raisonRefus) {
			this.raisonRefus = raisonRefus;
		}
		
		//adminAffecte
		public Administrateur getAdminAffecte() {
			return adminAffecte;
		}
		public void setAdminAffecte(Administrateur adminAffecte) {
			this.adminAffecte = adminAffecte;
		}
		
		//dateNaissance
		public Date getDateNaissance() {
			return dateNaissance;
		}
		public void setDateNaissance(Date dateNaissance) {
			this.dateNaissance = dateNaissance;
		}
		
		//situationMatrimoniale
		public SituationMatrimoniale getSituationMatrimoniale() {
			return SituationMatrimoniale;
		}
		public void setSituationMatrimoniale(SituationMatrimoniale situationMatrimoniale) {
			SituationMatrimoniale = situationMatrimoniale;
		}
		
		public String toString() {
		String s  = "-Id: "+this.getId()+ " -Nom: "+this.getNom()+" -Prenom:"+this.getPrenom()+" -Mail: "+this.getMail()+" -Tel:"+this.getTel()+"\n"
				+" -Date de naissance:"+this.getDateNaissance().toString()+" -Adresse:"+this.getAdresse()+"\n"
				+" -Date De cr�ation:"+this.getDateCreation().toString()+" -Agent affect�:"+this.getAgentAffecte().getMatricule()+"\n"
				+" -Admin affect�:"+this.getAdminAffecte().getLogin()+" -Agent affect�:"+this.getAgentAffecte().getMatricule()+" - Situation Matrimoniale:"+this.getSituationMatrimoniale()+"\n";
		return s;
		}
}
