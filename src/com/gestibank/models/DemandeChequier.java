package com.gestibank.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.gestibank.enumerations.StatutDemandeChequier;

@Entity
@Table
public class DemandeChequier {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(nullable = false)
		private Integer id;
		//@OneToOne(mappedBy = "demandeChequier")
		@OneToOne
		@JoinColumn(name="ribCompte")
		private CompteCourant compte;
		
		private StatutDemandeChequier statut;
		private Date dateDemande;
		private Date dateTraitement;
		
		@ManyToOne
	    @JoinColumn( name="agentAffecte" )
		private Agent agentAffecte;
		
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public DemandeChequier() {
			dateDemande = new Date();
		}
		
		
	/* GETERS-SETERS */
		//dateDemande
		public Date getDateDemande() {
			return dateDemande;
		}
		
		//dateTraitement
		public Date getDateTraitement() {
			return dateTraitement;
		}
		
		//ribCompte
		public CompteBancaire getCompte() {
			return this.compte;
		}
		public void setCompte(CompteCourant compte) {
			this.compte = compte;
		}
		
		//statut
		public StatutDemandeChequier getStatut() {
			return statut;
		}
		public void setStatut(StatutDemandeChequier statut) {
			if(statut == StatutDemandeChequier.accepte || statut == StatutDemandeChequier.refuse)
				dateTraitement = new Date();
			this.statut = statut;
		}
		
		//agentAffecte
		public Agent getAgentAffecte() {
			return agentAffecte;
		}
		public void setAgentAffecte(Agent agentAffecte) {
			this.agentAffecte = agentAffecte;
		}
		
		//id
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		
	/* toString */
		public String toString() {
			String result = 	"Demande chiquier: "+getId()+" pour le ccp: "+getCompte().getRib()+"   -   "+getStatut()+"\n"
							+	"   affile a l'agent: "+getAgentAffecte().getMatricule()+", le: "+getDateDemande().toString()+"\n"
							+	"         Validation: "+getDateTraitement().toString();
			return result;
		}
}
