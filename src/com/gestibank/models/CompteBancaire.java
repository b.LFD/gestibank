package com.gestibank.models;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.List;

@Entity
@Inheritance (strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class CompteBancaire {
	
	/* ATTRIBUTS DE CLASSE */
	
		//Attributs li�s aux colonnes de BDD
		@Id
		@Column(nullable = false)
		private String rib;
		
		@OneToMany (targetEntity = Transaction.class,
					mappedBy ="compte",
					cascade = CascadeType.ALL //Suppression des transactions lorsqu'un compteBancaire est supprim�
					)
		private List<Transaction> listeTransactions;
		
		@ManyToOne  
		@JoinColumn( name="login" )
		private Client propritaireClient;
		
		private Date dateCreation;
		private Double solde;
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public CompteBancaire() {
			dateCreation = new Date();
			listeTransactions = new ArrayList<Transaction>();
			rib = "";
			solde = 0.0;
		}
		
	/* GETERS-SETERS */
		//dateCreation
		public Date getDateCreation() {
			return dateCreation;
		}
		
		//listeTransactions
		@OneToMany (targetEntity = Transaction.class, mappedBy ="compte" )
		public List<Transaction> getListeTransactions() {
			return listeTransactions;
		}
		public void setListeTransactions(List<Transaction> listeTransactions) {
			this.listeTransactions = listeTransactions;
		}
		
		//propritaireClient
		public Client getPropritaireClient() {
			return propritaireClient;
		}
		public void setPropritaireClient(Client propritaireClient) {
			this.propritaireClient = propritaireClient;
		}
		
		//rib
		public String getRib() {
			return rib;
		}
		public void setRib(String rib) {
			this.rib = rib;
		}
		
		//montant
		public Double getSolde() {
			return solde;
		}
		public void setSolde(Double solde) {
			this.solde = solde;
		}
		
	/* toString */
		public String toString() {
			String result = 	getSolde()+" euro"+((getSolde()==0 || getSolde()==1)?"":"s")
						+"\n"+	"   "+getPropritaireClient().getLogin()+"   -   "+getDateCreation().toString();
			return result;
		}
}
