package com.gestibank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Utilisateur {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		
		@Id
		@Column(nullable = false)
		/**/		protected String loginUser;
		/**/		protected String password;
		protected String nom;
		protected String prenom;
		protected String mail;
		protected String tel;
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		protected Utilisateur() {
			this.loginUser = "";
			this.password = genererPwd();
		}
		//Constructeur avec login et password
		protected Utilisateur(String loginUser) {
			super();
			setLoginUser(loginUser);
		}
	
	
	/* GETERS-SETERS */
		//loginUser
		protected String getLoginUser() {
			return loginUser;
		}
		protected void setLoginUser(String loginUser) {
			this.loginUser = loginUser;
		}
		
		//password
		public String getPassword() {
			return password;
		}
		
		//nom
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		
		//prenom
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		
		//mail
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		
		//tel
		public String getTel() {
			return tel;
		}
		public void setTel(String tel) {
			this.tel = tel;
		}
		
	/* toString */
		@Override
		public String toString() {
			String s  = "-Login: "+this.getLoginUser()+ " -Nom: "+this.getNom()+" -Prenom:"+this.getPrenom()+" -Mail: "+this.getMail()+
						" -Tel:"+this.getTel()+"\n";
			return s;
					
		}

	/* PRIVATE */
		private String genererPwd() {
			String pwd = "";
			String charPossible = "AZERTYUIOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjklmwxcvbn0123456789";
			for( int i=0;i<10;i++) {
				pwd += charPossible.charAt((int)Math.random()*charPossible.length()+1);
			}
			return pwd;
		}
}
