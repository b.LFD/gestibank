package com.gestibank.models;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Agent extends Utilisateur {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
	
		private Date dateDebutContrat;
		
		@ManyToOne
	    @JoinColumn( name="adminAffecte")
		private Administrateur adminAffecte;
		
		@OneToMany( targetEntity=Client.class,
					mappedBy="loginUser")
		private List<Client> listeClients;
		
		@OneToMany( targetEntity=Affectation.class,
					mappedBy="agentAffecte")
		private List<Affectation> listeAffectations;
		
		@OneToMany( targetEntity=DemandeChequier.class,
					mappedBy="agentAffecte")
		private List<DemandeChequier> listeDemandeChequier;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Agent() {
			super();
			listeClients = new ArrayList<Client>();
			listeAffectations = new ArrayList<Affectation>();
			listeDemandeChequier = new ArrayList<DemandeChequier>();
		}
		//Constructeurs avec login et password
		public Agent(String matricule) {
			super(matricule);
			dateDebutContrat = new Date();
			listeClients = new ArrayList<Client>();
			listeAffectations = new ArrayList<Affectation>();
			listeDemandeChequier = new ArrayList<DemandeChequier>();
		}
	
	
	/* GETERS-SETERS */
		//dateDebutContrat
		public Date getDateDebutContrat() {
			return dateDebutContrat;
		}
		
		//listeClients
		public List<Client> getListeClients() {
			return listeClients;
		}
		public void setListeClients(List<Client> listeClients) {
			this.listeClients = listeClients;
		}

		//listeAffectations
		public List<Affectation> getListeAffectations() {
			return listeAffectations;
		}
		public void setListeAffectations(List<Affectation> listeAffectations) {
			this.listeAffectations = listeAffectations;
		}
		
		//listeDemandeChequier
		public List<DemandeChequier> getListeDemandeChequier() {
			return listeDemandeChequier;
		}
		public void setListeDemandeChequier(List<DemandeChequier> listeDemandeChequier) {
			this.listeDemandeChequier = listeDemandeChequier;
		}
		
		//matricule
		public String getMatricule() {
			return super.getLoginUser();
		}
		public void setMatricule(String matricule) {
			super.setLoginUser(matricule);
		}
		
		//password
		public String getPassword() {
			return super.getPassword();
		}
		
		//adminAffecte
		public Administrateur getAdminAffecte() {
			return adminAffecte;
		}
		public void setAdminAffecte(Administrateur adminAffecte) {
			this.adminAffecte = adminAffecte;
		}
		
	/* TO_STRING */
		@Override
		public String toString() {
			String s = super.toString();
			s= s +" -Matricule:"+this.getMatricule()+" -DateDebutContrat:"+this.getDateDebutContrat().toString()+" -Admin affect�:"+this.getAdminAffecte().getLogin()+"\n";
			return s;
		}
}
