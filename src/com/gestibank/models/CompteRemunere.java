package com.gestibank.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class CompteRemunere extends CompteBancaire {
	
	/* ATTRIBUTS DE CLASSE */
		//Constantes
	    @Transient
		private final double PLAFOND_REMUNERATION = 1000.00;
	    @Transient
		private final double TAUX_REMUNERATION = 0.02;
	    @Transient
		private final int NB_JOUR_CREDIT = 365;
	
		//Attributs li�s aux colonnes de BDD
		//@Id
		//@GeneratedValue(strategy = GenerationType.IDENTITY)
		//private Integer id;
	    /**/		private double montantRemuneration;
	    /**/		private int nbJoursPostRemuneration;
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public CompteRemunere() {
			super();
		}
		

	/* GETERS CONSTANTES */
		//NB_JOUR_CREDIT
		public int getNB_JOUR_CREDIT() {
			return NB_JOUR_CREDIT;
		}
		
		//PLAFOND_REMUNERATION
		public double getPLAFOND_REMUNERATION() {
			return PLAFOND_REMUNERATION;
		}
		
		//TAUX_REMUNERATION
		public double getTAUX_REMUNERATION() {
			return TAUX_REMUNERATION;
		}
		
	/* GETERS-SETERS */
		//montantRemuneration
		public double getMontantRemunerationt() {
			return montantRemuneration;
		}
		public void setMontantRemuneration(double montantRemuneration) {
			this.montantRemuneration = montantRemuneration;
		}
		
		//nbJoursPostRemuneration
		public int getNbJoursPostRemuneration() {
			return nbJoursPostRemuneration;
		}
		public void setNbJoursPostRemuneration(int nbJoursPostRemuneration) {
			this.nbJoursPostRemuneration = nbJoursPostRemuneration;
		}
		
		//id
//		private Integer getId() {
//			return id;
//		}
		
//		//methode de refresh de rib une fois id changer
//		public void refreshRib() {
//			this.setRib("CR"+"_"+getId());
//		}
		
	/* toString */
		public String toString() {
			String result = "CompteRemunere_: "+super.toString()+"\n"
						+	"      proprietes:\n"
						+	"         montant_remuneration (en cours): "+getMontantRemunerationt()+"   -   jour: "+(getNB_JOUR_CREDIT()- getNbJoursPostRemuneration())+"\n";
			return result;
		}
}
