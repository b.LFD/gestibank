package com.gestibank.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gestibank.enumerations.TypeOperation;
import com.gestibank.enumerations.TypeTransaction;

@Entity
@Table
public class Transaction {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		
		@Id 
		@GeneratedValue( strategy=GenerationType.IDENTITY )
		@Column(nullable = false)
		private int id;
		
		private TypeTransaction typeTransaction;
		
		@ManyToOne
		@JoinColumn( name="ribCompte" )
		private CompteBancaire compte;
		
		private double montant;
		private Date dateTransaction;
		private TypeOperation typeOperation;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Transaction() {
			dateTransaction = new Date();
		}
		
		
	/* GETERS-SETERS */
		//id
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		//compte
		public CompteBancaire getCompte() {
			return compte;
		}
		public void setCompte(CompteBancaire compte) {
			this.compte = compte;
		}
		
		//montant
		public double getMontant() {
			return montant;
		}
		public void setMontant(double montant) {
			this.montant = montant;
		}
		
		//typeOperation
		public TypeOperation getTypeOperation() {
			return typeOperation;
		}
		public void setTypeOperation(TypeOperation typeOperation) {
			this.typeOperation = typeOperation;
			
			if(	this.typeOperation == TypeOperation.agio
			||	this.typeOperation == TypeOperation.debit_simple
			||	this.typeOperation == TypeOperation.envoi_virement )	setTypeTransaction(TypeTransaction.debit);
			else														setTypeTransaction(TypeTransaction.credit);
		}
		
		//typeTransaction
		public TypeTransaction getTypeTransaction() {
			return typeTransaction;
		}
		private void setTypeTransaction(TypeTransaction typeTransaction) {
			this.typeTransaction = typeTransaction;
		}
		
		//dateTransaction
		public Date getDateTransaction() {
			return dateTransaction;
		}
		
		@Override
		public String toString() {
			String s = this.getTypeOperation()+" - "+this.getTypeTransaction()+" - "+this.getDateTransaction().toString()+" - "+this.getMontant();
			return s;
		}
		
	/* toString */
		public String toStringTest() {
			
			String typeCompte = (getCompte() instanceof CompteCourant) ? "courant" : ((getCompte() instanceof CompteRemunere) ? "remunere" : "inconnu");
			
			String rendu =		"Transaction "+getId()+":   "+getTypeTransaction()+"  -  "+getTypeOperation()+"   -   montant:"+getMontant()+"\n"
							+	"   details:       compte "+typeCompte+": "+compte.getRib()+"      le: "+getDateTransaction().toString();
			
			return rendu;
		}
}
