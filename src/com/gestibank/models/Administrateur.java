package com.gestibank.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Administrateur extends Utilisateur {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		
		@OneToMany(mappedBy="adminAffecte")
		private List<Agent> listeAgents;
		
		@OneToMany(mappedBy="adminAffecte")
		private List<Affectation> listeAffectations;
		
		
	/* CONSTRUCTEURS */
		//Constructeur sans parametres
		public Administrateur() {
			super();
			listeAgents = new ArrayList<Agent>();
			listeAffectations = new ArrayList<Affectation>();
		}
		//Contructeur avec login et password
		public Administrateur(String login) {
			super(login);
			listeAgents = new ArrayList<Agent>();
			listeAffectations = new ArrayList<Affectation>();
		}
		
		
	/* GETERS-SETERS */
		//listeAgents
		public List<Agent> getListeAgents() {
			return listeAgents;
		}
		public void setListeAgents(List<Agent> listeAgents) {
			this.listeAgents = listeAgents;
		}
		
		//listeAffectation
		public List<Affectation> getListeAffectations() {
			return listeAffectations;
		}
		public void setListeAffectations(List<Affectation> listeAffectations) {
			this.listeAffectations = listeAffectations;
		}
		
		//login
		public String getLogin() {
			return super.getLoginUser();
		}
		public void setLogin(String login) {
			super.setLoginUser(login);
		}
		
		@Override
		public String toString() {
			return super.toString();
		}
}
