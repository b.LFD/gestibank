package com.gestibank.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class CompteCourant extends CompteBancaire {
	
	/* ATTRIBUTS DE CLASSE */
		//Constantes
	    @Transient
		private final double TAUX_INTERET = 0.18;
	    @Transient
		private final int NB_JOUR_DEBIT = 90;
	
		//Attributs lies aux colonnes de BDD
		//@Id
		//@GeneratedValue(strategy = GenerationType.IDENTITY)
		//private Integer id;
		private double decouvert;
		private double entreeMoyenneMensuelle;
		/**/		private double montantInteret;
		/**/		private int nbJoursPostDecouvert;
		
		@OneToOne(cascade=CascadeType.ALL)
		@JoinColumn(name="idDemandeChequier")
		private DemandeChequier demandeChequier;
		
		//Attributs calcules
		private double plafondDecouvert;
	
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public CompteCourant() {
			super();
		}
		

	/* GETERS CONSTANTES */
		//NB_JOUR_DEBIT
		public int getNB_JOUR_DEBIT() {
			return NB_JOUR_DEBIT;
		}
		
		//TAUX_INTERET
		public double getTAUX_INTERET() {
			return TAUX_INTERET;
		}
		
	/* GETERS-SETERS */
		
		//decouvert
		public double getDecouvert() {
			return decouvert;
		}
		public void setDecouvert(double decouvert) {
			this.decouvert = decouvert;
		}
		
		//plafondDecouvert
		public double getPlafondDecouvert() {
			return plafondDecouvert;
		}
		
		//montantInteret
		public double getMontantInteret() {
			return montantInteret;
		}
		public void setMontantInteret(double montantInteret) {
			this.montantInteret = montantInteret;
		}
		
		//nbJoursPostDecouvert
		public int getNbJoursPostDecouvert() {
			return nbJoursPostDecouvert;
		}
		public void setNbJoursPostDecouvert(int nbJoursPostDecouvert) {
			this.nbJoursPostDecouvert = nbJoursPostDecouvert;
		}
		
		//demandeChequier
		public DemandeChequier getDemandeChequier() {
			return demandeChequier;
		}
		public void setDemandeChequier(DemandeChequier demandeChequier) {
			this.demandeChequier = demandeChequier;
		}
		
		//entreeMoyenneMensuelle
		public double getEntreeMoyenneMensuelle() {
			return entreeMoyenneMensuelle;
		}
		public void setEntreeMoyenneMensuelle(double entreeMoyenneMensuelle) {
			this.entreeMoyenneMensuelle = entreeMoyenneMensuelle;
			this.plafondDecouvert = this.entreeMoyenneMensuelle * 0.4;
		}
		
		//id
//		public Integer getId() {
//			return id;
//		}
		
//		//methode de refresh de rib une fois id changer
//		public void refreshRib() {
//			this.setRib("CC"+"_"+getId());
//		}
		
	/* toString */
		public String toString() {
			String result = "CompteCourant_: "+super.toString()+"\n"
						+	"      proprietes:\n"
						+	"         decouvert_max: "+getPlafondDecouvert()+"   -   decouvert_autorise: "+getDecouvert()+"\n"
						+	"         montant_Interet (en cours): "+getMontantInteret()+"   -   jour_avant_Debit: "+(getNB_JOUR_DEBIT()-getNbJoursPostDecouvert())+"\n"
						+	"      EMM client: "+getEntreeMoyenneMensuelle();
			return result;
		}
}
