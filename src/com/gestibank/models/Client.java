package com.gestibank.models;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.gestibank.enumerations.SituationMatrimoniale;

@Entity
@Table
public class Client extends Utilisateur {
	
	/* ATTRIBUTS DE CLASSE */
		//Attributs li�s aux colonnes de BDD
		private SituationMatrimoniale situation;
		private Date dateNaissance;
		private String adresse;
		private Date dateCreation;
		
		@OneToMany( targetEntity=CompteBancaire.class,
					mappedBy="rib",
					cascade=CascadeType.ALL)
		private List<CompteBancaire> comptesBancaires;
		
		@ManyToOne
		@JoinColumn( name="agentAffecte")
		private Agent agentAffecte;
		
		
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public Client() {
			this("");
		}
		//Constructeurs avec login
		public Client(String login) {
			super(login);
			dateCreation = new Date();
			comptesBancaires = new ArrayList<CompteBancaire>();
		}
		
		public Client (String login, String nom, String prenom, String mail,String tel, String adresse,Agent agentAffecte,Date dateNaissance, SituationMatrimoniale situation) {
			this(login);
			super.setNom(prenom);
			super.setPrenom(prenom);
			super.setMail(mail);
			super.setTel(tel);
			this.adresse= adresse;
			this.agentAffecte = agentAffecte;
			this.dateNaissance = dateNaissance;
			this.situation = situation;
		}
		
		
	/* GETERS-SETERS */
		//adresse
		public String getAdresse() {
			return adresse;
		}
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		
		//agentAffecte
		public Agent getAgentAffecte() {
			return agentAffecte;
		}
		public void setAgentAffecte(Agent agentAffecte) {
			this.agentAffecte = agentAffecte;
		}
		
		//comptesBancaires
		public List<CompteBancaire> getComptesBancaires() {
			return comptesBancaires;
		}
		public void setComptesBancaires(List<CompteBancaire> comptesBancaires) {
			this.comptesBancaires = comptesBancaires;
		}
		
		//dateCreation
		public Date getDateCreation() {
			return dateCreation;
		}
		
		//dateNaissance
		public Date getDateNaissance() {
			return dateNaissance;
		}
		public void setDateNaissance(Date dateNaissance) {
			this.dateNaissance = dateNaissance;
		}
		
		//situation
		public SituationMatrimoniale getSituation() {
			return situation;
		}
		public void setSituation(SituationMatrimoniale situation) {
			this.situation = situation;
		}
		
		//login
		public String getLogin() {
			return super.getLoginUser();
		}
		public void setLogin(String login) {
			super.setLoginUser(login);
		}
		
		@Override
		public String toString() {
			String s = this.getLogin()+" - "+ super.getNom()+" - "+super.getPrenom()+" - "+this.getSituation()+" - "
						+this.getDateNaissance().toString()+" - "+this.getAdresse()+ " - "+this.getDateCreation().toString()+" - "
						+this.getAgentAffecte().getMatricule();
			return s;
		}
		
		public String toStringTest() {
			String s = super.toString();
			s = s + " -Situation:"+this.getSituation()+" -Date de naissance:"+this.getDateNaissance().toString()+" -Adresse:"+this.getAdresse()+"\n"
					+" -Date De cr�ation:"+this.getDateCreation().toString()+" -Agent affect�:"+this.getAgentAffecte().getMatricule()+"\n";
			return s;
		}
		
}
