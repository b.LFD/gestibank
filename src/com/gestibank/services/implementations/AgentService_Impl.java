package com.gestibank.services.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.CompteCourant;
import com.gestibank.models.CompteRemunere;
import com.gestibank.models.DemandeChequier;
import com.gestibank.models.Transaction;
import com.gestibank.services.interfaces.AgentService;
import com.gestibank.services.interfaces.ClientService;
import com.gestibank.dao.interfaces.*;
import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.StatutDemandeChequier;


@Service("agentService")
@Transactional

public class AgentService_Impl implements AgentService {


	/* INJECTIONS DAO */
	@Autowired
	private ClientDAO clientDAO;
	@Autowired
	private AgentDAO agentDAO;
	@Autowired
	private CompteCourantDAO comptecourantDAO;
	@Autowired
	private CompteRemunereDAO compteremunereDAO;
	@Autowired
	private AffectationDAO affectationDAO;
	@Autowired
	private DemandeChequierDAO demandechequierDAO;
	@Autowired
	private ClientService clientservice;

	/* CONSTRUCTEURS */
	//Constructeurs sans parametres
	public AgentService_Impl() {
		// TODO Auto-generated constructor stub
	}

	/* METHODES INTERFACE */
	public Boolean verifCredentials(String login, String pwd) {
		boolean okVerifCredentials = false;
		Agent agentTemp = agentDAO.readAgentByMatricule(login);
		if (!(agentTemp ==null)) {
			okVerifCredentials = agentTemp.getPassword().equals(pwd);
		}
		return okVerifCredentials;
	}
	public List<Client> readListClientsByMatricule(String matricule, String login_nom) {
		List<Client> LClient = new ArrayList<Client>(clientDAO.searchAllClientsByMatricule(matricule, login_nom));
		return 	LClient;
	}
	public  Boolean updateClientByMatricule(String matricule, Client client) {
		boolean okUpdateClientByMatricule = false;
		if (matricule.equals(client.getAgentAffecte().getMatricule())) {
			okUpdateClientByMatricule = clientDAO.updateClient(client);
		}
		return okUpdateClientByMatricule;
	}
	public Boolean deleteClientByMatricule(String matricule,Client client) {
		boolean okDeleteClientByMatricule = false;
		if (matricule.equals(client.getAgentAffecte().getMatricule())) {
			okDeleteClientByMatricule = clientDAO.deleteClientByLogin(client.getLogin());
		}
		return okDeleteClientByMatricule;
	}
	public Boolean printListClients(List<Client> listClient) {
		boolean okPrintListClients = false;
		File file = new File("Liste_des_clients.txt");
		PrintWriter writer;
		try {
			writer = new PrintWriter(file);
			for(Client c: listClient) {
				writer.println(c.toString());
				okPrintListClients = true;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return okPrintListClients;
	}
	public Boolean createCompteBancaireByMatricule(String matricule, CompteBancaire compte) {
		boolean okCreateCompteBancaireByMatricule = false;
		if (matricule.equals(compte.getPropritaireClient().getAgentAffecte().getMatricule())) {
			if (compte instanceof CompteCourant) {
				okCreateCompteBancaireByMatricule = comptecourantDAO.createCompteCourant((CompteCourant)compte);
				if(okCreateCompteBancaireByMatricule) {
					List<CompteCourant> listCC = comptecourantDAO.readAllCompteCourantsByLogin(compte.getPropritaireClient().getLogin());
					for(CompteCourant cc : listCC) {
						//cc.refreshRib();
						comptecourantDAO.updateCompteCourant(cc);
					}
				}
			}
			else if (compte instanceof CompteRemunere) {
				okCreateCompteBancaireByMatricule = compteremunereDAO.createCompteRemunere((CompteRemunere)compte);
				if(okCreateCompteBancaireByMatricule) {
					List<CompteRemunere> listCR = compteremunereDAO.readAllCompteRemuneresByLogin(compte.getPropritaireClient().getLogin());
					for(CompteRemunere cr : listCR) {
						//cr.refreshRib();
						compteremunereDAO.updateCompteRemunere(cr);
					}
				}
			}
			else {
				okCreateCompteBancaireByMatricule = false;
			}
		}			
		return okCreateCompteBancaireByMatricule;
	}		
	public Boolean deleteCompteBancaireByMatricule(String matricule, CompteBancaire compte) {
		boolean okDeleteCompteBancaireByMatricule = false;
		if (matricule.equals(compte.getPropritaireClient().getAgentAffecte().getMatricule())) {
			if (compte instanceof CompteCourant) {
				okDeleteCompteBancaireByMatricule = comptecourantDAO.createCompteCourant((CompteCourant)compte);				
			}
			else if (compte instanceof CompteRemunere) {
				okDeleteCompteBancaireByMatricule = compteremunereDAO.createCompteRemunere((CompteRemunere)compte);
			}
		}			
		return okDeleteCompteBancaireByMatricule;
	}
	public List<CompteBancaire> readListComptesBancaireByMatricule(String matricule, Client client) {
		List<CompteBancaire> ListcompteBancaire = new ArrayList<CompteBancaire>();
		if (matricule.equals(client.getAgentAffecte().getMatricule())) {
			ListcompteBancaire.addAll(comptecourantDAO.readAllCompteCourantsByLogin(client.getLogin()));
			ListcompteBancaire.addAll(compteremunereDAO.readAllCompteRemuneresByLogin(client.getLogin()));			
		}
		return ListcompteBancaire ;
	}
	public Boolean createTransactionByMatricule(String matricule, Client client, Transaction transaction) {
		boolean okcreateTransactionByMatricule = false;
		if (matricule.equals(client.getAgentAffecte().getMatricule())) {
			okcreateTransactionByMatricule =clientservice.createTransactionByLogin(client.getLogin(), transaction);
		}			
		return okcreateTransactionByMatricule;
	}
	public Boolean createVirementByMatricule(String matricule, Client client, Transaction transactionDebit, Transaction transactionCredit) {
		boolean okCreateVirementByMatriculetransactionDebit = false;
		boolean okcreateVirementByMatriculetransactionCredit = false;
		if (matricule.equals(client.getAgentAffecte().getMatricule())){
			okCreateVirementByMatriculetransactionDebit = clientservice.createTransactionByLogin(client.getLogin(),transactionDebit);
			if (okCreateVirementByMatriculetransactionDebit) {
				okcreateVirementByMatriculetransactionCredit = clientservice.createTransactionByLogin(client.getLogin(),transactionCredit);
			}
		}
		return (okcreateVirementByMatriculetransactionCredit) ;
	}
	public List<Affectation> readListPendingAffectationsByMatricule(String matricule) {
		List<Affectation> PendingAffectationByMatricule = new ArrayList<Affectation>();
		List<Affectation> AllAffectationByStatut = new ArrayList<Affectation>(affectationDAO.readAllAffectationsByStatut(StatutAffectation.en_attente_agent));
		for (Affectation a : AllAffectationByStatut) {
			if (a.getAgentAffecte().getMatricule().equals(matricule)) {
				PendingAffectationByMatricule.add(a);
			}
		}
		return PendingAffectationByMatricule;
	}
	public Boolean updateAffectationByMatricule(String matricule, Affectation affectation, Boolean validation) {
		boolean okUpdateAffectationByMatriculeUpdateAffectation = false;
		boolean okUpdateAffectationByMatriculeCreateClient = false;
		if (matricule.equals(affectation.getAgentAffecte().getMatricule())) {
			if (validation) {
				affectation.setStatut(StatutAffectation.accepte);
				okUpdateAffectationByMatriculeUpdateAffectation = affectationDAO.updateAffectation(affectation);
				Client nouveauClient = new Client("client_"+affectation.getId(),affectation.getNom(), affectation.getPrenom(),affectation.getMail(),affectation.getTel(),
									   affectation.getAdresse(),affectation.getAgentAffecte(),affectation.getDateNaissance(),affectation.getSituationMatrimoniale());
				okUpdateAffectationByMatriculeCreateClient = clientDAO.createClient(nouveauClient);
			} else {
				affectation.setStatut(StatutAffectation.refuse);
				affectation.setRaisonRefus("T'es trop pauvre frayr");
			}
		}
		return okUpdateAffectationByMatriculeUpdateAffectation && okUpdateAffectationByMatriculeCreateClient ;
	}
	public Boolean updateDemandeChequierByMatricule(String matricule, DemandeChequier demandeChequier, Boolean validation) {
		boolean okUpdateDemandeChequierByMatricule = false;
		if (matricule.equals(demandeChequier.getAgentAffecte().getMatricule())) {
			if (validation) {
				demandeChequier.setStatut(StatutDemandeChequier.accepte);
			}
			else {
				demandeChequier.setStatut(StatutDemandeChequier.refuse);
			}
			okUpdateDemandeChequierByMatricule = demandechequierDAO.updateDemandeChequier(demandeChequier);
			if(okUpdateDemandeChequierByMatricule) demandechequierDAO.deleteDemandeChequierById(demandeChequier.getId());
		}		
		return okUpdateDemandeChequierByMatricule;
	}
	public List<DemandeChequier> readListPendingDemandeChequierByMatricule(String matricule){
		List<DemandeChequier> PendingDemandeChequierByMatricule = new ArrayList<DemandeChequier>();
		List<DemandeChequier> AllDemandeChequierByStatut = new ArrayList<DemandeChequier>(demandechequierDAO.readAllDemandeChequierByStatus(StatutDemandeChequier.en_attente));
		for (DemandeChequier dc : AllDemandeChequierByStatut) {
			if (dc.getAgentAffecte().getMatricule().equals(matricule)) {
				PendingDemandeChequierByMatricule.add(dc);
			}
		}
		return PendingDemandeChequierByMatricule;
	}
	public Agent getAgent(String matricule) {
		return agentDAO.readAgentByMatricule(matricule);
	}
}