package com.gestibank.services.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.AdministrateurDAO;
import com.gestibank.dao.interfaces.AffectationDAO;
import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.services.interfaces.GuestService;

@Service("guestService")
@Transactional
public class GuestService_Impl implements GuestService {
	
	/* INJECTIONS DAO */
		@Autowired
		private AffectationDAO affectationDAO;
		@Autowired
		private AdministrateurDAO administrateurDAO;
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public GuestService_Impl() {
			// TODO Auto-generated constructor stub
		}
	
	/* METHODES INTERFACE */
		public Boolean createAffectation(Affectation affectation) {
			
			List<Administrateur> listAdmin = administrateurDAO.readAllAdministrateur();
			int i = (int)(Math.random() * listAdmin.size());
			affectation.setAdminAffecte(listAdmin.get(i));
			affectation.setStatut(StatutAffectation.en_attente_admin);
			
			boolean okCreateAffectation = false;
			okCreateAffectation = affectationDAO.createAffectation(affectation);
			return okCreateAffectation;
		}
}
