package com.gestibank.services.implementations;

import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gestibank.dao.interfaces.AdministrateurDAO;
import com.gestibank.dao.interfaces.AffectationDAO;
import com.gestibank.dao.interfaces.AgentDAO;
import com.gestibank.dao.interfaces.ClientDAO;
import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.TypeTriAffectionAdmin;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.services.interfaces.AdminService;
import com.gestibank.services.interfaces.AgentService;

@Service("adminService")
@Transactional
public class AdminService_Impl implements AdminService {
	
	/* INJECTIONS DAO */
	@Autowired
	private AffectationDAO affectationdao;
	@Autowired
	private AdministrateurDAO administrateurdao;
	@Autowired
	private AgentDAO agentdao;
	@Autowired
	private ClientDAO clientdao;
	@Autowired
	private AgentService agentservice;
	private PrintWriter writer;
	
	/* CONSTRUCTEURS */
		//Constructeurs sans parametres
		public AdminService_Impl() {
			// TODO Auto-generated constructor stub
		}
	
	/* METHODES INTERFACE */
		public Boolean verifCredentials(String login, String pwd) {
			boolean okVerifCredentials = false;
			System.out.println("AdminService login = " + login);
			Administrateur adminTemp = administrateurdao.readAdministrateurByLogin(login);
			if (!(adminTemp ==null)) {
				okVerifCredentials = adminTemp.getPassword().equals(pwd);
			} else {
				System.out.println("AdminService adminTemp == null");
			}
			return okVerifCredentials;
		}
		
		public Administrateur getAdmin(String login) {
			Administrateur getAdmin = administrateurdao.readAdministrateurByLogin(login);
			if (!(getAdmin.getLogin().equals(login))){
				getAdmin = null;
			}
			return getAdmin;
		}
		public Boolean createAgentByLogin(String login, Agent agent) {
			Boolean canCreate;
			
			if(login.equals(agent.getAdminAffecte().getLogin())) {
				canCreate = true;
				
				String matricule = "";
				List<Agent> listAgents = agentdao.readAllAgents();
				int i = 1;
				boolean exist = false;
				do {
					matricule = "agent_"+i;
					for(Agent agentCourant : listAgents) {
						exist |= matricule.equals(agentCourant.getMatricule());
					}
					i++;
				} while(exist);
				agent.setMatricule(matricule);
				
				
				if(!agentdao.createAgent(agent)) {
					canCreate = false;
//					System.out.println("Vous avez les droit mais l'action n'as pas pu se faire");
				}
			} else {
				canCreate = false;
//				System.out.println("Vous n'avez pas les droits sur cet agent");
			}
			
			return canCreate;
		}
		public List<Agent> readListAgentByLogin(String login, String matricule_nom) {
			return agentdao.searchAllAgentsByLoginAdmin(login, matricule_nom);
		}
		public Boolean updateAgentByLogin(String login, Agent agent) {
			Boolean canUpdate;
			if(login.equals(agent.getAdminAffecte().getLogin())) {
				canUpdate = true;
					if(!agentdao.updateAgent(agent)) {
						canUpdate = false;
//						System.out.println("Vous avez les droit mais l'action n'as pas pu se faire");
					}
			}else{
				canUpdate = false;
//				System.out.println("Vous n'avez pas les droits sur cet agent");
			}
			return canUpdate;
		}
		public Boolean deleteAgentByLogin(String login, Agent agent) {
			Boolean canDelete;
			if(login.equals(agent.getAdminAffecte().getLogin())) {
				canDelete = true;
				if(!agentdao.deleteAgentByMatricule(agent.getMatricule())) {
					canDelete = false;
//					System.out.println("Vous avez les droit mais l'action n'as pas pu se faire");
				}
			}else{
				canDelete = false;
//				System.out.println("Vous n'avez pas les droits sur cet agent");
			}
			return canDelete;
		}
		public List<Client> readListClientByLogin(String login, Agent agent) {
			List<Client> rendu = new ArrayList<Client>();
			if(login.equals(agent.getAdminAffecte().getLogin())) {
				rendu = clientdao.readAllClientsByMatricule(agent.getMatricule());
//				if(rendu == null)
//					System.out.println("Vous avez les droit mais l'action n'as pas pu se faire");
			} else {
//				System.out.println("Vous n'avez pas les droits sur cet agent");
			}
			return rendu;
		}
		public List<Client> readListClientsByLogin(String loginAdmin, Agent agent, String loginClient_nom) {
			List<Client> rendu = new ArrayList<Client>();
			if(loginAdmin.equals(agent.getAdminAffecte().getLogin())) {
				rendu = agentservice.readListClientsByMatricule(loginAdmin, loginClient_nom);
			} else {
//				System.out.println("Vous n'avez pas les droits sur cet agent");
			}
			return rendu;
		}
		public Boolean updateClientByLogin(String loginAdmin, Client client) {
			Boolean canUpdate;
			if(loginAdmin.equals(client.getAgentAffecte().getAdminAffecte().getLogin())) {
				canUpdate = true;
				if(clientdao.updateClient(client)) {
					canUpdate = false;
//					System.out.println("Vous avez les droit mais l'action n'as pas pu se faire");
				}
			} else {
				canUpdate = false;
//				System.out.println("Vous n'avez pas les droits sur ce client");
			}
			return canUpdate;
		}
		public Boolean printListAgents(List<Agent> listAgent) {
			
			Boolean canPrint;
			
			try {
				File pdf = new File("./ListClient.txt");
				pdf.createNewFile();
				
				writer = new PrintWriter(pdf);

				for(Agent agentCourant : listAgent) {
					writer.println(agentCourant.toString());
				}
				
				canPrint = true;
				
			} catch (FileNotFoundException e) {
				canPrint = false;
				e.printStackTrace();
			} catch (IOException e) {
				canPrint = false;
				e.printStackTrace();
			}
			
			return canPrint;
		}
		public Boolean printListClients(List<Client> listClient) {
			
			Boolean canPrint;
			
			try {
				File pdf = new File("./ListClient.txt");
				pdf.createNewFile();
				
				writer = new PrintWriter(pdf);

				for(Client clientCourant : listClient) {
					writer.println(clientCourant.toString());
				}
				
				canPrint = true;
				
			} catch (FileNotFoundException e) {
				canPrint = false;
				e.printStackTrace();
			} catch (IOException e) {
				canPrint = false;
				e.printStackTrace();
			}
			
			return canPrint;
		}
		public List<Affectation> readPendingAffectationsByLogin(String login) {
			return readListAffectationsByLogin(login, StatutAffectation.en_attente_admin);
		}
		public Boolean updateAffectationByLogin(String login, Affectation affectation) {
			Boolean canUpdate;
			if(login.equals(affectation.getAdminAffecte().getLogin())) {
				canUpdate = true;
				if(!affectationdao.updateAffectation(affectation)) {
					canUpdate = false;
//					System.out.println("Vous avez les droit mais l'action n'as pas pu se faire");
				}
			} else {
				canUpdate = false;
//				System.out.println("Vous n'avez pas les droits sur cette Affectation");
			}
			return canUpdate;
		}
		public List<Affectation> readListAffectationsByLogin(String login) {
			List<Affectation> affectationCorrespondante = new ArrayList<Affectation>();
			List<Affectation> affectationPossible = affectationdao.readAllAffectations();
			for(Affectation affectationCourant : affectationPossible) {
				if(affectationCourant.getAdminAffecte().getLogin().equals(login)) {
					affectationCorrespondante.add(affectationCourant);
				}
			}
			return affectationCorrespondante;
		}
		public List<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation) {
			List<Affectation> affectationCorrespondante = new ArrayList<Affectation>();
			List<Affectation> affectationPossible = affectationdao.readAllAffectationsByStatut(statutAffectation);
			for(Affectation affectationCourant : affectationPossible) {
				if(affectationCourant.getAdminAffecte().getLogin().equals(login)) {
					affectationCorrespondante.add(affectationCourant);
				}
			}
			return affectationCorrespondante;
		}
		public List<Affectation> readListAffectationsByLogin(String login, TypeTriAffectionAdmin order) {
			List<Affectation> affectationCorrespondante = new ArrayList<Affectation>();
			List<Affectation> affectationPossible = affectationdao.readAllAffectationsOrderByTypeTriAffectationAdmin(order);
			for(Affectation affectationCourant : affectationPossible) {
				if(affectationCourant.getAdminAffecte().getLogin().equals(login)) {
					affectationCorrespondante.add(affectationCourant);
				}
			}
			return affectationCorrespondante;
		}
		public List<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation, TypeTriAffectionAdmin order) {
			List<Affectation> affectationCorrespondante = new ArrayList<Affectation>();
			List<Affectation> affectationPossible = affectationdao.readAllAffectationsByStatutOrderByTypeTriAffectationAdmin(statutAffectation, order);
			for(Affectation affectationCourant : affectationPossible) {
				if(affectationCourant.getAdminAffecte().getLogin().equals(login)) {
					affectationCorrespondante.add(affectationCourant);
				}
			}
			return affectationCorrespondante;
		}
		public Administrateur getAdministrateur(String login) {
			return administrateurdao.readAdministrateurByLogin(login);
		}
}
