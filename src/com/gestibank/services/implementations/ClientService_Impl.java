package com.gestibank.services.implementations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.gestibank.dao.interfaces.ClientDAO;
import com.gestibank.dao.interfaces.CompteCourantDAO;
import com.gestibank.dao.interfaces.CompteRemunereDAO;
import com.gestibank.dao.interfaces.DemandeChequierDAO;
import com.gestibank.dao.interfaces.TransactionDAO;
import com.gestibank.enumerations.TypeTransaction;
import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.CompteCourant;
import com.gestibank.models.CompteRemunere;
import com.gestibank.models.DemandeChequier;
import com.gestibank.models.Transaction;
import com.gestibank.services.interfaces.ClientService;

@Service("clientService")
@Transactional

public class ClientService_Impl implements ClientService {

	/* INJECTIONS DAO */
	@Autowired
	private ClientDAO clientDAO;
	@Autowired
	private CompteCourantDAO comptecourantDAO;
	@Autowired
	private CompteRemunereDAO compteremunereDAO;
	@Autowired
	private TransactionDAO transactionDAO;
	@Autowired
	private DemandeChequierDAO demandechequierDAO;

	/* CONSTRUCTEURS */
	//Constructeurs sans parametres
	public ClientService_Impl() {

	}

	/* METHODES INTERFACE */
	public Boolean verifCredentials(String login, String pwd) {
		boolean okVerifCredentials = false;
		Client clientTemp = clientDAO.readClientByLogin(login);
		if (!(clientTemp ==null)) {
			okVerifCredentials = clientTemp.getPassword().equals(pwd);
		}
		return okVerifCredentials;
	}
	public Boolean updateCompteClientByLogin(String login, Client client) {
		boolean okUpdateCompteClientByLogin = false;
		if(login.equals(client.getLogin())) {
			okUpdateCompteClientByLogin = clientDAO.updateClient(client);
		}
		return okUpdateCompteClientByLogin;
	}
	public List<CompteBancaire> readListComptesBancairesByLogin(String login) {
		List<CompteBancaire> ListCompteBancaire = new ArrayList<CompteBancaire>();
		ListCompteBancaire.addAll(comptecourantDAO.readAllCompteCourantsByLogin(login));
		ListCompteBancaire.addAll(compteremunereDAO.readAllCompteRemuneresByLogin(login));			
		return ListCompteBancaire ;
	}
	public Boolean createTransactionByLogin(String login, Transaction transaction) {
		boolean okcreateTransactionByLogin = false;
		CompteBancaire cbTemp =transaction.getCompte();
		if (login.equals(transaction.getCompte().getPropritaireClient().getLogin())) {
			if (transaction.getTypeTransaction() ==TypeTransaction.debit) {
				if (cbTemp instanceof CompteCourant) {
					CompteCourant ccTemp = (CompteCourant) cbTemp;
					if (ccTemp.getSolde()+ccTemp.getPlafondDecouvert() >= transaction.getMontant()) {
						okcreateTransactionByLogin =  transactionDAO.createTransaction(transaction);
						ccTemp.setSolde(ccTemp.getSolde()-transaction.getMontant());
						okcreateTransactionByLogin = true;
					}
					else {
//						System.out.println("Solde insuffisant");
					}
				}
				else {
					okcreateTransactionByLogin =  transactionDAO.createTransaction(transaction);
					cbTemp.setSolde(cbTemp.getSolde()+transaction.getMontant());
					okcreateTransactionByLogin = true;
				}
			}
			else {
				okcreateTransactionByLogin =  transactionDAO.createTransaction(transaction);
				cbTemp.setSolde(cbTemp.getSolde()+transaction.getMontant());
				okcreateTransactionByLogin = true;
			}
		}

		else {
			okcreateTransactionByLogin = false;
		}
		return okcreateTransactionByLogin;
	}
	public Boolean createVirementByLogin(String login, Transaction transactionDebit, Transaction transactionCredit) {
		boolean okCreateVirementByLogintransactionDebit = false;
		boolean okcreateVirementByLogintransactionCredit = false;
		if (login.equals(transactionDebit.getCompte().getPropritaireClient().getLogin())){	
			okCreateVirementByLogintransactionDebit = createTransactionByLogin(transactionDebit.getCompte().getPropritaireClient().getLogin(),transactionDebit);
			if (okCreateVirementByLogintransactionDebit) {
				okcreateVirementByLogintransactionCredit = createTransactionByLogin(transactionCredit.getCompte().getPropritaireClient().getLogin(),transactionCredit);
			}

		}
		return (okcreateVirementByLogintransactionCredit && okCreateVirementByLogintransactionDebit);
	}
	public List<Transaction> readListTransactionByLogin(String login, CompteBancaire compte, Date dateDebut, Date dateFin) {
		List<Transaction> ListTransaction = new ArrayList<Transaction>();
		List<Transaction> ListTransactionSelect = new ArrayList<Transaction>();
		if (login.equals(compte.getPropritaireClient().getLogin())) {			
			for (Transaction t:ListTransaction) {
				Date dateTransaction= t.getDateTransaction();
				if (dateTransaction.compareTo(dateDebut)>=0 ||(dateTransaction.compareTo(dateFin))<=0) {
					ListTransactionSelect.add(t);
				}
			}
		}
		return ListTransactionSelect;
	}
	public Boolean printListTransaction(List<Transaction> listTransaction) {
		boolean okPrintListTransaction = false;
		File file = new File("Liste_des_transactions.txt");
		PrintWriter writer;
		try {
			writer = new PrintWriter(file);
			for(Transaction t: listTransaction) {
				writer.println(t.toString());
				okPrintListTransaction = true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return okPrintListTransaction;
	}
	public Boolean createDemandeChequierByLogin(String login, DemandeChequier demandeChequier) {
		boolean okCreateDemandeChequier = false;
		if (login.equals(demandeChequier.getCompte().getPropritaireClient().getLogin())) {
			okCreateDemandeChequier = demandechequierDAO.createDemandeChequier(demandeChequier);
		}
		return okCreateDemandeChequier;
	}
	public Double getSoldeCompteBancaireByLogin(String login, CompteBancaire compte) {
		double solde = 0.0;
		if (login.equals(compte.getPropritaireClient().getLogin())) {
			if( compte instanceof CompteCourant) {
				solde = comptecourantDAO.readCompteCourantByRib(compte.getRib()).getSolde();
			}
			else if (compte instanceof CompteRemunere) {
				solde = compteremunereDAO.readCompteRemunereByRib(compte.getRib()).getSolde();

			}
		}
		return solde;
	}
	public Client getClient(String login) {
		return clientDAO.readClientByLogin(login);
	}
}
