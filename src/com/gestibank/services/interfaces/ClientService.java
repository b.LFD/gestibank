package com.gestibank.services.interfaces;

import java.util.Date;
import java.util.List;

import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.DemandeChequier;
import com.gestibank.models.Transaction;



public interface ClientService {
	// verification des identifiants de l'utilisateur  pour une connection en tant que client
	Boolean verifCredentials(String login, String pwd);

	// mise a jour du CLIENT via l'objet Client client seulement si ce dernier correspond (en tant que login) au CLIENT
	Boolean updateCompteClientByLogin(String login, Client client);

	// retourne la liste des CompteBancaire qui sont sous affilies au CLIENT
	List<CompteBancaire> readListComptesBancairesByLogin(String login);

	// creation de la nouvelle Transaction transaction dans la bdd seulement si cette derniere concerne un compte affilie au CLIENT
	Boolean createTransactionByLogin(String login, Transaction transaction);

	// creation d'un virement dans la bdd seulement si la transactionDebit et transactionCredit forme bien un virement
	// et si la Transaction transactionDebit concerne un compte affilie au CLIENT
	Boolean createVirementByLogin(String login, Transaction transactionDebit, Transaction transactionCredit);

	// retourne la liste de toute les Transactions faite entre les Dates dateDebut et dateFin sur le CompteBancaire compte
	// seulement si ce dernier est possede par le CLIENT
	List<Transaction> readListTransactionByLogin(String login, CompteBancaire compte, Date dateDebut, Date dateFin);

	// imprime dans un document txt la liste de toute les Transaction passer en parametre dans listTransaction
	Boolean printListTransaction(List<Transaction> listTransaction);

	// cree la DemandeChequier demandeChequier en bdd seulement si le client reponsable de cette demande est bien le CLIENT
	Boolean createDemandeChequierByLogin(String login, DemandeChequier demandeChequier);

	// retourne le Solde du CompteBancaire compte seulement si le CLIENT est bien titulaire de ce compte
	Double getSoldeCompteBancaireByLogin(String login, CompteBancaire compte);

	//retourne un client by login
	Client getClient(String login);
}