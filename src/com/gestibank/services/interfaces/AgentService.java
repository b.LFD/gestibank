package com.gestibank.services.interfaces;

import java.util.List;

import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;
import com.gestibank.models.CompteBancaire;
import com.gestibank.models.DemandeChequier;
import com.gestibank.models.Transaction;



public interface AgentService {
	
	/**V�rifie que le login et le password correspond � la session courante.
	 * Renvoie true si ce pr�dicat est vrai.
	 */
	Boolean verifCredentials(String login, String pwd);
	
	/** readListClientsByMatricule renvoie la liste des clients d'un agent 
	 */
	List<Client> readListClientsByMatricule(String matricule, String login_nom);
	
	/** Met � jour un client en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client 
	 * Renvoie true si la commande c'est effectu�e correctement.
	 */
	Boolean updateClientByMatricule(String matricule, Client client);
	
	/** Supprime un client en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client 
	 * Renvoie true si la commande c'est effectu�e correctement.
	 */
	Boolean deleteClientByMatricule(String matricule, Client client);
		
	/** Ecrit dans un fichier la liste des clients pass�s en param�tre
	* Renvoie true si la commande c'est effectu�e correctement.
	*/
	Boolean printListClients(List<Client> listClient);
	
	/**Cr�e un un compte bancaire en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client
	* Renvoie true si la commande c'est effectu�e correctement.
	*/
	Boolean createCompteBancaireByMatricule(String matricule, CompteBancaire compte);
	
	/** Supprime un compte bancaire en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client 
	 * Renvoie true si la commande c'est effectu�e correctement.
	 */
	Boolean deleteCompteBancaireByMatricule(String matricule, CompteBancaire compte);
	
	/** Renvoie la liste des comptes bancaires d'un client en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client.
	 */
	List<CompteBancaire> readListComptesBancaireByMatricule(String matricule, Client client);
	
	/**Cr�e une transaction en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client 
	 * Renvoie true si la commande c'est effectu�e correctement*/
	Boolean createTransactionByMatricule(String matricule, Client client, Transaction transaction);
	
	
	/** Cr�e un virement sur les comptes d'un m�me client en v�rifiant que le matricule de l'agent correspond au matricule de l'agent affect� � ce client .
	 * Renvoie true si la commande c'est effectu�e correctement 
	 */
	Boolean createVirementByMatricule(String matricule, Client client, Transaction transactionDebit, Transaction transactionCredit);
	
	/**Renvoie la liste des affectations en attente de l'agent correspondant au matricule.
	 */	
	List<Affectation> readListPendingAffectationsByMatricule(String matricule);
	
	/** Met � jour une affectation en fonction de la valeur du bool�en pass� en param�tre. Si le bool�en est vrai, l'affectation est accept�e et un nouveau client est cr�e.
	 * Sinon, l'affectation est refus�e.
	 */
	Boolean updateAffectationByMatricule(String matricule, Affectation affectation, Boolean validation);
	
	/** Met � jour une demande de chequier en fonction de la valeur du bool�en pass� en param�tre. Si le bool�en est vrai, la demande de chequier est accept�e.
	 * Sinon, la demande de ch�quier est refus�e.
	 */
	Boolean updateDemandeChequierByMatricule(String matricule, DemandeChequier demandeChequier, Boolean validation);

	/** retour l'agent en cours dont le matricule est egale a "matricule"
	 */
	Agent getAgent(String matricule);
}
