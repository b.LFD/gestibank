package com.gestibank.services.interfaces;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gestibank.enumerations.StatutAffectation;
import com.gestibank.enumerations.TypeTriAffectionAdmin;
import com.gestibank.models.Administrateur;
import com.gestibank.models.Affectation;
import com.gestibank.models.Agent;
import com.gestibank.models.Client;


@Service("adminService")
public interface AdminService {
	// verification des identifiants de l'admin lors de la connection
	Boolean verifCredentials(String login, String pwd);
	
	// creation de l'Agent agent si l'ADMIN appelant correspond a l'Admin responsable de l'Agent
	Boolean createAgentByLogin(String login, Agent agent);
	
	// retourne la liste des agents sous la tutelle de l'ADMIN appelant commencant par le nom ou matricule "matricule_nom"
	List<Agent> readListAgentByLogin(String login, String matricule_nom);
	
	// mise a jour de l'Agent agent dans la bdd si l'ADMIN appelant est responsable de ce dernier
	Boolean updateAgentByLogin(String login, Agent agent);
	
	// suppresion de l'Agent agent de la bdd si l'ADMIN appelant est responsable de ce dernier
	Boolean deleteAgentByLogin(String login, Agent agent);
	
	// retourne la liste des Clients sous la tutelle de l'Agent agent seulement si l'ADMIN appelant est responsable de ce dernier
	List<Client> readListClientByLogin(String login, Agent agent);
	
	// retourne la liste des Clients avec un nom ou login commencant par "loginClient_nom" qui sous la tutelle de l'Agent agent
	// seulement si l'ADMIN appelant est responsable de ce dernier
	List<Client> readListClientsByLogin(String loginAdmin, Agent agent, String loginClient_nom);
	
	// mise a jour du Client client dans la bdd seulement si l'ADMIN appelant est responsable de l'Agent affecte a ce Client
	Boolean updateClientByLogin(String loginAdmin, Client client);
	
	// imprime dans un fichier txt la list des Agent passes en parametre
	Boolean printListAgents(List<Agent> listAgent);
	
	// imprime dans un fichier txt la list des Client passes en parametre
	Boolean printListClients(List<Client> listClient);
	
	// retourne la liste des affectation en attente d'affectation attribuees a l'ADMIN appelant
	List<Affectation> readPendingAffectationsByLogin(String login);
	
	// mise a jour de l'Affectation affectation seulement si l'ADMIN appelant est responsable de cette derniere
	Boolean updateAffectationByLogin(String login, Affectation affectation);
	
	// retourne la liste de toute les affectations dont l'ADMIN appelant est responsable
	List<Affectation> readListAffectationsByLogin(String login);
	
	// retourne la liste de toute les affectations ayant le status statutAffectation dont l'ADMIN appelant est responsable
	List<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation);
	
	// retourne la liste de toute les affectations dont l'ADMIN appelant est responsable, trier suivant order
	List<Affectation> readListAffectationsByLogin(String login, TypeTriAffectionAdmin order);
	
	// retourne la liste de toute les affectations ayant le status statutAffectation dont l'ADMIN appelant est responsable,
	// trier suivant order
	List<Affectation> readListAffectationsByLogin(String login, StatutAffectation statutAffectation, TypeTriAffectionAdmin order);
	
	// retourne l'admin dont le login est "login", soit l'admin en cours
	Administrateur getAdministrateur(String login);
}
