<!-- <%@ page language="java" contentType="text/html; charset=utf-8"
pageEncoding="utf-8"%>
    <%@ taglib
uri="http://www.springframework.org/tags/form" prefix="form"%>
        <%@ taglib
uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> -->

<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Espace-Client</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="index.html">
            <img src="../img/logo-me.png" alt="GestiBank" width="45" height="45">
            <!-- GestiBank -->
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">
                        <!-- Home -->
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="espace-client/accueil-client.jsp">Mes comptes
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="espace-client/virement.jsp">Virement</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="espace-client/historique-operation.jsp">Mon historique</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.jsp">Contact</a>
                </li>

                <form class="form-inline my-2 my-md-0" data-children-count="1">
                    <input class="form-control py-2 border-right-0 border" type="search" value="Rechercher" aria-label="Search" id="example-search-input">
                    <span class="input-group-append">
                                      <button class="btn btn-outline-secondary border" type="button">
                                        <!-- Search -->
                                        <i class="fa fa-search"></i>
                                      </button>
                                    </span>

                    <!-- <i class="fa fa-search"></i> -->
                </form>

                <li class="nav-item">
                    <a class="nav-link" href="index.html">Déconnexion</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">
    <!-- <section class="page-section"> -->
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <br>
                <h2>Espace Client</h2>
                <p>Bienvenue sur votre espace personnel.</p>
                <table class="table">
                    <thead class="thead table-info">
                        <tr>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Type de compte</th>
                            <th>Numéro de compte</th>
                            <th>Solde</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${compt.nom}</td>
                            <td>${compt.prenom}</td>
                            <td>${compt.typeCompte}</td>
                            <td>${compt.numeroCompte}</td>
                            <td>${compt.soldeCompte}</td>
                        </tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-info">Opération</button>
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-info">Historique</button>
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-info">Chéquier</button>
                    </div>
                </div>

                <br>
                <br>
                <br>

                <c:foreach var="compt" items="${list}">
                </c:foreach>
                <table class="table">
                    <thead class="thead table-info">
                        <tr>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Type de compte</th>
                            <th>Numéro de compte</th>
                            <th>Solde</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${compt.nom}</td>
                            <td>${compt.prenom}</td>
                            <td>${compt.typeCompte}</td>
                            <td>${compt.numeroCompte}</td>
                            <td>${compt.soldeCompte}</td>
                        </tr>
                    </tbody>

                </table>

                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-info">Opération</button>
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-info">Historique</button>
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-info">Chéquier</button>
                    </div>
                </div>
            </div>
            <!-- </section> -->
            <!-- </div> -->
        </div>



    </div>
</div>
</body>

</html>