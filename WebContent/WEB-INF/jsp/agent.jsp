<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
  <!-- Bulma Version 0.9.0-->
  <link rel="stylesheet" href="https://unpkg.com/bulma@0.9.0/css/bulma.min.css" />
  <link rel="stylesheet" type="text/css" href="css/index.css">
  <script async type="text/javascript" src="../js/bulma.js"></script>
  
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<title>GestiBank</title>
</head>
<body>
       <!-- START NAV -->
    <nav class="navbar is-white">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item brand-text" href="../">
          Bank  Agent
        </a>
                <div class="navbar-burger burger" data-target="navMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div id="navMenu" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="admin.html">
            Se déconnecter >
          </a>
                </div>

            </div>
        </div>
    </nav>
    <!-- END NAV -->
    <div class="container">
        <div class="columns">
            <div class="column is-3 ">
                <aside class="menu is-hidden-mobile">
                    <p class="menu-label">
                        General
                    </p>
                    <ul class="menu-list">
                        <li><a class="is-active">Dashboard</a></li>
                    </ul>
                    <ul class="menu-list">
                        <li>
                            <a>Gestion</a>
                            <ul>
                                <li><a  > <button class="button is-block is-right is-primary is-medium is-rounded" type="submit">
                                    Recherche client
                                </button></a></li>
                               
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="column is-9">
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li><a href="../">View</a></li>
                        <li class="is-active"><a href="#" aria-current="page">Agent</a></li>
                    </ul>
                </nav>
                <section class="hero is-primary welcome is-small">
                    <div class="hero-body">
                        <div class="container">
                            <h1 class="title">
                                Espace Agent
                            </h1>
                            <h2 class="subtitle">
                                Bonjour, j'espère que que vous avez une bonne journée!
                            </h2>
                        </div>
                    </div>
                </section>
                <div class="columns">
                    <div class="column is-12">
                        <div class="card events-card">
                            <header class="card-header">
                                                <p class="card-header-title">
                                                    Affectation clients
                                                </p>
                                                <a href="#" class="card-header-icon" aria-label="more options">
                                                        <span class="icon">
                                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                        </span>
                                                </a>
                            </header>
                            <div class="card-table">
                                <form>
                                            <div class="content">
                                                <table class="table is-fullwidth is-striped">
                                                    <tbody>
                                                         <tr>
                                                             <th>nom</td>
                                                             <th>prénom</th>
                                                             <th>âge</td>
                                                             <th>nik</td>   
                                                             <th>employée</th>
                                                             <th>
                                                                 
                                                             </th>
                                                        </tr>
                                                        <tr>
                                                            <td>Camile</td>
                                                            <td>Henry</td>
                                                            <td>45</td>
                                                            <td>Peld</td>
                                                            <td><label class="checkbox"><input type="checkbox"  name="empl1" value="1"></label></td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Constantin</td>
                                                            <td>Brancusi</td>
                                                            <td>28</td>
                                                            <td>Peld</td>
                                                            <td><label class="checkbox"><input type="checkbox" name="empl2" value="2"></label></td>
                                                        </tr>
                                                        <tr>
                                                             <td>Eugène</td>
                                                             <td>Ionesco</td>
                                                             <td>32</td>
                                                             <td>Peld</td>
                                                            <td><label class="checkbox"><input type="checkbox" name="empl3" value="3"></label></td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                </form>
                            </div>
                            <footer class="card-footer">

                            </footer>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <script async type="text/javascript" src="../js/agent.js"></script>


</body>
</html>     